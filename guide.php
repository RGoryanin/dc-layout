<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once 'variables.php';
		include_once $partialsPath . '_head.php';
	?>
	

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<div class="container">
				<br>
				<br>
				<div class="row">
					<div class="col-6">
						<h1>checkbox</h1>
					</div>
					<div class="col-6">
						<h1>radio</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<form action="">
							<div class="form-item">
								<div class="form-checkbox">
									<input type="checkbox" name="" id="checkbox1" class="form-checkbox__input">
									<label for="checkbox1" class="form-checkbox__label">Тестовое </label>
								</div>
							</div>
							<div class="form-item">
								<div class="form-checkbox">
									<input type="checkbox" name="" id="checkbox2" class="form-checkbox__input" checked>
									<label for="checkbox2" class="form-checkbox__label">Тестовое выбрано</label>
								</div>
							</div>
							<div class="form-item">
								<div class="form-checkbox">
									<input type="checkbox" name="" id="checkbox3" class="form-checkbox__input" checked disabled>
									<label for="checkbox3" class="form-checkbox__label">Тестовое выбрано, не активно </label>
								</div>
							</div>
							<div class="form-item">
								<div class="form-checkbox">
									<input type="checkbox" name="" id="checkbox4" class="form-checkbox__input" disabled>
									<label for="checkbox4" class="form-checkbox__label">Тестовое не активно</label>
								</div>
							</div>
						</form>
					</div>
					<div class="col-6">
						<form action="">
							<div class="form-item">
								<div class="form-radio">
									<input type="radio" name="" id="radio1" class="form-radio__input">
									<label for="radio1" class="form-radio__label">Тестовое </label>
								</div>
							</div>
							<div class="form-item">
								<div class="form-radio">
									<input type="radio" name="" id="radio2" class="form-radio__input" checked>
									<label for="radio2" class="form-radio__label">Тестовое выбрано</label>
								</div>
							</div>
							<div class="form-item">
								<div class="form-radio">
									<input type="radio" name="" id="radio3" class="form-radio__input" checked disabled>
									<label for="radio3" class="form-radio__label">Тестовое выбрано, не активно </label>
								</div>
							</div>
							<div class="form-item">
								<div class="form-radio">
									<input type="radio" name="" id="checkbox4" class="form-radio__input" disabled>
									<label for="radio4" class="form-radio__label">Тестовое не активно</label>
								</div>
							</div>
						</form>
					</div>
				</div>
				<br>
				<br>
				<br>
				<div class="row">
					<div class="col-12">
						<h1>Stars</h1>
					</div>
					<div class="col-6">
						<br>
						<h5>Активный рейтинг</h5>
						<div class="stars">
							<form novalidate="novalidate">
								<div class="stars__items">
									<input class="stars__button" checked name="star" value="5" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="4" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="3" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="2" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="1" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
								</div>
							</form>
							<a class="stars__count" href="">
								16
							</a>
		              	</div>
						<br>
						<div class="stars stars_red">
							<form novalidate="novalidate">
								<div class="stars__items">
									<input class="stars__button" checked name="star" value="5" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="4" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="3" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="2" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
									<input class="stars__button" name="star" value="1" type="radio">
									<span class="stars__item"><span class="icon-star"></span></span>
								</div>
							</form>
							<a class="stars__count" href="">
								16
							</a>
		              	</div>
						
					</div>	
					<div class="col-6">
						<br>
		              	<h5>Информативный рейтинг</h5>
		              	<div class="stars stars_view">
							<form novalidate="novalidate">
								<div class="stars__items">
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
								</div>
								<div class="stars__items stars__items_active" style="width:40%">
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
								</div>
							</form>
							<a class="stars__count" href="">
								16
							</a>
		              	</div>
		              	<br>
		              	<div class="stars stars_red stars_view">
							<form novalidate="novalidate">
								<div class="stars__items">
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
								</div>
								<div class="stars__items stars__items_active" style="width:40%">
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
									<span class="stars__item"><span class="icon-star"></span></span>
								</div>
							</form>
							<a class="stars__count" href="">
								16
							</a>
		              	</div>
					</div>
				</div>
				<br>
				<br>
				<br>
				<div class="row">
					<div class="col-12">
						<h1>Buttons</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<button class="btn btn-outline mb-3 mr-3">.btn.btn-outline</button>
						<button class="btn btn-outline btn-outline-white mb-3 mr-3">.btn.btn-outline.btn-outline-white</button>
						<button class="btn btn-outline btn-outline-primary mb-3 mr-3">.btn.btn-outline.btn-outline-primary</button>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<button class="btn btn-primary mb-3 mr-3">.btn.btn-primary</button>
						<button class="btn btn-danger mb-3 mr-3">.btn.btn-danger</button>
						
						
					</div>
				</div>
				<br>
				<br>
				<br>
				<div class="row">
					<div class="col-12">
						<h1>Forms</h1>
					</div>
					<br>
					<div class="col-6">
						<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Email address</label>
						    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ваш email">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Password</label>
						    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Придумайте пароль">
						  </div>
						</form>
					</div>
				</div>
				<br>
				<br>
				<br>
				<div class="row">
					<div class="col-12">
						<h1>Modals</h1>
					</div>
					<div class="col-12">
						<button class="btn btn-outline mb-3 mr-3" data-toggle="modal" data-target="#regModal">Регистрация</button>
						<button class="btn btn-outline btn-outline-primary mb-3 mr-3" data-toggle="modal" data-target="#loginModal">Вход</button>
						<button class="btn btn-danger mb-3 mr-3" data-toggle="modal" data-target="#newPassModal">Восстановление пароля</button>
						<button class="btn btn-primary mb-3 mr-3" data-toggle="modal" data-target="#successModal">Успешное сообщение</button>
						
					</div>
					<div class="col-12">
						

						<!-- Modal NewPass-->
						<div class="modal fade" id="newPassModal" tabindex="-1" role="dialog" aria-labelledby="newPassModal" aria-hidden="true">
						  <div class="modal-dialog modal-dialog-centered" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <div class="modal-title">Восстановление пароля</div>
						        <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
									<span class="icon-x"></span>
								</button>
						      </div>
						      <div class="modal-body">
						        <form class="form">
								  <div class="form-group">
								    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ваш email">
								  </div>
								 
								  <div class="form-group">
								  		<button class="btn btn-primary btn-block" type="submit">Восстановить пароль</button>
								  </div>
								</form>
								<div class="row mt-2 text-small">
									<div class="col-12">
										<a href="" class="link">Войти</a> или <a href="" class="link">зарегистрироватсья</a>
									</div>
									
								</div>
						      </div>

						    </div>
						  </div>
						</div>

						<!-- Modal success-->
						<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModal" aria-hidden="true">
						  <div class="modal-dialog modal-dialog-centered" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
									<span class="icon-x"></span>
								</button>
						      </div>
						      <div class="modal-body">
						       <div class="row">
						       		<div class="col-12">
						       			<div class="success-modal text-center">
						       				<div class="success-modal__img-container mt-2 mb-4">
						       					<img src="<?= $imagePath ?>success-img.jpg" alt="" class="success-modal__img">
						       				</div>
						       				<div class="success-modal__title">
						       					Проверьте почту
						       				</div>
						       				<p class="lh_1_5 pb-4">На&nbsp;вашу почту была отправлена ссылка. Перейдите по&nbsp;ней для подтверждения регистрации.</p>
						       			</div>
						       			
						       		</div>
						       </div>
						      </div>

						    </div>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>