<?php
	//error_reporting(0);

	$assetsPath = '/web/';

	if (isset($_SERVER['HTTP_HOST']) && ('dc-layout.it-solo.com' == $_SERVER['HTTP_HOST'])) {
		error_reporting(E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE);
		ini_set('display_errors', 1);

		$assetsPath = '/web/';

		// auth
		$authenticated = false;
				
		if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
			$user = $_SERVER['PHP_AUTH_USER'];
			$pass = $_SERVER['PHP_AUTH_PW'];
			if ($user == 'dc-layout' && $pass == 'enter') {
				$authenticated = true;
			}
		}
		
		if (!$authenticated) {
			header('WWW-Authenticate: Basic realm="Restricted Area"');
			header('HTTP/1.1 401 Unauthorized');
			echo ("Access denied.");
			exit();
		}
	}

	$cssPath = $assetsPath .'src/scss/';
	$jsPath = $assetsPath .'src/js/';
	$partialsPath = $_SERVER['DOCUMENT_ROOT'] . $assetsPath . 'templates/partials/';
	$imagePath = $assetsPath .'src/img/'


?>