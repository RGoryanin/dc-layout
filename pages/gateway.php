<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include_once '../variables.php';
    include_once $partialsPath . '_head.php';
    $pageTitle = 'Samsung';
    ?>
    <link rel="stylesheet" href="<?= $cssPath ?>pages/gateway.css?v=<?=Date('t' )?>">

</head>
<body>

    <div class="offer-block">
        <div class="offer__title">Кэшбэк до 4% от Мегафон активирован</div>
        <div class="offer__description">Кэшбэк начислится после покупки</div>
        <div class="offer-counter-wrapper">
            <div class="offer-counter">3</div>
        </div>
        <a href="#" class="btn btn-primary offer__link">Перейти сейчас</a>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        $(function () {
            let digit = 3;
           let counterBlock = $('.offer-counter');
           let counter = setInterval(() => {
               --digit;
               $(counterBlock).text(digit);
               if (!digit) clearInterval(counter);
           }, 1000);
           let timer = setTimeout(() => {
               counter;
               clearTimeout(timer);
           }, 1000);
            timer;
        });
    </script>
</body>