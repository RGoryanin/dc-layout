<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Бренды';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/brands.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-12 col-sm-6 col-lg-12">
							<h1><?php echo $pageTitle ?></h1>
						</div>
                        <div class="col-sm-3 d-none d-sm-block d-lg-none">
                            <select name="" id="" class="select">
                                <option value="">Eng</option>
                                <option value="">Рус</option>
                            </select>
                        </div>
                        <div class="col-sm-3 d-none d-sm-block d-lg-none">
                            <select name="" id="" class="select">
                                <option value="">Все</option>
                                <option value="">A</option>
                                <option value="">B</option>
                                <option value="">C</option>
                                <option value="">D</option>
                                <option value="">E</option>
                                <option value="">F</option>
                                <option value="">G</option>
                                <option value="">H</option>
                                <option value="">I</option>
                                <option value="">J</option>
                                <option value="">K</option>
                                <option value="">L</option>
                                <option value="">M</option>
                                <option value="">N</option>
                                <option value="">O</option>
                                <option value="">P</option>
                                <option value="">Q</option>
                                <option value="">R</option>
                                <option value="">S</option>
                                <option value="">T</option>
                                <option value="">U</option>
                                <option value="">V</option>
                                <option value="">W</option>
                                <option value="">X</option>
                                <option value="">Y</option>
                                <option value="">Z</option>
                                <option value="">0–9</option>
                            </select>
                        </div>
					</div>
				</div>
			</div>

			
			<div class="container">	
				<div class="row">
					<div class="col-12">
						<div class="filter-alphabet d-none d-lg-block">
							<div class="filter-alphabet__lang d-inline-block">
								<span class="filter-alphabet__item active">Eng</span>
								<span class="filter-alphabet__item">Рус</span>
							</div>
							<div class="filter-alphabet__container d-inline-block">
								<span class="filter-alphabet__item active">Все</span>
								<span class="filter-alphabet__item">A</span>
								<span class="filter-alphabet__item">B</span>
								<span class="filter-alphabet__item">C</span>
								<span class="filter-alphabet__item">D</span>
								<span class="filter-alphabet__item">E</span>
								<span class="filter-alphabet__item">F</span>
								<span class="filter-alphabet__item">G</span>
								<span class="filter-alphabet__item">H</span>
								<span class="filter-alphabet__item">I</span>
								<span class="filter-alphabet__item">J</span>
								<span class="filter-alphabet__item">K</span>
								<span class="filter-alphabet__item">L</span>
								<span class="filter-alphabet__item">M</span>
								<span class="filter-alphabet__item">N</span>
								<span class="filter-alphabet__item">O</span>
								<span class="filter-alphabet__item">P</span>
								<span class="filter-alphabet__item">Q</span>
								<span class="filter-alphabet__item">R</span>
								<span class="filter-alphabet__item">S</span>
								<span class="filter-alphabet__item">T</span>
								<span class="filter-alphabet__item">U</span>
								<span class="filter-alphabet__item">V</span>
								<span class="filter-alphabet__item">W</span>
								<span class="filter-alphabet__item">X</span>
								<span class="filter-alphabet__item">Y</span>
								<span class="filter-alphabet__item">Z</span>
								<span class="filter-alphabet__item">0–9</span>
								
							</div>
							
						</div>
						<div class="filter-slelect d-block d-sm-none">
							<div class="row mb-2">
								<div class="col-6 pr-2">
									<select name="" id="" class="select">
										<option value="">Eng</option>
										<option value="">Рус</option>
									</select>
								</div>
								<div class="col-6 pl-2">
									<select name="" id="" class="select">
										<option value="">Все</option>
										<option value="">A</option>
										<option value="">B</option>
										<option value="">C</option>
										<option value="">D</option>
										<option value="">E</option>
										<option value="">F</option>
										<option value="">G</option>
										<option value="">H</option>
										<option value="">I</option>
										<option value="">J</option>
										<option value="">K</option>
										<option value="">L</option>
										<option value="">M</option>
										<option value="">N</option>
										<option value="">O</option>
										<option value="">P</option>
										<option value="">Q</option>
										<option value="">R</option>
										<option value="">S</option>
										<option value="">T</option>
										<option value="">U</option>
										<option value="">V</option>
										<option value="">W</option>
										<option value="">X</option>
										<option value="">Y</option>
										<option value="">Z</option>
										<option value="">0–9</option>
									</select>
								</div>
							</div>
						</div>
						<div class="brands">
							<div class="row">
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
							

							</div>
						</div>

                        <div class="slider">
                            <?php include_once $partialsPath . 'banners/banner-samsung.php'; ?>
                        </div>
						
						<div class="brands">
							<div class="row">
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
								<div class="col-4 col-sm-2 px-2 px-sm-3">
									<a href="" class="brands__item">
										<div class="brands__img-container">
											<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
										</div>
									</a>
								</div>
							</div>
						</div>

						<div class="pagination mt-0 mb-4 pb-2">
							<ul class="pagination__list">
								<li class="pagination__item"><a href="" class="pagination__link">1</a></li>
								<li class="pagination__item pagination__item_around"><a href="" class="pagination__link pagination__item_arrow pagination__item_arrow-back"><span class="icon-arrow"></span></a></li>
								<li class="pagination__item"><a href="" class="pagination__link">4</a></li>
								<li class="pagination__item pagination__item_active"><span class="pagination__link">5</span></li>
								<li class="pagination__item"><a href="" class="pagination__link">6</a></li>
								<li class="pagination__item pagination__item_around pagination__item_arrow"><a href="" class="pagination__link"><span class="icon-arrow"></span></a></li>
								<li class="pagination__item"><a href="" class="pagination__link">99</a></li>
							</ul>
						</div>
						<div class="name-list mb-5">
							<div class="row">
								<div class="col-12 h3 my-4 pb-3 border-bottom name-list__title">
									A
								</div>
							</div>
							<div class="row">
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link ">Все бренды на A</a></div>
							</div>
							<div class="row">
								<div class="col-12 h3 my-4 pb-3 border-bottom name-list__title">
									B
								</div>
							</div>
							<div class="row">
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link ">Все бренды на A</a></div>
							</div>
							<div class="row">
								<div class="col-12 h3 my-4 pb-3 border-bottom name-list__title">
									C
								</div>
							</div>
							<div class="row">
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link link_dark">A Bathing Ape</a></div>
								<div class="col-6 col-sm-2 mb-2"><a href="" class="link ">Все бренды на A</a></div>
							</div>
						</div>

						
					</div>
				</div>

			</div>
			
			<?php include_once $partialsPath . '_advantages.php'; ?>
			<div class="container">
				<?php include_once $partialsPath . '_about-block.php'; ?>
				<?php include_once $partialsPath . '_reg-form.php'; ?>
			</div>
			
		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>