<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-sm-3';
		$pageTitle = 'Покупки в магазине «М.Видео»';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/shop.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1><?php echo $pageTitle ?></h1>
						</div>
					</div>
				</div>
				
			</div>

			
			<div class="container">	
				<div class="row">
					<div class="col-12">
						<div class="shop mt-3">
							<div class="shop__header">
								<div class="row">
									<div class="col-12 col-sm-4">
										<div class="shop__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shop__img">
										</div>
									</div>
									<div class="col-sm-5 pl-3 pl-sm-0">
										<div class="shop__name">кэшбэк до 12%</div>
										<div class="shop__rating">
											<div class="stars stars_red d-inline-block mr-3 align-top">
												<form novalidate="novalidate">
													<div class="stars__items">
														<input class="stars__button" checked name="star" value="5" type="radio">
														<span class="stars__item"><span class="icon-star"></span></span>
														<input class="stars__button" name="star" value="4" type="radio">
														<span class="stars__item"><span class="icon-star"></span></span>
														<input class="stars__button" name="star" value="3" type="radio">
														<span class="stars__item"><span class="icon-star"></span></span>
														<input class="stars__button" name="star" value="2" type="radio">
														<span class="stars__item"><span class="icon-star"></span></span>
														<input class="stars__button" name="star" value="1" type="radio">
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
							              	</div>
							              	<span class="text-small d-inline-block color_gray_dark" style="line-height: 2">
												рейтинг магазина на основании <a href="" class="link link_red">72 отзывов</a>
											</span>
										</div>
										
									</div>
									<div class="col-12 col-sm-3 pt-3 pt-sm-1">
										<a href="/pages/shop.php"
                                           class="btn btn-primary btn-block mb-3 mt-2 mr-3 shop-link">
                                            <span class="d-inline d-sm-none d-lg-inline">Перейти в </span>
                                            <span class="d-none d-sm-inline d-lg-none">В </span>
                                            магазин
                                        </a>
									</div>
								</div>
							</div>
							<div class="shop__body m-fz-14">
								<div class="row">
									<div class="col-12 col-sm-4">
										<div class="h4 pt-4 mt-2 pl-0 pl-sm-3">Таблица ставок</div>
										<div class="shop__list-rate">
											<ul class="list-rate">
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Домашний офис
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Домашний офис
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Мобильная техника
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Развлечения и Фото-Видео
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Кухня
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Дом и забота о себе
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Кино и Звук
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
												<li class="list-rate__item">
													<div class="row">
														<div class="col-8">
															<div class="list-rate__name">
																Аудиотехника
															</div>
															
														</div>
														<div class="col-4 text-right">
															<span class="list-rate__rate-old">1,49%</span>
															<span class="list-rate__rate">1,50%</span>
															
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="col-12 col-sm-8">
										<div class="shop__desription">
											<div class="h4 pt-4 mt-2">Условия начисления кэшбэка</div>
											<p>Среднее время ожидания кэшбэка: 45 дней</p>
											<p>Максимальное время ожидания кэшбэка: 60 дней</p>
											<p>Уважаемые пользователи, обращаем ваше внимание, что сумма кэшбэка за заказ в магазине М.видео может изменяться в ходе обработки и будет правильно отображаться после подтверждения заказа.</p>
											<div class="h4 pt-4 mt-1">Кэшбэк не начисляется:</div>
											<p>услуги клиентам;</p>
											<p>установку техники;</p>
											<p>программы доп.сервиса;</p>
											<p>подарочные сертификаты;</p>
											<p>покупки в кредит.</p>
											<p>товары, оплаченные 100% бонусами или предоставлена 100% скидка на товар</p>
											<p>дополнительные продажи в магазине (например, клиент заказал телефон в интернет-магазине, а при самовывозе купил чехол или наушники, в этом случае чехол и наушники оплачиваться не будут)</p>
											<div class="h4 pt-4 mt-1">Покупай лучшую бытовую технику с кэшбэк в M.Видео</div>
											<p>Магазин М.Видео является крупнейшей в России розничной сетью по продаже электроники и бытовой техники. Сеть из 380 магазинов объединяет более сотни городов России. Товарный ассортимент состоит из 20 000 наименований: электроника, мелкая и крупная бытовая техника, смартфоны, аксессуары, телевизоры и многое другое.</p>
											<p>Помимо качественной техники магазин предлагает первоклассную сервисную поддержку и расширенную гарантию на многие товары. М.Видео постоянно прислушивается к отзывам покупателей улучшая обслуживание и ассортимент товаров.</p>
											<p>Ассортимент магазина полон новейшими моделями известных брендов, на которые часто распространяются акции и скидки. И если вы давно мечтали купить телевизор или холодильник с кэшбэком, то магазин М.Видео поможет ее исполнить.</p>
											<p>Покупая товары с кэшбэком в М.Видео пользователь может быть уверен, что получит качественную продукцию по низкой цене.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-12">
						<?php include_once $partialsPath . 'banners/banner-samsung.php'; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<h2 class="h1 mt-4 mb-4 pb-2">Промокоды</h2>
					</div>
					<div class="col-12">
						<div class="shops">
							<div class="row">
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a class="shops__item" href="">
										<div class=" shops__promo">
											Промокод при регистрации
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a class="shops__item" href="">
										<div class="shops__promo">
											Скидки 30% на все!
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a class="shops__item" href="">

										<div class="shops__promo">
											2500 ₽ купон на покупки
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a href="" class="shops__item">

										<div class="shops__promo">
											12% кешбек
										</div>
									</a>
								</div>

							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-6 col-sm-12">
						<h2 class="h1 mb-4 mt-4" data-toggle="modal" data-target="#addShopReview">
							Отзывы
						</h2>
					</div>
					<div class="col-6 text-right lh_1_5 d-block d-sm-none mt-4 pt-1">
						<a href="#addShopReview" class="link link_gray text-small" data-toggle="modal">Хочу написать отзыв <span class="around-link-icon around-link-icon_small">?</span></a>
					</div>
					<div class="col-12">
                        <?php include_once $partialsPath . '_reviews.php'; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4">
						<a class="h1 h-link" href="">Лучшие товары магазина <div class="around-link-icon"><span class="icon-arrow"></span></div></a>
						
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<?php include_once $partialsPath . '_products4.php'; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4 mt-4">
						<a class="h1 h-link" href="">Похожие магазины  <div class="around-link-icon"><span class="icon-arrow"></span></div></a>
						
					</div>
				</div>
				<div class="row mb-4">
					<div class="col-12">
						<?php include_once $partialsPath . '_shops.php'; ?>
					</div>
				</div>
			</div>
			
			<?php include_once $partialsPath . '_advantages.php'; ?>
			<div class="container">
				<?php include_once $partialsPath . '_about-block.php'; ?>
				<?php include_once $partialsPath . '_reg-form.php'; ?>
			</div>
		</div>
		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>