<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
						
					</div>
					<div class="col-12 col-lg-9">
						<div class="row mb-3">
							<div class="col-9 pr-0">
								<a class="h1 h-link h-link_start" href=""><div class="around-link-icon around-link-icon_invert"><span class="icon-arrow"></span></div> Заказ №001803</a>
							</div>
							<div class="col-3 text-right">
								<div class="tags__item tags__item_no-active tags__item_gray mt-2">
									<span class="tags__label">Выполнен</span>
								</div>
							</div>
						</div>
						


						<div class="box m-fz-14">
							<div class="box__body">
								<div class="row lh_1_5 mt-0 mt-sm-3 mb-3">
									<div class="col-8 ">
										Магазин
									</div>
									<div class="col-4 text-right">
										<a href="" class="link">M.Видео</a>
									</div>
								</div>
								<div class="row lh_1_5 mb-sm-3 mb-2">
									<div class="col-8 ">
										Дата и время совершения заказа

									</div>
									<div class="col-4 text-right">
										12.08.2018 19:14
									</div>
								</div>
								<div class="row lh_1_5 mb-sm-3 mb-2">
									<div class="col-8 ">
										Стоимость заказа
									</div>
									<div class="col-4 text-right">
										21 990 ₽
									</div>
								</div>
								<div class="row lh_1_5 mb-0 mb-sm-3 mb-2">
									<div class="col-8 ">
										Кэшбэк
									</div>
									<div class="col-4 text-right fz-22">
										139 ₽
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<h2 class="h1 mb-4 pb-2">Товары</h2>
							</div>
							<div class="col-12">
								<div class="orders orders_product">
									<div class="orders__body">
										<div class="orders__item">
											<div class="row">
												<div class="col-8 d-flex align-items-start">
													<div class="orders__img-container"><img src="<?= $imagePath ?>products/mobile.png" alt="" class="orders__img"></div>
													<div>
														<p class="mb-2 mb-sm-3 color_black m-fz-14 orders__item-title">Система умный дом Nest BigBro 3000 (в комплект входит система автоматического управления)</p>
														<div class="text-small">24 490 ₽</div>
													</div>
													
												</div>
												<div class="col-4 text-right">
													<div class="color_black">кэшбэк</div>
													<div class="color_black fz-22">	
													95 ₽</div>
												</div>

											</div>
										</div>
										<div class="orders__item">
											<div class="row">
												<div class="col-8 d-flex align-items-start">
													<div class="orders__img-container"><img src="<?= $imagePath ?>products/nest.png" alt="" class="orders__img"></div>
													<div>
														<p class="mb-2 mb-sm-3 color_black m-fz-14 orders__item-title">Смартфон Apple iPhone X 256GB Space Gray (MQAF2RU/A)</p>
														<div class="text-small">149 490 ₽</div>
													</div>
													
												</div>
												<div class="col-4 text-right">
													<div class="color_black">кэшбэк</div>
													<div class="color_black fz-22">	
													44 ₽</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>