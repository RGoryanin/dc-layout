<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-sm-3';
		$pageTitle = 'Смартфон Samsung Galaxy S9 256Gb (королевский рубин)';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/card.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-12 col-sm-10">
							<h1><?php echo $pageTitle ?></h1>
						</div>
						<div class="col-12 col-sm-2">
							<a href="" class="link-brand text-left text-sm-right d-inline-block d-sm-block">
								<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="link-brand__img">
								<span class="link-brand__text">
									Все товары Samsung
								</span>
							</a>
						</div>
					</div>
				</div>
				
			</div>

			
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cards">
							<div class="cards__header">
								<div class="row">
									<div class="col-8">
                                        <div class="stars stars_view mt-0 d-inline-block align-middle">
                                            <div class="stars__items">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                            <div class="stars__items stars__items_active" style="width:40%">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                        </div>
						              	<div class="d-inline-block text-small color_gray_dark  align-middle ">
						              		<span class="d-none d-sm-inline">рейтинг на основании</span>
											<a class="link" href="">
												72 отзывов
											</a>
						              	</div>
									</div>
									<div class="col-4 text-right">
                                        <a href="#" class="btn-icon cards__compare" data-product-id="12324">
                                            <span class="icon-stats-bars"></span>
                                        </a>
										<a href="#" class="btn-icon cards__like">
											<span class="icon-heart"></span>
										</a>
									</div>	
								</div>
								
							</div>
							<div class="cards__body">
								<div class="row">
									<div class="col-12 col-sm-6 mb-3">
										<div class="cards__carousel c-carousel d-none d-lg-flex">
											<div class="c-carousel__preview">
												<div class="c-carousel__nav c-carousel__nav_prev scroll-prev"><span class="icon-arrow"></span></div>
												<div class="c-carousel__preview-container scroll-container">
													<div class="c-carousel__preview-item active"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-front-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png3">	
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-back-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-edge-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-all-side-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item active"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-front-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png3">	
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-back-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-edge-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-all-side-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item active"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-front-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png3">	
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-back-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-edge-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-all-side-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item active"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-front-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png3">	
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-back-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-edge-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
													<div class="c-carousel__preview-item"  data-img="<?= $imagePath ?>card/s9-front.png">
														<a href="#" class="c-carousel__preview-link"><img src="<?= $imagePath ?>card/s9-all-side-mini.png" alt="" class="c-carousel__preview-img"></a>
													</div>
												</div>
												<div class="c-carousel__nav c-carousel__nav_next scroll-next"><span class="icon-arrow"></span></div>
												
											</div>
											<div class="c-carousel__body">
												<div class="c-carousel__img-container">
													<img src="<?= $imagePath ?>card/s9-front.png" alt="" class="c-carousel__img">
												</div>
											</div>
										</div>
                                        <div class="card__product-images">
                                            <div class="card__product-images-wrapper owl-carousel">
                                                <img src="<?= $imagePath ?>card/s9-front.png" alt="">
                                                <img src="<?= $imagePath ?>card/s9-front.png" alt="">
                                                <img src="<?= $imagePath ?>card/s9-front.png" alt="">
                                                <img src="<?= $imagePath ?>card/s9-front.png" alt="">
                                                <img src="<?= $imagePath ?>card/s9-front.png" alt="">
                                            </div>
                                            <div class="cards__product-img">
                                                <img src="<?= $imagePath ?>card/s9-front.png" alt="">
                                            </div>
                                        </div>
									</div>
									<div class="col-12 col-sm-6 pl-lg-5 m-fz-14">
										<div class="cards__prices-block">
											<div class="tags__item tags__item_red">
												<span class="tags__label">Товар месяца</span>
											</div>
											<div class="cards__cashback">999 ₽ максимальный кэшбэк <span class="around-link-icon around-link-icon_mini" data-toggle="popover" data-trigger="focus" data-content="какой то текст в тултипчике" tabindex="-1">?</span></div>
											<div class="text-small lh_1_25 color_gray_dark">от МediaMarkt</div>
											<div class="pb-2"><span class="cards__price mr-3">49 990 ₽</span> <span class="cards__old-price color_gray_dark">54 490 ₽</span></div>
											<div class="row">
												<div class="col-7 col-sm-6"><button class="btn btn-primary btn-block">Купить</button></div>
												<div class="col-5 col-sm-6 pl-2"><button class="btn btn-link p-0 m-fz-14">Смотреть все цены</button></div>
											</div>
										</div>

										<h4>Характеристики</h4>
										<div class="cards__specifications">
											<ul class="specifications-list">
												<li class="specifications-list__item">смартфон с Android 7.0</li>
												<li class="specifications-list__item">3G, 4G LTE, LTE-A, Wi-Fi, Bluetooth, NFC, GPS, ГЛОНАСС</li>
												<li class="specifications-list__item">экран 5.8", разрешение 2960x1440</li>
												<li class="specifications-list__item">камера 12 МП, автофокус, F/1.7</li>
												<li class="specifications-list__item">объем оперативной памяти 4 Гб</li>
												<li class="specifications-list__item">аккумулятор 3000 мА⋅ч</li>
												<li class="specifications-list__item">вес 155 г, ШxВxТ 68.10x148.90x8 мм</li>
											</ul>
											<div class="cards__specifications-more text-small pt-1 pb-1">
												<a href="" class="link">Все характеристики</a>
											</div>
										</div>
										<h4 class="pt-4">Доступные цвета</h4>
										<div class="cards__color-list mt-3">
											<button class="btn-outline btn-outline_color mb-3 mr-3"><span class="btn-outline__color" style="background: #f1cca8;"></span>Желтый топаз</button>
											<button class="btn-outline btn-outline_color mb-3 mr-3 active"><span class="btn-outline__color" style="background: #851a1e;"></span>Королевский рубин</button>
											<button class="btn-outline btn-outline_color mb-3 mr-3"><span class="btn-outline__color" style="background: #b7b5ca;"></span>Металлический аметист</button>
											<button class="btn-outline btn-outline_color mb-3 mr-3"><span class="btn-outline__color" style="background: #111111;"></span>Черный бриллиант</button>
										</div>
										<h4 class="pt-4 ">Отзыв клиента</h4>
										<p class="text-small color_gray_dark lh_1_25 mb-2">Несколько дней пользуюсь аппаратом. Отличный экран, удобно лежит в руке. Ютуб, соцсети и сообщения, твиттер, фото, видео, Play music, телефон практически не выпускаю из рук и к вечеру остается…</p>
										<div class="pt-1"><a href="" class="link text-small">Полный отзыв</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
                        <ul class="nav nav-tabs nav-tabs--lefted nav-tabs--big" id="t-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="t-tab-1" data-toggle="tab" href="#t-content-1" role="tab" aria-controls="t-content-1" aria-selected="true">Все цены</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="t-tab-2" data-toggle="tab" href="#t-content-2" role="tab" aria-controls="t-content-2" aria-selected="false">Технические характеристики</a>
                            </li>
                        </ul>
					</div>
					<div class="col-12">
                        <div class="tab-content" id="t-tab-content">
                            <div class="tab-pane fade show active" id="t-content-1" role="tabpanel" aria-labelledby="t-tab-1">
                                <div class="prices-list prices-list_min">
                                        <div class="prices-list__container">
                                        <div class="prices-list__item">

                                            <div class="prices-list__colum_brand prices-list__colum">
                                                <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="prices-list__brand-img"></a>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_name">
                                                <div class="prices-list__name">М.Видео</div>
                                                <div class="prices-list__rating">
                                                    <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                        <form novalidate="novalidate">
                                                            <div class="stars__items">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                        </form>

                                                    </div>
                                                    <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                        72
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_description">
                                                <div class="offers-tags">
                                                    <div class="offers-tags__item">
                                                        ₽
                                                    </div>
                                                    <div class="offers-tags__item" data-toggle="popover" data-content="Все телефоны Samsung на уникальных условиях? В «MediaMarkt» возможно всё! Только до 30 сентября 2018 года удобная рассрочка 0-0-36" data-placement="top" data-trigger="hover">
                                                        %
                                                    </div>
                                                    <div class="offers-tags__item offers-tags__item_cart">
                                                        <span class="icon-card"><span class="path1"></span><span class="path2"></span></span>
                                                    </div>
                                                    <div class="offers-tags__item offers-tags__item_gift">
                                                        <img src="<?= $imagePath ?>/svg/gift.svg" alt="">
                                                    </div>
                                                </div>
                                                <p class="text-small color_gray_dark lh_1_25 m-0">Возможна доставка товаров<br>Есть самовывоз</p>

                                            </div>
                                            <div class="prices-list__colum prices-list__colum_prices">
                                                <div class="text-small text-sm-right">
                                                    <span class="prices-list__price">49 990 ₽</span>

                                                    <div class="prices-list__cashback text-truncate">
                                                        <div>
                                                            <div class="prices-list__cashback-icon">
                                                                <span class="icon-ic-plus"></span>
                                                            </div>
                                                            <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 999 ₽</span>
                                                        </div>
                                                        <button class="btn-buy d-inline-block d-sm-none">
                                                            <span class="icon-cart"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum d-none d-sm-block">
                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                    Купить
                                                </a>
                                            </div>
                                        </div>
                                        <div class="prices-list__item">
                                            <div class="prices-list__colum_brand prices-list__colum">
                                                <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="prices-list__brand-img"></a>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_name">
                                                <div class="prices-list__name">AliExpress</div>
                                                <div class="prices-list__rating">
                                                    <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                        <form novalidate="novalidate">
                                                            <div class="stars__items">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                        </form>

                                                    </div>
                                                    <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                        103
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_description">

                                                <p class="text-small color_gray_dark lh_1_25 m-0">
                                                    Бесплатная доставка товаров <br>почтой России
                                                </p>

                                            </div>
                                            <div class="prices-list__colum prices-list__colum_prices">
                                                <div class="text-small text-sm-right">
                                                    <span class="prices-list__price">51 490 ₽</span>

                                                    <div class="prices-list__cashback text-truncate">
                                                        <div>
                                                            <div class="prices-list__cashback-icon">
                                                                <span class="icon-ic-plus"></span>
                                                            </div>
                                                            <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 2 999 ₽</span>
                                                        </div>
                                                        <button class="btn-buy d-inline-block d-sm-none">
                                                            <span class="icon-cart"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum d-none d-sm-block">
                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                    Купить
                                                </a>
                                            </div>
                                        </div>
                                        <div class="prices-list__item">
                                            <div class="prices-list__colum_brand prices-list__colum">
                                                <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="prices-list__brand-img"></a>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_name">
                                                <div class="prices-list__name">Эльдорадо</div>
                                                <div class="prices-list__rating">
                                                    <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                        <form novalidate="novalidate">
                                                            <div class="stars__items">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                            <div class="stars__items stars__items_active" style="width:60%">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                        </form>

                                                    </div>
                                                    <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                        72
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_description">
                                                <div class="offers-tags">

                                                    <div class="offers-tags__item" data-toggle="popover" data-content="Все телефоны Samsung на уникальных условиях? В «MediaMarkt» возможно всё! Только до 30 сентября 2018 года удобная рассрочка 0-0-36" data-placement="top" data-trigger="click hover">
                                                        %
                                                    </div>
                                                    <div class="offers-tags__item offers-tags__item_cart">
                                                        <span class="icon-card"><span class="path1"></span><span class="path2"></span></span>
                                                    </div>
                                                </div>
                                                <p class="text-small color_gray_dark lh_1_25 m-0">Возможна доставка товаров<br>Есть самовывоз</p>

                                            </div>
                                            <div class="prices-list__colum prices-list__colum_prices">
                                                <div class="text-small text-sm-right">
                                                    <span class="prices-list__price">49 990 ₽</span>

                                                    <div class="prices-list__cashback text-truncate">
                                                        <div>
                                                            <div class="prices-list__cashback-icon">
                                                                <span class="icon-ic-plus"></span>
                                                            </div>
                                                            <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 999 ₽</span>
                                                        </div>
                                                        <button class="btn-buy d-inline-block d-sm-none">
                                                            <span class="icon-cart"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum d-none d-sm-block">
                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                    Купить
                                                </a>
                                            </div>
                                        </div>
                                        <div class="prices-list__item">
                                            <div class="prices-list__colum_brand prices-list__colum">
                                                <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/media-markt-logo.svg" alt="" class="prices-list__brand-img"></a>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_name">
                                                <div class="prices-list__name">MediaMarkt</div>
                                                <div class="prices-list__rating">
                                                    <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                        <form novalidate="novalidate">
                                                            <div class="stars__items">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                        </form>

                                                    </div>
                                                    <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                        103
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_description">

                                                <p class="text-small color_gray_dark lh_1_25 m-0">
                                                    Бесплатная доставка товаров <br>почтой России
                                                </p>

                                            </div>
                                            <div class="prices-list__colum prices-list__colum_prices">
                                                <div class="text-small text-sm-right">
                                                    <span class="prices-list__price">51 490 ₽</span>

                                                    <div class="prices-list__cashback text-truncate">
                                                        <div>
                                                            <div class="prices-list__cashback-icon">
                                                                <span class="icon-ic-plus"></span>
                                                            </div>
                                                            <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 2 999 ₽</span>
                                                        </div>
                                                        <button class="btn-buy d-inline-block d-sm-none">
                                                            <span class="icon-cart"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum d-none d-sm-block">
                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                    Купить
                                                </a>
                                            </div>
                                        </div>
                                        <div class="prices-list__item">
                                            <div class="prices-list__colum_brand prices-list__colum">
                                                <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="prices-list__brand-img"></a>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_name">
                                                <div class="prices-list__name">Samsung</div>
                                                <div class="prices-list__rating">
                                                    <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                        <form novalidate="novalidate">
                                                            <div class="stars__items">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                        </form>

                                                    </div>
                                                    <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                        103
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum prices-list__colum_description">

                                                <p class="text-small color_gray_dark lh_1_25 m-0">
                                                    Бесплатная доставка товаров <br>почтой России
                                                </p>

                                            </div>
                                            <div class="prices-list__colum prices-list__colum_prices">
                                                <div class="text-small text-sm-right">
                                                    <span class="prices-list__price">51 490 ₽</span>

                                                    <div class="prices-list__cashback text-truncate">
                                                        <div>
                                                            <div class="prices-list__cashback-icon">
                                                                <span class="icon-ic-plus"></span>
                                                            </div>
                                                            <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 2 999 ₽</span>
                                                        </div>
                                                        <button class="btn-buy d-inline-block d-sm-none">
                                                            <span class="icon-cart"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__colum d-none d-sm-block">
                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                    Купить
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="prices-list__footer text-small text-center">
                                            <a href="" class="link link_dark">Показать еще 8 магазинов</a>
                                        </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="t-content-2" role="tabpanel" aria-labelledby="t-tab-2">
                                <div class="characteristics">
                                        <div class="row">
                                            <div class="col-12 col-mb-6">
                                                <div class="characteristics__colum">
                                                    <div class="h4 mb-3">Общие характеристики</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип <span class="around-link-icon around-link-icon_mini" data-toggle="modal" data-target="#descModal" data-text="Частота процессора - это количество синхронизирующих импульсов в секунду. Количество тактов в секунду, не совпадает с фактическим количеством операций в секунду, выполняемых компьютером" data-trigger="hover">?</span></div>
                                                            <div class="col-6">смартфон</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Версия ОС</div>
                                                            <div class="col-6">Android 7.0</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип корпуса</div>
                                                            <div class="col-6">классический</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Конструкция</div>
                                                            <div class="col-6">водозащита</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип SIM-карты</div>
                                                            <div class="col-6">nano SIM</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Количество SIM-карт</div>
                                                            <div class="col-6">2</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Режим работы нескольких SIM-карт</div>
                                                            <div class="col-6">попеременный</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Поддержка MST</div>
                                                            <div class="col-6"></div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Вес</div>
                                                            <div class="col-6"></div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Размеры (ШxВxТ)</div>
                                                            <div class="col-6"></div>
                                                        </li>
                                                    </ul>
                                                    <div class="h4 mb-3">Экран</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип экрана</div>
                                                            <div class="col-6">цветной AMOLED, сенсорный</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип сенсорного экрана</div>
                                                            <div class="col-6">мультитач, емкостный</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Диагональ/div>
                                                                <div class="col-6">5.8 дюйм.</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Изогнутый экран</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Размер изображения</div>
                                                            <div class="col-6">2960x1440</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Число пикселей на дюйм (PPI)</div>
                                                            <div class="col-6">568</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Соотношение сторон</div>
                                                            <div class="col-6">18.5:9</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Автоматический поворот экрана</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Устойчивое к царапинам стекло</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                    </ul>
                                                    <div class="h4 mb-3">Звонки</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Световая индикация событий</div>
                                                            <div class="col-6">есть</div>
                                                        </li>

                                                    </ul>
                                                    <div class="h4 mb-3">Мультимедийные возможности</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тыловая фотокамера</div>
                                                            <div class="col-6">12 МП</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Фотовспышка</div>
                                                            <div class="col-6">тыльная, светодиодная</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Функции тыловой фотокамеры</div>
                                                            <div class="col-6">автофокус, оптическая стабилизация</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Диафрагма тыловой фотокамеры</div>
                                                            <div class="col-6">F/1.7</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Запись видеороликов</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Макс. разрешение видео</div>
                                                            <div class="col-6">3840x2160</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Макс. частота кадров видео</div>
                                                            <div class="col-6">30 кадров/с</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Фронтальная камера</div>
                                                            <div class="col-6">есть, 8 млн пикс.</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Аудио</div>
                                                            <div class="col-6">MP3, AAC, WAV, WMA</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Разъем для наушников</div>
                                                            <div class="col-6">3.5 мм</div>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="characteristics__colum">
                                                    <div class="h4 mb-3">Связь</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Стандарт</div>
                                                            <div class="col-6">GSM 900/1800/1900, 3G, 4G LTE, LTE-A Cat. 16, VoLTE</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Интерфейсы</div>
                                                            <div class="col-6">Wi-Fi 802.11ac, Bluetooth 5.0, USB, ANT+, NFC</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Спутниковая навигация</div>
                                                            <div class="col-6">GPS/ГЛОНАСС/BeiDou</div>
                                                        </li>
                                                    </ul>
                                                    <div class="h4 mb-3">Память и процессор</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Процессор</div>
                                                            <div class="col-6">Qualcomm Snapdragon 835 MSM8998</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Количество ядер процессора</div>
                                                            <div class="col-6">8</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Видеопроцессор</div>
                                                            <div class="col-6">Adreno 540</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Объем встроенной памяти</div>
                                                            <div class="col-6">64 Гб</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Объем оперативной памяти</div>
                                                            <div class="col-6">4 Гб</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Слот для карт памяти</div>
                                                            <div class="col-6">есть, объемом до 256 Гб, совмещенный с SIM-картой</div>
                                                        </li>
                                                    </ul>
                                                    <div class="h4 mb-3">Питание</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип аккумулятора</div>
                                                            <div class="col-6">Li-Ion</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Емкость аккумулятора</div>
                                                            <div class="col-6">3000 мА⋅ч</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Время работы в режиме разговора</div>
                                                            <div class="col-6">20 ч</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Время работы в режиме прослушивания музыки</div>
                                                            <div class="col-6">67 ч</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Тип разъема для зарядки</div>
                                                            <div class="col-6">USB Type-C</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Функция беспроводной зарядки</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Функция быстрой зарядки</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                    </ul>
                                                    <div class="h4 mb-3">Другие функции</div>
                                                    <ul class="characteristics__list p-0 text-small lh_1_25">
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Громкая связь (встроенный динамик)</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Управление</div>
                                                            <div class="col-6">голосовой набор, голосовое управление</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Режим полета</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Датчики</div>
                                                            <div class="col-6">освещенности, приближения, Холла, гироскоп, компас, барометр, считывание отпечатка пальца, сканер радужной оболочки глаза</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">Фонарик</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                        <li class="row">
                                                            <div class="col-6 color_gray_dark">USB-host</div>
                                                            <div class="col-6">есть</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
					</div>
					
				</div>
				<div class="box mb-5">

					<div class="row">
						<div class="col-12 col-lg-6 charts-list">
                            <div class="chart-block">
                                <canvas id="pricesChart"></canvas>
                            </div>
                            <div class="chart-desc">
                                <div class="chart-title">Средняя цена сегодня</div>
                                <div class="chart-price-wrapper">
                                    <div class="chart-middle-price">49 990 ₽</div>
                                    <div class="chart-prices-range">41 570 - 56 890</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 charts-list">
                            <div class="chart-block">
                                <canvas id="cashbackChart"></canvas>
                            </div>
                            <div class="chart-desc">
                                <div class="chart-title">Средний кэшбэк сегодня</div>
                                <div class="chart-price-wrapper">
                                    <div class="chart-middle-price">590 ₽</div>
                                    <div class="chart-prices-range">370 - 1200</div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<?php include_once $partialsPath . '_how-works.php'; ?>


			</div>
			<div class="video-block">
				<div class="container">
					<div class="row">
						<div class="col-12 mb-2 mb-sm-4">
							<h2 class="h1 color_white">Видеообзоры</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-10">
							<div class="video-block__video video">
                                <a href="https://www.youtube.com/watch?v=Y3g4xS9TfYU" class="video-link">
                                    <picture>
                                        <source srcset="https://i.ytimg.com/vi_webp/Y3g4xS9TfYU/maxresdefault.webp" type="image/webp">
                                        <img class="video-cover" src="https://i.ytimg.com/vi/Y3g4xS9TfYU/maxresdefault.jpg">
                                    </picture>
                                </a>
                                <button class="video-play" type="button" aria-label="Запустить видео">
                                    <svg width="68" height="48" viewBox="0 0 68 48">
                                        <path class="video-play_shape" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path>
                                        <path class="video-play_icon" d="M 45,24 27,14 27,34"></path>
                                    </svg>
                                </button>
							</div>
							<div class="video-block__name">
								Samsung Galaxy S9 Plus: все ли однозначно 2 месяца спустя?
							</div>
						</div>
						<div class="col-12 col-sm-2 pl-0">
							<div class="video-block__previews">
								<div class="video-block__previews-item active">
									<a href="" class="video-block__preview-link"><img src="<?= $imagePath ?>card/video-prev1.jpg" alt="" class="video-block__previews-img"></a>
								</div>
								<div class="video-block__previews-item">
									<a href="" class="video-block__previews-link"><img src="<?= $imagePath ?>card/video-prev2.jpg" alt="" class="video-block__previews-img"></a>
								</div>
								<div class="video-block__previews-item">
									<a href="" class="video-block__previews-link"><img src="<?= $imagePath ?>card/video-prev3.jpg" alt="" class="video-block__previews-img"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">	
				<div class="row">
					<div class="col-12 mb-4">
						<h2 class="h1">
							Отзывы
						</h2>
					</div>
					<div class="col-12">
                        <?php include_once $partialsPath . '_reviews.php'; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4">
						<h2 class="h1">Похожие предложения</h2>
					</div>
				</div>
				<?php include $partialsPath . '_products4.php'; ?>
                <?php include $partialsPath . '_products4.php'; ?>
				
				<?php include_once $partialsPath . '_collections-list.php'; ?>
			</div>
			<?php include_once $partialsPath . '_advantages.php'; ?>
			
			
		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>

        <div class="modal fade" id="descModal" tabindex="-1" role="dialog" aria-labelledby="Описание технических характеристик" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>
	</div>

    <div class="card__product-zoomer">
        <div class="card__product-zoomer-closer">X</div>
        <div class="card__product-zoomer-wrapper owl-carousel">
            <img src="<?= $imagePath ?>card/s9-front.png" alt="">
            <img src="<?= $imagePath ?>card/s9-front.png" alt="">
            <img src="<?= $imagePath ?>card/s9-front.png" alt="">
            <img src="<?= $imagePath ?>card/s9-front.png" alt="">
            <img src="<?= $imagePath ?>card/s9-front.png" alt="">
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script>
        let cbCharts = {
            charts: [
                {
                    element: document.getElementById('pricesChart').getContext('2d'),
                    colorHEX: '#fb5550',
                    colorRGBA: '251, 85, 80',
                    data: [12, 19, 3, 5, 6, 7]
                },
                {
                    element: document.getElementById('cashbackChart').getContext('2d'),
                    colorHEX: '#5c44c1',
                    colorRGBA: '92, 68, 193',
                    data: [22, 12, 3, 44, 6, 1]
                }
            ],

            init: function () {
                $.each(this.charts, function (index, chart) {
                    let bg = chart.element.createLinearGradient(0, 0,0, 170);
                    bg.addColorStop(0, 'rgba('+ chart.colorRGBA +', 0.52)');
                    bg.addColorStop(1, 'rgba('+ chart.colorRGBA +', 0)');

                    let chartQ = new Chart(chart.element, {
                        type: 'line',
                        data: {
                            labels: chart.data,
                            datasets: [{
                                label: '',
                                data: chart.data,
                                backgroundColor: bg,
                                borderColor: 'rgba('+ chart.colorRGBA +', 0.64)',
                                borderWidth: 2,
                                pointBackgroundColor: '#fff',
                                pointBorderColor: chart.colorHEX,
                                pointBorderWidth: 3
                            }]
                        },
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            title: {
                                display: false
                            },
                            legend: {
                                display: false
                            },
                            tooltips: {
                                enabled: false
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: false,
                                        offsetGridLines: true
                                    },
                                    display: false
                                }],
                                yAxes: [{
                                    gridLines: {
                                        display: false,
                                        offsetGridLines: true
                                    },
                                    display: false
                                }]
                            }
                        }
                    })
                });
            }
        };

        window.cbCharts = cbCharts.init();

        $('#descModal').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let recipient = button.data('text');
            let modal = $(this);

            modal.find('.modal-body').text(recipient);
        })
    </script>
</body>
</html>