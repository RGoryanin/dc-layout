<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Магазины';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/shops.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-10 col-lg-12">
							<h1><?php echo $pageTitle ?></h1>
						</div>
						<div class="col-2 d-lg-none text-right">
							<button class="btn btn-primary btn-primary_filter toggle-right-mobile">
								<span class="icon-filters"></span>
							</button>
						</div>
					</div>
				</div>
				
			</div>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-9 ">
						<div class="shops">
							<div class="row">
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
										</div>
										<span  class="shops__name">М.Видео</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
										</div>
										<span href="" class="shops__name">AliExpress</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												390 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Возможна доставка товаров Есть самовывоз
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Эльдорадо</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:60%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
										</div>
										<span  class="shops__name">М.Видео</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
										</div>
										<span href="" class="shops__name">AliExpress</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												390 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Возможна доставка товаров Есть самовывоз
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Эльдорадо</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:60%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
							</div>
						</div>
						
						<div class="slider">
                            <?php include_once $partialsPath . 'banners/banner-friday.php'; ?>
                        </div>
						
						<div class="shops">
							<div class="row">
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
										</div>
										<span  class="shops__name">М.Видео</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
										</div>
										<span href="" class="shops__name">AliExpress</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												390 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Возможна доставка товаров Есть самовывоз
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Эльдорадо</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:60%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
										</div>
										<span  class="shops__name">М.Видео</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
										</div>
										<span href="" class="shops__name">AliExpress</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												390 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Возможна доставка товаров Есть самовывоз
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Эльдорадо</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:60%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 col-lg-4 px-lg-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
							</div>
						</div>

						<div class="pagination mt-0">
							<ul class="pagination__list">
								<li class="pagination__item"><a href="" class="pagination__link">1</a></li>
								<li class="pagination__item pagination__item_around"><a href="" class="pagination__link pagination__item_arrow pagination__item_arrow-back"><span class="icon-arrow"></span></a></li>
								<li class="pagination__item"><a href="" class="pagination__link">4</a></li>
								<li class="pagination__item pagination__item_active"><span class="pagination__link">5</span></li>
								<li class="pagination__item"><a href="" class="pagination__link">6</a></li>
								<li class="pagination__item pagination__item_around pagination__item_arrow"><a href="" class="pagination__link"><span class="icon-arrow"></span></a></li>
								<li class="pagination__item"><a href="" class="pagination__link">99</a></li>
							</ul>
						</div>
					</div>
                    <?php include_once $partialsPath . '_shops-filters.php'; ?>
				</div>

			</div>
			
			<?php include_once $partialsPath . '_advantages.php'; ?>
			<div class="container">
				<?php include_once $partialsPath . '_about-block.php'; ?>
				<?php include_once $partialsPath . '_reg-form.php'; ?>
			</div>
			
		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>