<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
						
					</div>
					<div class="col-12 col-lg-9">
						<div class="box">
							<div class="box__body">
								<div class="row">
									<div class="col-12 col-sm-6 border-right paddin-border">
										<div class="row">
											<div class="col-5 col-sm-7">
												<div class="mb-1 m-fz-14">
													<span class="fw-600">На балансе</span> <span class="around-link-icon around-link-icon_mini">?</span>
												</div>
												<div class="h1">
													6 990 ₽
												</div>
											</div>
											<div class="col-7 col-sm-5 text-right">
												<button class="btn btn-primary pr-4 pl-4 mb-3 mt-2 text-truncate">Вывести</button>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 paddin-border">
										<div class="pl-sm-0">
											<div class="mb-1 m-fz-14">
												<span class="fw-600">Ожидает начисления</span> <span class="around-link-icon around-link-icon_mini">?</span>
											</div>
											<div class="h1">
												590 ₽
											</div>
										</div>
										
					
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<h2 class="h1 mb-3 pb-1">Вывод средств</h2>
							</div>
							<div class="col-12 mb-3 pb-2">
								<ul class="list-style">
									<li>Вывод средств с разных аккаунтов на одинаковые реквизиты невозможен.</li>
									<li>Указывайте только реальные, зарегистрированные данные.</li>
									<li>Ваша заявка будет обработана в течение 1-3 рабочих дней.</li>
								</ul>
							</div>
						</div>
						<div class="payment-list">
							<div class="row">
								<div class="col-12 col-sm-6 col-lg-4">
									<a href="" class="payment-list__item">
										<div class="payment-list__img-container">
											<img src="<?= $imagePath ?>svg/lk-pays/cards.svg" alt="" class="payment-list__img">
										</div>
										<div class="payment-list__description">
											<div class="row align-items-end">
												<div class="col-12 col-lg-8 pr-1">
													<div class="payment-list__name">
														Visa, MasterCard, МИР
													</div>
												</div>
												<div class="col-12 col-lg-4 pl-lg-0 text-lg-right">
													<div class="payment-list__sum">
														от 500 ₽ 
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-12 col-sm-6 col-lg-4">
									<a href="" class="payment-list__item">
										<div class="payment-list__img-container">
											<img src="<?= $imagePath ?>svg/lk-pays/mobile-pay.svg" alt="" class="payment-list__img">
										</div>
										<div class="payment-list__description">
											<div class="row align-items-end">
												<div class="col-12 col-lg-8 pr-1">
													<div class="payment-list__name">
														Счет мобильного телефона
													</div>
												</div>
												<div class="col-12 col-lg-4 pl-lg-0 text-lg-right">
													<div class="payment-list__sum">
														от 500 ₽ 
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-12 col-sm-6 col-lg-4">
									<a href="" class="payment-list__item ">
										<div class="payment-list__img-container">
											<img src="<?= $imagePath ?>svg/lk-pays/yandex-pay-logo.svg" alt="" class="payment-list__img">
										</div>
										<div class="payment-list__description">
											<div class="row align-items-end">
												<div class="col-12 col-lg-8 pr-1">
													<div class="payment-list__name">
														Яндекс.Деньги
													</div>
												</div>
												<div class="col-12 col-lg-4 pl-lg-0 text-lg-right">
													<div class="payment-list__sum">
														от 1 ₽ 
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-12 col-sm-6 col-lg-4">
									<a href="" class="payment-list__item">
										<div class="payment-list__img-container">
											<img src="<?= $imagePath ?>svg/lk-pays/web-money-logo.svg" alt="" class="payment-list__img">
										</div>
										<div class="payment-list__description">
											<div class="row align-items-end">
												<div class="col-12 col-lg-8 pr-1">
													<div class="payment-list__name">
														WebMoney
													</div>
												</div>
												<div class="col-12 col-lg-4 pl-lg-0 text-lg-right">
													<div class="payment-list__sum">
														от 1 ₽ 
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-12 col-sm-6 col-lg-4">
									<a href="" class="payment-list__item">
										<div class="payment-list__img-container">
											<img src="<?= $imagePath ?>svg/lk-pays/qiwi-logo.svg" alt="" class="payment-list__img">
										</div>
										<div class="payment-list__description">
											<div class="row align-items-end">
												<div class="col-12 col-lg-8 pr-1">
													<div class="payment-list__name">
														QIWI Кошелек
													</div>
												</div>
												<div class="col-12 col-lg-4 pl-lg-0 text-lg-right">
													<div class="payment-list__sum">
														от 500 ₽ 
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-12 col-sm-6 col-lg-4">
									<a href="" class="payment-list__item">
										<div class="payment-list__img-container">
											<img src="<?= $imagePath ?>svg/lk-pays/pay-pal-logo.svg" alt="" class="payment-list__img">
										</div>
										<div class="payment-list__description">
											<div class="row align-items-end">
												<div class="col-12 col-lg-8 pr-1">
													<div class="payment-list__name">
														PayPal
													</div>
												</div>
												<div class="col-12 col-lg-4 pl-lg-0 text-lg-right">
													<div class="payment-list__sum">
														от 500 ₽ 
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>