<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$pageTitle = 'Смартфон Samsung Galaxy S9 256Gb (королевский рубин)';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/card.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1><?php echo $pageTitle ?></h1>
						</div>

					</div>
				</div>
				
			</div>

			
			<div class="container">

				<div class="row">
					<div class="col-12 ">
						<div class="block-around-border">
                            <ul class="nav nav-tabs nav-tabs--lefted" id="m-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="m-tab-1" data-toggle="tab" href="#m-content-1" role="tab" aria-controls="m-content-1" aria-selected="true">Все цены</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="m-tab-2" data-toggle="tab" href="#m-content-2" role="tab" aria-controls="m-content-2" aria-selected="false">Технические характеристики</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="m-tab-3" data-toggle="tab" href="#m-content-3" role="tab" aria-controls="m-content-3" aria-selected="false">Отзывы</a>
                                </li>
                            </ul>
						</div>
					</div>
					<div class="col-12">
                        <div class="tab-content" id="m-tab-content">
                            <div class="tab-pane fade show active" id="m-content-1" role="tabpanel" aria-labelledby="m-tab-1">
                                <div class="row">
                                    <div class="col-12 col-lg-9">
                                        <div class="prices-list prices-list_min">
                                            <div class="prices-list__header">

                                                <div class="row">
                                                    <div class="col-9 col-lg-12 align-items-center">
                                                        <div class="sort__item">
                                                            <div class="form-checkbox">
                                                                <input type="checkbox" name="" id="checkbox1" class="form-checkbox__input">
                                                                <label for="checkbox1" class="form-checkbox__label">Показывать цену с учетом кэшбэка </label>
                                                            </div>
                                                        </div>
                                                        <div class="sort__item d-none d-lg-inline-block">
                                                            <label for="" class="sort__label">Сортировать:</label>
                                                            <div class="d-inline-block" style="vertical-align: bottom">
                                                                <select class="select" name="">
                                                                    <option>по популярности</option>
                                                                    <option>по цене</option>
                                                                    <option selected="selected">еще по какой-нибудь фигне</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-3 text-right d-lg-none">
                                                        <button class="d-inline-block d-lg-none btn btn-primary btn-primary_filter toggle-right-mobile">
                                                            <span class="icon-filters"></span>
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="prices-list__container">
                                                <div class="prices-list__item">

                                                    <div class="prices-list__colum_brand prices-list__colum">
                                                        <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="prices-list__brand-img"></a>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_name">
                                                        <div class="prices-list__name">М.Видео</div>
                                                        <div class="prices-list__rating">
                                                            <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                                <form novalidate="novalidate">
                                                                    <div class="stars__items">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                    <div class="stars__items stars__items_active" style="width:40%">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                            <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                                72
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_description">
                                                        <div class="offers-tags">
                                                            <div class="offers-tags__item">
                                                                ₽
                                                            </div>
                                                            <div class="offers-tags__item" data-toggle="popover" data-content="Все телефоны Samsung на уникальных условиях? В «MediaMarkt» возможно всё! Только до 30 сентября 2018 года удобная рассрочка 0-0-36" data-placement="top" data-trigger="hover">
                                                                %
                                                            </div>
                                                            <div class="offers-tags__item offers-tags__item_cart">
                                                                <span class="icon-card"><span class="path1"></span><span class="path2"></span></span>
                                                            </div>
                                                            <div class="offers-tags__item offers-tags__item_gift">
                                                                <img src="<?= $imagePath ?>/svg/gift.svg" alt="">
                                                            </div>
                                                        </div>
                                                        <p class="text-small color_gray_dark lh_1_25 m-0">Возможна доставка товаров<br>Есть самовывоз</p>

                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_prices">
                                                        <div class="text-small text-sm-right">
                                                            <span class="prices-list__price">49 990 ₽</span>

                                                            <div class="prices-list__cashback text-truncate">
                                                                <div>
                                                                    <div class="prices-list__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 999 ₽</span>
                                                                </div>
                                                                <button class="btn-buy d-inline-block d-sm-none">
                                                                    <span class="icon-cart"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum d-none d-sm-block">
                                                        <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                            Купить
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="prices-list__item">
                                                    <div class="prices-list__colum_brand prices-list__colum">
                                                        <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="prices-list__brand-img"></a>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_name">
                                                        <div class="prices-list__name">AliExpress</div>
                                                        <div class="prices-list__rating">
                                                            <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                                <form novalidate="novalidate">
                                                                    <div class="stars__items">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                    <div class="stars__items stars__items_active" style="width:40%">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                            <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                                103
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_description">

                                                        <p class="text-small color_gray_dark lh_1_25 m-0">
                                                            Бесплатная доставка товаров <br>почтой России
                                                        </p>

                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_prices">
                                                        <div class="text-small text-sm-right">
                                                            <span class="prices-list__price">51 490 ₽</span>

                                                            <div class="prices-list__cashback text-truncate">
                                                                <div>
                                                                    <div class="prices-list__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 2 999 ₽</span>
                                                                </div>
                                                                <button class="btn-buy d-inline-block d-sm-none">
                                                                    <span class="icon-cart"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum d-none d-sm-block">
                                                        <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                            Купить
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="prices-list__item">
                                                    <div class="prices-list__colum_brand prices-list__colum">
                                                        <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="prices-list__brand-img"></a>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_name">
                                                        <div class="prices-list__name">Эльдорадо</div>
                                                        <div class="prices-list__rating">
                                                            <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                                <form novalidate="novalidate">
                                                                    <div class="stars__items">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                    <div class="stars__items stars__items_active" style="width:60%">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                            <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                                72
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_description">
                                                        <div class="offers-tags">

                                                            <div class="offers-tags__item" data-toggle="popover" data-content="Все телефоны Samsung на уникальных условиях? В «MediaMarkt» возможно всё! Только до 30 сентября 2018 года удобная рассрочка 0-0-36" data-placement="top" data-trigger="click hover">
                                                                %
                                                            </div>
                                                            <div class="offers-tags__item offers-tags__item_cart">
                                                                <span class="icon-card"><span class="path1"></span><span class="path2"></span></span>
                                                            </div>
                                                        </div>
                                                        <p class="text-small color_gray_dark lh_1_25 m-0">Возможна доставка товаров<br>Есть самовывоз</p>

                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_prices">
                                                        <div class="text-small text-sm-right">
                                                            <span class="prices-list__price">49 990 ₽</span>

                                                            <div class="prices-list__cashback text-truncate">
                                                                <div>
                                                                    <div class="prices-list__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 999 ₽</span>
                                                                </div>
                                                                <button class="btn-buy d-inline-block d-sm-none">
                                                                    <span class="icon-cart"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum d-none d-sm-block">
                                                        <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                            Купить
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="prices-list__item">
                                                    <div class="prices-list__colum_brand prices-list__colum">
                                                        <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/media-markt-logo.svg" alt="" class="prices-list__brand-img"></a>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_name">
                                                        <div class="prices-list__name">MediaMarkt</div>
                                                        <div class="prices-list__rating">
                                                            <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                                <form novalidate="novalidate">
                                                                    <div class="stars__items">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                    <div class="stars__items stars__items_active" style="width:40%">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                            <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                                103
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_description">

                                                        <p class="text-small color_gray_dark lh_1_25 m-0">
                                                            Бесплатная доставка товаров <br>почтой России
                                                        </p>

                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_prices">
                                                        <div class="text-small text-sm-right">
                                                            <span class="prices-list__price">51 490 ₽</span>

                                                            <div class="prices-list__cashback text-truncate">
                                                                <div>
                                                                    <div class="prices-list__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 2 999 ₽</span>
                                                                </div>
                                                                <button class="btn-buy d-inline-block d-sm-none">
                                                                    <span class="icon-cart"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum d-none d-sm-block">
                                                        <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                            Купить
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="prices-list__item">
                                                    <div class="prices-list__colum_brand prices-list__colum">
                                                        <a href="" class="prices-list__brand-link"><img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="prices-list__brand-img"></a>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_name">
                                                        <div class="prices-list__name">Samsung</div>
                                                        <div class="prices-list__rating">
                                                            <div class="stars stars_red stars_view d-inline-block mt-0 align-middle">
                                                                <form novalidate="novalidate">
                                                                    <div class="stars__items">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                    <div class="stars__items stars__items_active" style="width:40%">
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                        <span class="stars__item"><span class="icon-star"></span></span>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                            <div class="d-inline-block text-small color_gray_dark  align-middle ">
                                                                103
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_description">

                                                        <p class="text-small color_gray_dark lh_1_25 m-0">
                                                            Бесплатная доставка товаров <br>почтой России
                                                        </p>

                                                    </div>
                                                    <div class="prices-list__colum prices-list__colum_prices">
                                                        <div class="text-small text-sm-right">
                                                            <span class="prices-list__price">51 490 ₽</span>

                                                            <div class="prices-list__cashback text-truncate">
                                                                <div>
                                                                    <div class="prices-list__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="prices-list__cashback-text">кешбек<span class="d-none d-sm-inline"> до</span> 2 999 ₽</span>
                                                                </div>
                                                                <button class="btn-buy d-inline-block d-sm-none">
                                                                    <span class="icon-cart"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="prices-list__colum d-none d-sm-block">
                                                        <a href="#" class="btn btn-outline btn-outline-primary btn-block">
                                                            Купить
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="prices-list__footer text-small text-center">
                                                <a href="" class="link link_dark">Показать еще 8 магазинов</a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php include_once $partialsPath . '_filters.php'; ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="m-content-2" role="tabpanel" aria-labelledby="m-tab-2">
                                <div class="characteristics">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="characteristics__colum">
                                                <div class="h4 mb-3">Общие характеристики</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип <span class="around-link-icon around-link-icon_mini" data-toggle="popover" data-content="Частота процессора - это количество синхронизирующих импульсов в секунду. Количество тактов в секунду, не совпадает с фактическим количеством операций в секунду, выполняемых компьютером" data-trigger="hover">?</span></div>
                                                        <div class="col-6">смартфон</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Версия ОС</div>
                                                        <div class="col-6">Android 7.0</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип корпуса</div>
                                                        <div class="col-6">классический</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Конструкция</div>
                                                        <div class="col-6">водозащита</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип SIM-карты</div>
                                                        <div class="col-6">nano SIM</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Количество SIM-карт</div>
                                                        <div class="col-6">2</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Режим работы нескольких SIM-карт</div>
                                                        <div class="col-6">попеременный</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Поддержка MST</div>
                                                        <div class="col-6"></div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Вес</div>
                                                        <div class="col-6"></div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Размеры (ШxВxТ)</div>
                                                        <div class="col-6"></div>
                                                    </li>
                                                </ul>
                                                <div class="h4 mb-3">Экран</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип экрана</div>
                                                        <div class="col-6">цветной AMOLED, сенсорный</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип сенсорного экрана</div>
                                                        <div class="col-6">мультитач, емкостный</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Диагональ/div>
                                                            <div class="col-6">5.8 дюйм.</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Изогнутый экран</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Размер изображения</div>
                                                        <div class="col-6">2960x1440</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Число пикселей на дюйм (PPI)</div>
                                                        <div class="col-6">568</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Соотношение сторон</div>
                                                        <div class="col-6">18.5:9</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Автоматический поворот экрана</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Устойчивое к царапинам стекло</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                </ul>
                                                <div class="h4 mb-3">Звонки</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Световая индикация событий</div>
                                                        <div class="col-6">есть</div>
                                                    </li>

                                                </ul>
                                                <div class="h4 mb-3">Мультимедийные возможности</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тыловая фотокамера</div>
                                                        <div class="col-6">12 МП</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Фотовспышка</div>
                                                        <div class="col-6">тыльная, светодиодная</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Функции тыловой фотокамеры</div>
                                                        <div class="col-6">автофокус, оптическая стабилизация</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Диафрагма тыловой фотокамеры</div>
                                                        <div class="col-6">F/1.7</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Запись видеороликов</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Макс. разрешение видео</div>
                                                        <div class="col-6">3840x2160</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Макс. частота кадров видео</div>
                                                        <div class="col-6">30 кадров/с</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Фронтальная камера</div>
                                                        <div class="col-6">есть, 8 млн пикс.</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Аудио</div>
                                                        <div class="col-6">MP3, AAC, WAV, WMA</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Разъем для наушников</div>
                                                        <div class="col-6">3.5 мм</div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="characteristics__colum">
                                                <div class="h4 mb-3">Связь</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Стандарт</div>
                                                        <div class="col-6">GSM 900/1800/1900, 3G, 4G LTE, LTE-A Cat. 16, VoLTE</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Интерфейсы</div>
                                                        <div class="col-6">Wi-Fi 802.11ac, Bluetooth 5.0, USB, ANT+, NFC</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Спутниковая навигация</div>
                                                        <div class="col-6">GPS/ГЛОНАСС/BeiDou</div>
                                                    </li>
                                                </ul>
                                                <div class="h4 mb-3">Память и процессор</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Процессор</div>
                                                        <div class="col-6">Qualcomm Snapdragon 835 MSM8998</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Количество ядер процессора</div>
                                                        <div class="col-6">8</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Видеопроцессор</div>
                                                        <div class="col-6">Adreno 540</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Объем встроенной памяти</div>
                                                        <div class="col-6">64 Гб</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Объем оперативной памяти</div>
                                                        <div class="col-6">4 Гб</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Слот для карт памяти</div>
                                                        <div class="col-6">есть, объемом до 256 Гб, совмещенный с SIM-картой</div>
                                                    </li>
                                                </ul>
                                                <div class="h4 mb-3">Питание</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип аккумулятора</div>
                                                        <div class="col-6">Li-Ion</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Емкость аккумулятора</div>
                                                        <div class="col-6">3000 мА⋅ч</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Время работы в режиме разговора</div>
                                                        <div class="col-6">20 ч</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Время работы в режиме прослушивания музыки</div>
                                                        <div class="col-6">67 ч</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Тип разъема для зарядки</div>
                                                        <div class="col-6">USB Type-C</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Функция беспроводной зарядки</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Функция быстрой зарядки</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                </ul>
                                                <div class="h4 mb-3">Другие функции</div>
                                                <ul class="characteristics__list p-0 text-small lh_1_25">
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Громкая связь (встроенный динамик)</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Управление</div>
                                                        <div class="col-6">голосовой набор, голосовое управление</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Режим полета</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Датчики</div>
                                                        <div class="col-6">освещенности, приближения, Холла, гироскоп, компас, барометр, считывание отпечатка пальца, сканер радужной оболочки глаза</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">Фонарик</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6 color_gray_dark">USB-host</div>
                                                        <div class="col-6">есть</div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="m-content-3" role="tabpanel" aria-labelledby="m-tab-3">
                                <?php include_once $partialsPath . '_reviews.php'; ?>
                            </div>
					    </div>
				    </div>
			    </div>

                <?php include $partialsPath . 'banners/banner-samsung.php'; ?>
                <?php include_once $partialsPath . '_collections-list.php'; ?>
            </div>

			<?php include_once $partialsPath . '_advantages.php'; ?>

			<div class="container">
				<?php include_once $partialsPath . '_about-block.php'; ?>
				<?php include_once $partialsPath . '_reg-form.php'; ?>
			</div>
		</div>

		<?php include_once $partialsPath . '_footer.php'; ?>
	</div>
</body>
</html>