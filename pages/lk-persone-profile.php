<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
						
						
					</div>

					<div class="col-12 col-lg-9">
						
						<div class="box box_padding">
							<div class="box__body">
								<form action="">
									<div class="row">
										<div class="col-12 col-sm-6 m-fz-14">
											<div class="form-group">
											    <label class="fw-600">ID</label>
											    <input type="text" class="form-control" placeholder="" disabled value="69023045">
										  	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 m-fz-14">
											<div class="form-group">
											    <label class="fw-600">Электронная почта</label>
											    <div class="position-relative">
											    	<input type="email" class="form-control" placeholder="" disabled value="roman.pavlichuk@gmail.com">
											    	<button class="btn btn-icon form-control__btn"><span class="icon-interface"></span></button>
											    </div>
										  	</div>
										</div>

									</div>
									<div class="row">
										<div class="col-12 col-sm-6 m-fz-14">
											<div class="form-group">
												<label class="fw-600">Имя на сайте</label>
											    
												 <div class="position-relative">
											    	<input type="text" class="form-control" placeholder="" disabled value="Roman Pavlichuk">
											    	<button class="btn btn-icon form-control__btn"><span class="icon-interface"></span></button>
											    </div>
											    
										  	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 m-fz-14">
											<div class="form-group">
												<label class="fw-600">Дата рождения</label>
												<div class="position-relative">
													<div class="date-picker btn-group d-flex">
											    		<input type="text" placeholder="01.01.1980" class="form-control date-picker__input">
											    		<button class="btn btn-primary"><span class="icon-tick-disabled"></span></button>
											    		
											    	</div>
													<button class="date-picker__icon btn btn-icon">
										    			<span class="icon-clock"></span>
										    		</button>
												</div>
										    	
											    
										  	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 m-fz-14">
											<div class="form-group">
											    <label class="fw-600">Сумма вывода (₽)</label>
											    <div class="form-item mt-1">
													<div class="form-radio d-inline-block mr-4">
														<input type="radio" name="sex" id="radio1" class="form-radio__input">
														<label for="radio1" class="form-radio__label">Мужской </label>
													</div>
													<div class="form-radio d-inline-block">
														<input type="radio" name="sex" id="radio2" class="form-radio__input">
														<label for="radio2" class="form-radio__label">Женский </label>
													</div>
												</div>
										  	</div>
										</div>
										<div class="col-12 mb-4 pb-2"><div class="border-bottom"></div></div>
									</div>
									<div class="row">
										<div class="col-12 m-fz-14">
											<div class="form-group">
											    <label class="fw-600 d-block">Добавить телефон</label>
											    <p>Номер мобильного телефона – это дополнительный способ защиты вашего аккаунта с помощью которого вы сможете подтверждать вывод средств.</p>
												<button type="button" class="btn btn-primary mb-3 mr-3 px-4" data-toggle="modal" data-target="#addPhone">Добавить телефон</button>
														
										  	</div>
										</div>
										<div class="col-12 mb-4 pb-2"><div class="border-bottom"></div></div>
									</div>
									<div class="row">
										<div class="col-12 m-fz-14">
											<div class="form-group">
											    <label class="fw-600 d-block">Пароль</label>
											    <p>Смените пароль, если это необходимо.</p>
												<button type="button" class="btn btn-primary mb-3 px-4" data-toggle="modal" data-target="#addPass">Сменить пароль</button>
														
										  	</div>
										</div>
										<div class="col-12 mb-4 pb-2"><div class="border-bottom"></div></div>
									</div>
									<div class="row">
										<div class="col-12">
											<div class="form-group m-fz-14">
											    <label class="fw-600 d-block">Социальные сети</label>
											    <p>Привяжите социальные сети для моментального входа в аккаунт.</p>
												
												<ul class="social-reg-list social-reg-list_lk">
													<li class="social-reg-list__item social-reg-list__item_vk active">
														<button class="btn-icon social-reg-list__close">
															<span class="icon-x"></span>
														</button>
														<a href="" class="social-reg-list__link"><span class="icon-social-vk"></span></a></li>
													<li class="social-reg-list__item social-reg-list__item_fb active">
														<button class="btn-icon social-reg-list__close">
															<span class="icon-x"></span>
														</button>
														<a href="" class="social-reg-list__link"><span class="icon-social-facebook"></span></a>
													</li>
													<li class="social-reg-list__item social-reg-list__item_ok">
														<button class="btn-icon social-reg-list__close">
															<span class="icon-x"></span>
														</button>
														<a href="" class="social-reg-list__link"><span class="icon-social-ok"></span></a>
													</li>
													<li class="social-reg-list__item social-reg-list__item_g">
														<button class="btn-icon social-reg-list__close">
															<span class="icon-x"></span>
														</button>
														<a href="" class="social-reg-list__link"><span class="icon-social-gplus"></span></a>
													</li>
												</ul>	
										  	</div>
										</div>
										
									</div>
								</form>
								
							</div>
						</div>

					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>