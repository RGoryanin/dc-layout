<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include_once '../variables.php';
    include_once $partialsPath . '_head.php';
    $productsGrid = 'col-sm-3 col-lg-4';
    $pageTitle = 'Сравнение';
    ?>
    <link rel="stylesheet" href="<?= $cssPath ?>pages/compare.css?v=<?=Date('t' )?>">

</head>
<body>
    <div class="page">
        <?php include_once $partialsPath . '_header.php'; ?>
        <div class="body">
            <?php include_once $partialsPath . '_breadcrumbs.php'; ?>
            <div class="page__title">
                <div class="container">
                    <div class="row">
                        <div class="col-6 col-sm-10">
                            <h1><?php echo $pageTitle ?></h1>
                        </div>
                    </div>
                </div>

            </div>

            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 ">
                                <ul class="nav nav-tabs nav-tabs--lefted" id="m-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active"
                                           id="m-tab-1"
                                           data-toggle="tab"
                                           href="#m-content-1"
                                           role="tab"
                                           aria-controls="m-content-1"
                                           aria-selected="true">
                                            Смартфоны
                                            <span>5</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link"
                                           id="m-tab-2"
                                           data-toggle="tab"
                                           href="#m-content-2"
                                           role="tab"
                                           aria-controls="m-content-2"
                                           aria-selected="false">
                                            Планшеты
                                            <span>2</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="tab-content" id="m-tab-content">
                                    <div class="tab-pane fade show active compare__list" id="m-content-1" role="tabpanel" aria-labelledby="m-tab-1">
                                        <div class="product__list-wrapper">
                                            <div class="container">
                                                <button class="compare__list-arrow compare__list-arrow--prev disabled"></button>
                                                <button class="compare__list-arrow compare__list-arrow--next"></button>
                                                <div class="product__list">
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/blender.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Зеркальный фотоаппарат Canon Mark II</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/nest.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Зеркальный фотоаппарат Canon Mark II</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/mobile.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/photocam.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/photocam.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/photocam.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/photocam.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 pr-0">
                                                <div class="actions-item">
                                                    <div class="form-checkbox">
                                                        <input type="checkbox" id="checkbox-1" class="form-checkbox__input">
                                                        <label for="checkbox-1" class="form-checkbox__label">
                                                            <span class="d-none d-lg-block">Показывать только отличающиеся параметры</span>
                                                            <span class="d-block d-lg-none">Только отличия</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 pl-0">
                                                <div class="actions-item text-right">
                                                    <div class="delete-action">
                                                        <div class="icon-trash"><span></span></div>
                                                        Очистить список
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="characteristic__list">
                                            <div class="accordion" id="characteristicsAccordion-1">
                                                <div class="card">
                                                    <div class="card-header" id="characteristic-header-1" data-toggle="collapse" data-target="#characteristic-body-1" aria-expanded="true" aria-controls="characteristic-body-1">
                                                        Основные
                                                    </div>
                                                    <div class="collapse show" id="characteristic-body-1" aria-labelledby="characteristic-header-1">
                                                        <div class="characteristic__row characteristic__row--diffs">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">
                                                                    <div class="offers-tags">
                                                                        <div class="offers-tags__item">
                                                                            <span class="icon-rouble"></span>
                                                                        </div>
                                                                        <div class="offers-tags__item">
                                                                            %
                                                                        </div>
                                                                        <div class="offers-tags__item offers-tags__item_cart">
                                                                            <span class="icon-credit-card"></span>
                                                                        </div>
                                                                        <div class="offers-tags__item offers-tags__item_gift">
                                                                            <img src="/web/src/img/svg/gift.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">
                                                                    <div class="offers-tags">
                                                                        <div class="offers-tags__item">
                                                                            <span class="icon-rouble"></span>
                                                                        </div>
                                                                        <div class="offers-tags__item">
                                                                            %
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row characteristic__row--diffs">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">123</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">11</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">21</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">11</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">21</div>
                                                            </div>

                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">31</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">31</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="characteristic-header-2" data-toggle="collapse" data-target="#characteristic-body-2" aria-expanded="true" aria-controls="characteristic-body-2">
                                                        Общие характеристики
                                                    </div>
                                                    <div class="collapse show" id="characteristic-body-2" aria-labelledby="characteristic-header-2">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">Android 7.0</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">iOS 12</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">Android 7.0</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">iOS 12</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">Android 7.0</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">iOS 12</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">iOS 12</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">раскладной</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">раскладной</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">постоянный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">постоянный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header collapsed" id="characteristic-header-3" data-toggle="collapse" data-target="#characteristic-body-3" aria-expanded="true" aria-controls="characteristic-body-3">
                                                        Экран
                                                    </div>
                                                    <div class="collapse" id="characteristic-body-3" aria-labelledby="characteristic-header-3">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">постоянный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">постоянный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header collapsed" id="characteristic-header-4" data-toggle="collapse" data-target="#characteristic-body-4" aria-expanded="true" aria-controls="characteristic-body-4">
                                                        Мультимедийные возможности
                                                    </div>
                                                    <div class="collapse" id="characteristic-body-4" aria-labelledby="characteristic-header-4">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="characteristic-header-5" data-toggle="collapse" data-target="#characteristic-body-5" aria-expanded="true" aria-controls="characteristic-body-5">
                                                        Питание
                                                    </div>
                                                    <div class="collapse show" id="characteristic-body-5" aria-labelledby="characteristic-header-5">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade compare__list" id="m-content-2" role="tabpanel" aria-labelledby="m-tab-2">
                                        <div class="product__list-wrapper">
                                            <div class="container">
                                                <div class="product__list">
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/blender.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Зеркальный фотоаппарат Canon Mark II</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/nest.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Зеркальный фотоаппарат Canon Mark II</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product__list-item">
                                                        <div class="product__header">
                                                            <a href="#" class="product-img-wrapper">
                                                                <img src="<?= $imagePath ?>products/mobile.png" alt="" class="product-img">
                                                            </a>
                                                            <a href="#" class="product-title">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                        </div>
                                                        <div class="product__body">
                                                            <div class="product__prices">
                                                                <div class="product__price-range">
                                                                    <span class="product__price-from">89 490 ₽</span>
                                                                    <span class="product__price-to">— 104 990 ₽ </span>
                                                                </div>
                                                                <div class="product__cashback">
                                                                    <div class="product__cashback-icon">
                                                                        <span class="icon-ic-plus"></span>
                                                                    </div>
                                                                    <span class="product__cashback-text">кешбек до 999 ₽</span>
                                                                </div>
                                                            </div>
                                                            <div class="product__buy">
                                                                <a href="#" class="btn btn-outline btn-outline-primary btn-block">Купить</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 pr-0">
                                                <div class="actions-item">
                                                    <div class="form-checkbox">
                                                        <input type="checkbox" id="checkbox-2" class="form-checkbox__input">
                                                        <label for="checkbox-2" class="form-checkbox__label">
                                                            <span class="d-none d-lg-block">Показывать только отличающиеся параметры</span>
                                                            <span class="d-block d-lg-none">Только отличия</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 pl-0">
                                                <div class="actions-item text-right">
                                                    <div class="delete-action">
                                                        <div class="icon-trash"><span></span></div>
                                                        Очистить список
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="characteristic__list">
                                            <div class="accordion" id="characteristicsAccordion-1">
                                                <div class="card">
                                                    <div class="card-header" id="characteristic-header-1" data-toggle="collapse" data-target="#characteristic-body-1" aria-expanded="true" aria-controls="characteristic-body-1">
                                                        Основные
                                                    </div>
                                                    <div class="collapse show" id="characteristic-body-1" aria-labelledby="characteristic-header-1">
                                                        <div class="characteristic__row characteristic__row--diffs">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">
                                                                    <div class="offers-tags">
                                                                        <div class="offers-tags__item">
                                                                            <span class="icon-rouble"></span>
                                                                        </div>
                                                                        <div class="offers-tags__item">
                                                                            %
                                                                        </div>
                                                                        <div class="offers-tags__item offers-tags__item_cart">
                                                                            <span class="icon-credit-card"></span>
                                                                        </div>
                                                                        <div class="offers-tags__item offers-tags__item_gift">
                                                                            <img src="/web/src/img/svg/gift.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Акции</div>
                                                                <div class="characteristic-value">
                                                                    <div class="offers-tags">
                                                                        <div class="offers-tags__item">
                                                                            <span class="icon-rouble"></span>
                                                                        </div>
                                                                        <div class="offers-tags__item">
                                                                            %
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row characteristic__row--diffs">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">123</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">11</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Отзывы</div>
                                                                <div class="characteristic-value">21</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Рейтинг</div>
                                                                <div class="characteristic-value">
                                                                    <div class="stars stars_view d-inline-block align-middle">
                                                                        <form novalidate="novalidate">
                                                                            <div class="stars__items">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                            <div class="stars__items stars__items_active" style="width:40%">
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="characteristic-header-2" data-toggle="collapse" data-target="#characteristic-body-2" aria-expanded="true" aria-controls="characteristic-body-2">
                                                        Общие характеристики
                                                    </div>
                                                    <div class="collapse show" id="characteristic-body-2" aria-labelledby="characteristic-header-2">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">Android 7.0</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">iOS 12</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Версия ОС</div>
                                                                <div class="characteristic-value">Android 7.0</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">классический</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Тип корпуса</div>
                                                                <div class="characteristic-value">раскладной</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">постоянный</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header collapsed" id="characteristic-header-3" data-toggle="collapse" data-target="#characteristic-body-3" aria-expanded="true" aria-controls="characteristic-body-3">
                                                        Экран
                                                    </div>
                                                    <div class="collapse" id="characteristic-body-3" aria-labelledby="characteristic-header-3">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">попеременный</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Режим работы нескольких SIM-карт</div>
                                                                <div class="characteristic-value">постоянный</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header collapsed" id="characteristic-header-4" data-toggle="collapse" data-target="#characteristic-body-4" aria-expanded="true" aria-controls="characteristic-body-4">
                                                        Мультимедийные возможности
                                                    </div>
                                                    <div class="collapse" id="characteristic-body-4" aria-labelledby="characteristic-header-4">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="characteristic-header-5" data-toggle="collapse" data-target="#characteristic-body-5" aria-expanded="true" aria-controls="characteristic-body-5">
                                                        Питание
                                                    </div>
                                                    <div class="collapse show" id="characteristic-body-5" aria-labelledby="characteristic-header-5">
                                                        <div class="characteristic__row characteristic__row--diff">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">2</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Количество SIM-карт</div>
                                                                <div class="characteristic-value">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="characteristic__row">
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">есть</div>
                                                            </div>
                                                            <div class="characteristic__row-item">
                                                                <div class="characteristic-title">Поддержка MST</div>
                                                                <div class="characteristic-value">—</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once $partialsPath . '_footer.php'; ?>
        <script src="<?= $jsPath ?>compare.js"></script>
    </div>
</body>
</html>