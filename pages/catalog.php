<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-sm-3 col-lg-4';
		$pageTitle = 'Смартфоны Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/catalog.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1><?php echo $pageTitle ?></h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="sort">
					<div class="row">
						<div class="col-9 col-lg-11">
							<div class="sort__item d-none d-lg-inline-block">
								<div class="form-checkbox">
									<input type="checkbox" name="" id="checkbox1" class="form-checkbox__input">
									<label for="checkbox1" class="form-checkbox__label">Показывать цену с учетом кэшбэка </label>
								</div>
							</div>
							<div class="sort__item">
								<label for="" class="sort__label d-none d-sm-inline-block">Сортировать:</label>
								<div class="d-inline-block" style="vertical-align: bottom">
									<select class="select" name="">
								      <option>по популярности</option>
								      <option>по цене</option>
								      <option selected="selected">еще по какой-нибудь фигне</option>
								    </select>
								</div>
							</div>
						</div>
						<div class="col-3 col-lg-1 text-right text-lg-left">
							<button class="d-inline-block d-lg-none btn btn-primary btn-primary_filter toggle-right-mobile">
								<span class="icon-filters"></span>
							</button>
							<div class="catalog-view d-none d-lg-flex">
								<button class="btn-icon catalog-view__item" data-catalog-type="block">
									<span class=" icon-category-view-block"></span>
								</button>
								<button class="btn-icon catalog-view__item active" data-catalog-type="list">
									<span class=" icon-category-view-list"></span>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-lg-9">
						<div class="row">
							<div class="col-12">
								<div class="tags tags_catalog">
									<div class="row">
										<div class="col-12 col-sm-8">
											<div class="tags__item tags__item_gray">
												<span class="tags__label">Aser</span>
												<button class="btn-icon tags__button"><span class="icon-x"></span></button></div>
											<div class="tags__item tags__item_red">
												<span class="tags__label">Aser</span>
												<button class="btn-icon tags__button"><span class="icon-x"></span></button></div>
											<div class="tags__item">
												<span class="tags__label">Aser</span>
												<button class="btn-icon tags__button"><span class="icon-x"></span></button></div>
											<div class="tags__item">
												<span class="tags__label">Alienware</span>
												<button class="btn-icon tags__button"><span class="icon-x"></span></button></div>
											<div class="tags__item">
												<span class="tags__label">Alienware</span>
												<button class="btn-icon tags__button"><span class="icon-x"></span></button></div>
											<a href="" class="tags__filter-clear d-inline-block d-sm-none">
												Очистить фильтры
												<button class="btn-icon tags__button"><span class="icon-x"></span></button>
											</a>
										</div>
										<div class="col-4 text-right d-none d-sm-block">
											<a href="" class="tags__filter-clear">
												Очистить фильтры
												<button class="btn-icon tags__button"><span class="icon-x"></span></button>
											</a>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="content-view m-fz-14">
							<div class="content-view__item " id="list-view">
								<?php include_once $partialsPath . '_products-list.php'; ?>
							</div>
							<div class="content-view__item collapse" id="block-view">
								<?php include $partialsPath . '_products.php'; ?>

								<?php include_once $partialsPath . 'banners/banner-friday.php'; ?>

								<?php include $partialsPath . '_products.php'; ?>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<a href="" class="btn-outline btn-block pagination-more">
									Показать еще 24 товарa
								</a>
							</div>
						</div>
						<div class="pagination">
							<ul class="pagination__list">
								<li class="pagination__item"><a href="" class="pagination__link">1</a></li>
								<li class="pagination__item pagination__item_around"><a href="" class="pagination__link pagination__item_arrow pagination__item_arrow-back"><span class="icon-arrow"></span></a></li>
								<li class="pagination__item"><a href="" class="pagination__link">4</a></li>
								<li class="pagination__item pagination__item_active"><span class="pagination__link">5</span></li>
								<li class="pagination__item"><a href="" class="pagination__link">6</a></li>
								<li class="pagination__item pagination__item_around pagination__item_arrow"><a href="" class="pagination__link"><span class="icon-arrow"></span></a></li>
								<li class="pagination__item"><a href="" class="pagination__link">99</a></li>
							</ul>
						</div>
					</div>
                    <?php include_once $partialsPath . '_filters.php'; ?>
				</div>
				<div class="row">
					<div class="col-12">
						<?php include_once $partialsPath . 'banners/banner-samsung.php'; ?>
					</div>
				</div>

				<?php include_once $partialsPath . '_collections-list.php'; ?>


			</div>

			<?php include_once $partialsPath . '_advantages.php'; ?>
			<div class="container">
				<?php include_once $partialsPath . '_about-block.php'; ?>
				<?php include_once $partialsPath . '_reg-form.php'; ?>
			</div>

		</div>



		<?php include_once $partialsPath . '_footer.php'; ?>

	</div>

<script id="block-skeleton" type="text/x-custom-template">
    <div class="<?php echo $productsGrid ?> px-2 px-sm-3 col-6">
		<div class="products__item skeleton">
			<div class="products__header">
				<div class="skeleton__header">

				</div>

			</div>
			<div class="products__body">
				<div class="skeleton__img"></div>
				<div class="skeleton__name"></div>
				<div class="skeleton__name w-75"></div>
			</div>
			<div class="products__footer">
				<div class="row">
					<div class="col-8">
						<div class="skeleton__price w-75"></div>
						<div class="skeleton__cashback">
							<span class="skeleton__cashback-icon"></span>
							<span class="skeleton__cashback-text"></span>
						</div>
					</div>
					<div class="col-4">
						<div class="skeleton__btn-buy"></div>
					</div>
				</div>

			</div>
		</div>
	</div>
</script>
<script id="list-skeleton" type="text/x-custom-template">
   <div class="col-12">
			<div class="products-list__item skeleton skeleton_list">
				<div class="products-list__left">
					<div class="skeleton__img"></div>
				</div>
				<div class="products-list__body">
					<div class="skeleton__name"></div>
					<div class="skeleton__name w-75"></div>
					<div class="skeleno__rating">
						<span class="skeleton__cashback-icon"></span>
						<span class="skeleton__cashback-icon"></span>
						<span class="skeleton__cashback-icon"></span>
						<span class="skeleton__cashback-icon"></span>
						<span class="skeleton__cashback-icon"></span>

	              	</div>
	              	<div class="products-list__description">
	              		<div class="skeleton__name w-25"></div>
	              		<div class="skeleton__name"></div>
						<div class="skeleton__name w-75"></div>
	              	</div>
				</div>
				<div class="products-list__right">
					<div class="">
						<div class="skeleton__price w-75"></div>
					</div>
					<div class="">
						<div class="skeleton__cashback">
							<span class="skeleton__cashback-icon"></span>
							<span class="skeleton__cashback-text"></span>
						</div>
						<div class="skeleton__btn">

						</div>
					</div>


				</div>
			</div>
		</div>
</script>
</body>
</html>
