<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
					</div>
					<div class="col-12 col-lg-9">
						<div class="box">
							<div class="box__body">
								<div class="row">
									<div class="col-12 col-sm-6 border-right paddin-border">
										<div class="row">
											<div class="col-5 col-sm-7">
												<div class="mb-1 m-fz-14">
													<span class="fw-600">На балансе</span> <span class="around-link-icon around-link-icon_mini" data-toggle="popover" data-content="какой то текст который никому не интересен" data-trigger="focus" tabindex="-1">?</span>
												</div>
												<div class="h1">
													6 990 ₽
												</div>
											</div>
											<div class="col-7 col-sm-5 text-right">
												<button class="btn btn-primary pr-4 pl-4 mb-3 mt-2 text-truncate">Вывести</button>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 paddin-border">
										<div class="pl-sm-1">
											<div class="mb-1 m-fz-14">
												<span class="fw-600">Ожидает начисления</span> <span class="around-link-icon around-link-icon_mini" data-toggle="popover" data-content="какой то текст который никому не интересен" data-trigger="focus" tabindex="-1">?</span>
											</div>
											<div class="h1">
												590 ₽
											</div>
										</div>
										
					
									</div>
								</div>
							</div>
						</div>
						<div class="box">
							<div class="box__body">
								<div class="row mb-4">
									<div class="col-12 col-sm-6 border-right paddin-border">
										<div class="row">
											<div class="col-12">
												<div class="mb-1">
													<span class="fw-600">Статус</span> <span class="around-link-icon around-link-icon_mini">?</span>
												</div>
												<div class="h1">
													Бывалый
												</div>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 paddin-border ">
										<div class="pl-sm-1">
											<div class="mb-1 d-flex justify-content-between">
												<span class="text-small color_gray_dark">До статуса Бывалый осталось</span>
												<span class="fw-600 m-fz-14">10 000.00 ₽</span>
											</div>
											<div class="mb-1 d-flex justify-content-between">
												<span class="text-small color_gray_dark">Накоплено за все время</span>
												<span class="fw-600 m-fz-14">8 590.37 ₽</span>
											</div>
										</div>
									</div>
								</div>
								<div class="progress-scale">
									<div class="progress-scale__item active">
										<div class="progress-scale__value progress-scale__value_start">
											0
										</div>
										<div class="progress-scale__value">
											+10%
										</div>
										<div class="progress-scale__bar">

										</div>
										<div class="progress-scale__desc progress-scale__desc_start">
											Новичок
										</div>
										
										<div class="progress-scale__desc">
											Бывалый<br>500 ₽
										</div>
									</div>
									<div class="progress-scale__item">
										<div class="progress-scale__value">
											+20%
										</div>
										<div class="progress-scale__bar">

										</div>
										<div class="progress-scale__desc">
											Батя<br>3000 ₽
										</div>
									</div>
									<div class="progress-scale__item">
										<div class="progress-scale__value">
											+30%
										</div>
										<div class="progress-scale__bar">
										</div>
										<div class="progress-scale__desc">
											Старичок <br>10 000 ₽
										</div>
									</div>
								</div>
							</div>
							<div class="box__footer">
								<div class="gradient-block color_white">
									<div class="row">
										<div class="col-12 col-sm-7 mb-3 mb-sm-0">
											<span class="h1 font-weight-normal align-middle">Получать +30% кэшбэка? </span> <span class="around-link-icon around-link-icon_mini">?</span>
										</div>
										<div class="col-12 col-sm-5">
											<div class="row">
												<div class="col-6 px-sm-0 pr-0">
													<span class="h1 font-weight-normal">150 ₽</span><span class="h3 font-weight-normal"> / мес.</span>
												</div>
												<div class="col-6 text-right text-sm-left">
													<button class="btn btn-outline btn-outline-white ">Подключить</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<h2 class="h1 mb-4 pb-2">Акции</h2>
							</div>
							<div class="col-12">
								<div class="shops">
									<div class="row">
										<div class="col-6 col-sm-4 px-2 px-sm-3">
											<a class="shops__item" href="">
												<div class=" shops__promo">
													Промокод при регистрации
												</div>
											</a>
										</div>
										<div class="col-6 col-sm-4 px-2 px-sm-3">
											<a class="shops__item" href="">
												<div class="shops__promo">
													Скидки 30% на все!
												</div>
											</a>
										</div>
										<div class="col-6 col-sm-4 px-2 px-sm-3">
											<a class="shops__item" href="">

												<div class="shops__promo">
													2500 ₽ купон на покупки
												</div>
											</a>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>