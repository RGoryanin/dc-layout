<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-sm-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/category.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>
			<div class="page__title">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1><?php echo $pageTitle ?></h1>
						</div>
					</div>
				</div>
				
			</div>

			
			<div class="container">	
				<div class="row">
					<div class="col-12">
						<div class="slider mb-5">
                            <?php include_once $partialsPath . 'banners/banner-friday.php'; ?>
							<div class="slider__controls">
								<button class="slider__arrow slider__arrow_prev">
									<span class="icon-arrow-right"></span>
								</button>
								<button class="slider__arrow slider__arrow_next">
									<span class="icon-arrow-right"></span>
								</button>
							</div>
						</div>
						

						
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-lg-3">
						<div class="multi-filter">
							<div class="accordion" aria-multiselectable="true" id="accordionMultifilter">
								<div class="card">
									<div class="card-header collapsed"  data-toggle="collapse" data-target="#collapseCategory2" aria-expanded="true" aria-controls="collapseCategory2">
										<div class="card-title">
											Игровые приставки
										</div>
									</div>

									<div class="card-container collapse" id="collapseCategory2" aria-labelledby="headingOne" >
										<div class="card-body">
											<ul class="list-category">
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>
													
												</li>
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
											</ul>
                                            <ul class="list-category show-more-block ">
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>

                                                </li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
                                            </ul>
											<a href="" class="link text-small show-more-btn">Показать все</a>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header collapsed"  data-toggle="collapse" data-target="#collapseCategory3" aria-expanded="true" aria-controls="collapseCategory3">
										<div class="card-title">
											Ноутбуки и аксессуары
										</div>
									</div>

									<div class="card-container collapse" id="collapseCategory3" aria-labelledby="headingOne" >
										<div class="card-body"  >
											<ul class="list-category">
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>
													
												</li>
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
											</ul>
                                            <ul class="list-category show-more-block">
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>

                                                </li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
                                            </ul>
											<a href="" class="link text-small show-more-btn">Показать все</a>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header collapsed"  data-toggle="collapse" data-target="#collapseCategory4" aria-expanded="true" aria-controls="collapseCategory4">
										<div class="card-title">
											Офисная техника
										</div>
									</div>

									<div class="card-container collapse" id="collapseCategory4" aria-labelledby="headingOne" >
										<div class="card-body"  >
											<ul class="list-category">
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>
													
												</li>
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
											</ul>
                                            <ul class="list-category show-more-block">
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>

                                                </li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
                                            </ul>
                                            <a href="" class="link text-small show-more-btn">Показать все</a>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header collapsed"  data-toggle="collapse" data-target="#collapseCategory5" aria-expanded="true" aria-controls="collapseCategory5">
										<div class="card-title">
											Телефоны
										</div>
									</div>

									<div class="card-container collapse show" id="collapseCategory5" aria-labelledby="headingOne" >
										<div class="card-body"  >
											<ul class="list-category">
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>
													
												</li>
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
											</ul>
                                            <ul class="list-category show-more-block">
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>

                                                </li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
                                            </ul>
                                            <a href="" class="link text-small show-more-btn">Показать все</a>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header collapsed"  data-toggle="collapse" data-target="#collapseCategory6" aria-expanded="true" aria-controls="collapseCategory6">
										<div class="card-title">
											Компьютеры и комплектующие
										</div>
									</div>

									<div class="card-container collapse" id="collapseCategory6" aria-labelledby="headingOne" >
										<div class="card-body"  >
											<ul class="list-category">
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>
													
												</li>
												<li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
												<li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
											</ul>
                                            <ul class="list-category show-more-block">
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a>

                                                </li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Мобильные телефоны</a></li>
                                                <li class="list-category__item"><a href="" class="list-category__link">Умные часы и браслеты</a></li>
                                            </ul>
                                            <a href="" class="link text-small show-more-btn">Показать все</a>
										</div>
									</div>
								</div>
								<div class="card">
									<a href="#" class="card-header no-items">
										<span class="card-title">
											Гаджеты
										</span>
									</a>
								</div>

							</div>
			
						</div>
					</div>
					<div class="col-12 col-lg-9">

						<div class="row">
							<div class="col-12 mb-4">
								<h3 class="h3">Популярные товары</h3>
								
							</div>
							<div class="col-12">
								
								<?php include_once $partialsPath . '_products.php'; ?>
							</div>
						</div>
						<div class="row">
							<div class="col-12 mb-4">
								<h3 class="h3">Популярные магазины</h3>
								
							</div>
							<div class="col-12">
								<div class="shops">
									<div class="row">
										<div class="col-6 col-sm-4 px-2 px-sm-3">
											<a class="shops__item" href="">
												<div class="shops__img-container">
													<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
												</div>
												<span  class="shops__name">М.Видео</span>
												<div class="shops__rating">
													<div class="stars stars_red stars_view d-inline-block">
														<form novalidate="novalidate">
															<div class="stars__items">
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
															</div>
															<div class="stars__items stars__items_active" style="width:40%">
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
															</div>
														</form>
														
									              	</div>
									              	<span class="link link_gray text-small" >
														72 отзывов
													</span>
												</div>
												<div class="shops__description text-small lh_1_25">
													Бесплатная доставка товаров почтой России
												</div>
											</a>
										</div>
										<div class="col-6 col-sm-4 px-2 px-sm-3">
											<a class="shops__item" href="">
												<div class="shops__img-container">
													<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
												</div>
												<span href="" class="shops__name">AliExpress</span>
												<div class="shops__rating">
													<div class="stars stars_red stars_view d-inline-block">
														<form novalidate="novalidate">
															<div class="stars__items">
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
															</div>
															<div class="stars__items stars__items_active" style="width:40%">
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
															</div>
														</form>
														
									              	</div>
									              	<span class="link link_gray text-small" href="">
														390 отзывов
													</span>
												</div>
												<div class="shops__description text-small lh_1_25">
													Возможна доставка товаров Есть самовывоз
												</div>
											</a>
										</div>
										<div class="col-6 col-sm-4 px-2 px-sm-3">
											<a class="shops__item" href="">
												<div class="shops__img-container">
													<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
												</div>
												<span class="shops__name">Эльдорадо</span>
												<div class="shops__rating">
													<div class="stars stars_red stars_view d-inline-block">
														<form novalidate="novalidate">
															<div class="stars__items">
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
															</div>
															<div class="stars__items stars__items_active" style="width:60%">
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
																<span class="stars__item"><span class="icon-star"></span></span>
															</div>
														</form>
														
									              	</div>
									              	<span class="link link_gray text-small" >
														72 отзывов
													</span>
												</div>
												<div class="shops__description text-small lh_1_25">
													Бесплатная доставка товаров почтой России
												</div>
											</a>
										</div>
					
									</div>
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="col-12 mb-4 mt-4">
                                <a class="h1 h-link" href="">Товары для лета <div class="around-link-icon"><span class="icon-arrow"></span></div></a>

                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="products">
                                    <div class="row">
                                        <div class="col-md-4 px-lg-3 col-6">
                                            <div class="products__item">
                                                <div class="products__header">
                                                    <div class="tags__item tags__item_red">
                                                        <span class="tags__label" data-toggle="popover" data-content="Какой то текст классный распрекрасный" data-trigger="click">Товар месяца</span>
                                                    </div>
                                                    <a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
                                                </div>
                                                <div class="products__body">
                                                    <a href="#" class="products__images">
                                                        <img class="products__img" src="<?= $imagePath ?>products/photocam.png">
                                                    </a>
                                                    <a href="#" class="products__name">Зеркальный фотоаппарат Canon Mark II</a>
                                                </div>
                                                <div class="products__footer">
                                                    <div class="row">
                                                        <div class="col-9 pr-0  align-self-end">
                                                            <div class="products__price">
                                                                от 19 990 ₽
                                                            </div>
                                                            <div class="products__max-price">
                                                                до 24 490 ₽
                                                            </div>
                                                            <div class="products__cashback text-truncate">
                                                                <div class="products__cashback-icon"><span class="icon-ic-plus"></span>
                                                                </div><span class="products__cashback-text">кешбек до 999 ₽</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-3 pl-0 align-self-end text-right">
                                                            <a href="#" class="btn-buy">
                                                                <span class="icon-cart"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 px-lg-3 col-6">
                                            <div class="products__item">
                                                <div class="products__header">
                                                    <div class="products__tags"></div>
                                                    <a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
                                                </div>
                                                <div class="products__body">
                                                    <a href="#" class="products__images">
                                                        <img class="products__img" src="<?= $imagePath ?>products/mobile.png">
                                                    </a>
                                                    <a href="#" class="products__name">Смартфон Apple iPhone X 256Gb (серый космос)</a>
                                                </div>
                                                <div class="products__footer">
                                                    <div class="row">
                                                        <div class="col-9 pr-0  align-self-end">
                                                            <div class="products__price products__price_once">
                                                                89 490 ₽
                                                            </div>
                                                            <div class="products__cashback text-truncate">
                                                                <div class="products__cashback-icon"><span class="icon-ic-plus"></span>
                                                                </div><span class="products__cashback-text">кешбек до 999 ₽</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-3 pl-0 align-self-end text-right">
                                                            <a href="#" class="btn-buy">
                                                                <span class="icon-cart"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 px-lg-3 col-6">
                                            <div class="products__item">
                                                <div class="products__header">
                                                    <div class="tags__item tags__item_red">
                                                        <span class="tags__label" data-toggle="popover" data-content="Какой то текст классный распрекрасный" data-trigger="click">Товар месяца</span>
                                                    </div>
                                                    <a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
                                                </div>
                                                <div class="products__body">
                                                    <a href="#" class="products__images">
                                                        <img class="products__img" src="<?= $imagePath ?>products/nest.png">
                                                    </a>
                                                    <a href="#" class="products__name">Зеркальный фотоаппарат Canon Mark II</a>
                                                </div>
                                                <div class="products__footer">
                                                    <div class="row">
                                                        <div class="col-9 pr-0  align-self-end">
                                                            <div class="products__old-price">
                                                                <span class="products__old-price-text">24 490 ₽</span>

                                                            </div>
                                                            <div class="products__price">
                                                                19 990 ₽
                                                            </div>
                                                            <div class="products__cashback text-truncate">
                                                                <div class="products__cashback-icon"><span class="icon-ic-plus"></span>
                                                                </div><span class="products__cashback-text">кешбек до 999 ₽</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-3 pl-0 align-self-end text-right">
                                                            <a href="#" class="btn-buy">
                                                                <span class="icon-cart"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-md-none px-lg-3 col-6">
                                            <div class="products__item">
                                                <div class="products__header">
                                                    <div class="tags__item tags__item_red">
                                                        <span class="tags__label" data-toggle="popover" data-content="Какой то текст классный распрекрасный" data-trigger="click">Товар месяца</span>
                                                    </div>
                                                    <a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
                                                </div>
                                                <div class="products__body">
                                                    <a href="#" class="products__images">
                                                        <img class="products__img" src="<?= $imagePath ?>products/blender.png">
                                                    </a>
                                                    <a href="#" class="products__name">Зеркальный фотоаппарат Canon Mark II</a>
                                                </div>
                                                <div class="products__footer">
                                                    <div class="row">
                                                        <div class="col-9 pr-0  align-self-end">
                                                            <div class="products__price">
                                                                от 19 990 ₽
                                                            </div>
                                                            <div class="products__max-price">
                                                                до 24 490 ₽
                                                            </div>
                                                            <div class="products__cashback text-truncate">
                                                                <div class="products__cashback-icon"><span class="icon-ic-plus"></span>
                                                                </div><span class="products__cashback-text">кешбек до 999 ₽</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-3 pl-0 align-self-end text-right">
                                                            <a href="#" class="btn-buy">
                                                                <span class="icon-cart"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="row">
							<div class="col-12 mb-4">
								<h3 class="h3">Популярные товары</h3>
								
							</div>
							<div class="col-12">
								<div class="brands">
									<div class="row">
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										<div class="col-4 col-sm-2 px-2 px-sm-3">
											<a href="" class="brands__item">
												<div class="brands__img-container">
													<img src="<?= $imagePath ?>brands/samsung.svg" alt="" class="brands__img">
												</div>
											</a>
										</div>
										
									

									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 mb-4">
								<h3 class="h3">Подборки</h3>
								
							</div>

							<div class="col-12 list-buttons">
								<button class="btn-outline mb-3 mr-3">Бюджетные смартфоны</button>
								<button class="btn-outline mb-3 mr-3">Игровые консоли</button>
								<button class="btn-outline mb-3 mr-3">Игры для PS4</button>
								<button class="btn-outline mb-3 mr-3">Бензопилы для слабовидящих</button>
								<button class="btn-outline mb-3 mr-3">Лампы дневного света</button>
								<button class="btn-outline mb-3 mr-3">Лампы дневного света</button>
								<button class="btn-outline mb-3 mr-3">Шлемы виртуальной реальности</button>
								<button class="btn-outline mb-3 mr-3">Несессеры</button>
								<button class="btn-outline mb-3 mr-3">Бюджетные смартфоны</button>
								<button class="btn-outline mb-3 mr-3">SMART LED телевизоры</button>
								<button class="btn-outline mb-3 mr-3">Бензопилы для слабовидящих</button>
								<button class="btn-outline mb-3 mr-3">LED телевизоры</button>
								<button class="btn-outline mb-3 mr-3">Детские надувные бассейны</button>
								<button class="btn-outline mb-3 mr-3">Игровые консоли</button>
								<button class="btn-outline mb-3 mr-3">Показать еще 144</button>

							</div>

						</div>
					</div>
				</div>
			</div>
			
			<?php include_once $partialsPath . '_advantages.php'; ?>
			<div class="container">
				<?php include_once $partialsPath . '_about-block.php'; ?>
				<?php include_once $partialsPath . '_reg-form.php'; ?>
			</div>
			
		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>