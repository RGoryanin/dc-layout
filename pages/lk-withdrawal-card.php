<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
					</div>
					<div class="col-12 col-lg-9">
						<div class="row mb-4">
							<div class="col-12 col-sm-9 pr-0">
								<a class="h1 h-link" href=""><div class="around-link-icon around-link-icon_invert"><span class="icon-arrow"></span></div> Вывод средств на банковские карты  </a>

							</div>
							<div class="col-12 col-sm-3 pl-0 d-none d-sm-block">
								<div class="d-flex justify-content-start justify-content-sm-end pt-2">
									<span class="ml-3"><img src="<?= $imagePath ?>svg/visa-inc-logo-color.svg" alt="" class=""></span>
									<span class="ml-3"><img src="<?= $imagePath ?>svg/mastercard-logo.svg" alt="" class=""></span>
									<span class="ml-3"><img src="<?= $imagePath ?>svg/mir-logo-color.svg" alt="" class=""></span>
								</div>
							</div>

						</div>
						<div class="box box_padding">
							<div class="box__body">
								<div class="row">
									<div class="col-7 d-block pl-0 d-sm-none mb-3">
										<div class="d-flex justify-content-start justify-content-sm-end pt-2">
											<span class="ml-3"><img src="<?= $imagePath ?>svg/visa-inc-logo-color.svg" alt="" class=""></span>
											<span class="ml-3"><img src="<?= $imagePath ?>svg/mastercard-logo.svg" alt="" class=""></span>
											<span class="ml-3"><img src="<?= $imagePath ?>svg/mir-logo-color.svg" alt="" class=""></span>
										</div>
									</div>
								</div>
								
								<p class="lh_1_5 m-fz-14">
									При выводе на банковскую карту используйте только российские карты Visa и MasterCard с валютой российский рубль, иначе платеж вернется обратно.
								</p>
								<form action="">
									<div class="form-group">
										<div class="row">
											<div class="col-12 col-sm-6">
											    <label class="fw-600 m-fz-14">Номер карты</label>
											    <input type="text" class="form-control error" placeholder="0000 0000 0000 0000">
											</div>
											<div class="col-12 col-sm-6 pt-sm-4 m-fz-14">
												<span class="text-small color_red mt-sm-3 pt-1 d-block">Неверный номер карты</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-12 col-sm-6">
											    <label class="fw-600 m-fz-14">Имя</label>
											    <input type="text" class="form-control error" placeholder="Введите имя">
											</div>
											<div class="col-12 col-sm-6 pt-sm-4">
												<span class="text-small color_red mt-sm-3 pt-1 d-block">Введите имя</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-12 col-sm-6">
											    <label class="fw-600 m-fz-14">Фамилия</label>
											    <input type="text" class="form-control error" placeholder="Введите фамилию">
											</div>
											<div class="col-12 col-sm-6 pt-sm-4 m-fz-14">
												<span class="text-small color_red mt-sm-3 pt-1 d-block">Введите фамилию</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-12 col-sm-6">
											    <label class="fw-600 m-fz-14">Сумма вывода (₽)</label>
											    <input type="text" class="form-control error" placeholder="Неверная сумма">

											</div>
											<div class="col-12 col-sm-6 pt-sm-4 m-fz-14">
												<span class="text-small color_red mt-sm-3 pt-1 d-block">Неверный номер карты</span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 border-left border-left-2 ml-3 mb-3 input-desc">
											<p class="text-small mb-0 lh_1_25 color_gray_dark">Минимальная сумма вывода составляет 500 ₽<br>
											Для вывода суммы более 15 000 ₽ <a href="" class="link">свяжитесь с нами</a></p>
										</div>
										
									</div>
									<div class="row">
										<div class="col-12 col-lg-3 mt-4 mt-lg-1">
                                            <p class="fw-600 m-fz-14">Подтверждение действия</p>
                                            <p class="m-fz-14">Для повышения защиты вашего аккаунта, привяжите свой номер телефона. <br>
                                                Привязка номера телефона сделает последующие выводы средств быстрее.</p>
										</div>
									</div>
                                    <div class="row">
                                        <div class="col-9 col-lg-3">
                                            <button class="btn btn-primary mr-3 px-4">Привязать номер</button>
                                            <button class="btn btn-outline btn-outline-primary">Вывести деньги без привязки номера</button>
                                        </div>
                                    </div>
								</form>
								
							</div>
						</div>

					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>