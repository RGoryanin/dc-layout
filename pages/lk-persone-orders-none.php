<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
						
					</div>
					<div class="col-12 col-lg-9">
						<div class="box box_p-m-0">
							<div class="box__body">
								<div class="row">
									<div class="col-12 col-sm-6 border-right paddin-border">
										<div class="row">
											<div class="col-12 col-sm-7">
												<div class="mb-1 m-fz-14">
													<span class="fw-600">На балансе</span> <span class="around-link-icon around-link-icon_mini">?</span>
												</div>
												<div class="h1">
													0 ₽
												</div>
											</div>

										</div>
									</div>
									<div class="col-12 col-sm-6 paddin-border">
										<div class="pl-0 pl-sm-3 m-fz-14">
											<div class="mb-1">
												<span class="fw-600">Ожидает начисления</span> <span class="around-link-icon around-link-icon_mini">?</span>
											</div>
											<div class="h1">
												0 ₽
											</div>
										</div>
										
					
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<h2 class="h1 mb-4 pb-2">Мои заказы</h2>
							</div>
							<div class="col-12">
								<div class="orders">

									<div class="orders__body">
										<div class="orders__no-items text-center">
											<div class="h3 mb-3 font-weight-normal ">У вас нет ни одного заказа 😲</div>
											<p class="mb-4 m-fz-14">В каталоге ДелиКэш более 100 000 товаров <br> с существенными скидками и кэшбэком до 50%!</p>
											<button class="btn btn-primary pr-4 pl-4 mb-3 mt-2 mr-3">Перейти в каталог</button>
										</div>
									</div>
								</div>
							</div>


							
						</div>
					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>