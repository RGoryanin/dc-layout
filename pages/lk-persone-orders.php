<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once '../variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-4';
		$pageTitle = 'Samsung';
	?>
	<link rel="stylesheet" href="<?= $cssPath ?>pages/lk-persone.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<?php include_once $partialsPath . '_breadcrumbs.php'; ?>

			
			<div class="container">	
				<div class="row">
					<div class="col-12 col-lg-3">
						<?php include_once $partialsPath . 'lk/_persone-right-block.php'; ?>
						
					</div>
					<div class="col-12 col-lg-9">
						<div class="box box_p-m-0">
							<div class="box__body">
								<div class="row">
									<div class="col-12 col-sm-6 border-right paddin-border">
										<div class="row">
											<div class="col-7">
												<div class="mb-1 m-fz-14">
													<span class="fw-600">На балансе</span> <span class="around-link-icon around-link-icon_mini">?</span>
												</div>
												<div class="h1">
													6 990 ₽
												</div>
											</div>
											<div class="col-5 text-right">
												<button class="btn btn-primary pr-4 pl-4 mb-3 mt-2 ">Вывести</button>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 paddin-border">
										<div class="pl-sm-0">
											<div class="mb-1 m-fz-14">
												<span class="fw-600">Ожидает начисления</span> <span class="around-link-icon around-link-icon_mini">?</span>
											</div>
											<div class="h1">
												590 ₽
											</div>
										</div>
										
					
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<h2 class="h1 mb-4 pb-2">Мои заказы</h2>
							</div>
							<div class="col-12">
								<div class="orders m-fz-14">
									<div class="orders__header">
										<div class="row">
											<div class="col-3 pr-0">
												<span class="sort sort_desc">Дата</span>
											</div>
											<div class="col-4 pr-0">
												<span class="">Заказ</span>
											</div>
											<div class="col-2 pr-0">
												<span class="">Кэшбэк</span>
											</div>
											<div class="col-3 pr-0">
												<span class="">Статус</span>
											</div>
										</div>
									</div>
									<div class="orders__body">
										<div class="orders__item">
											<div class="row">
												<div class="col-3 d-none d-sm-block  pr-sm-0">
													29.08.2018 <br>19:14
												</div>
												<div class="col-8 col-sm-4 pr-0">
													<span class="fw-600 color_black">Связной</span><br>
													№234995 на сумму 19 990 ₽ 
													<div class="d-block d-sm-none">
														29.08.2018 19:14
													</div>
												</div>
												<div class="col-4 col-sm-5 text-right text-sm-left">
													<div class="row">
														<div class="col-12 col-sm-5 pr-sm-0">
															<div class="orders__price">
																490 ₽
															</div>
														</div>
														<div class="col-12 col-sm-7 pt-2 pr-sm-0">

															<div class="tags__item tags__item_red tags__item_no-active mt-2">
																<span class="tags__label">Отменен</span>
															</div>
														</div>	
													</div>
													
												</div>
												
											</div>
										</div>
										<div class="orders__item">
											<div class="row">
												<div class="col-3 d-none d-sm-block  pr-sm-0">
													29.08.2018 <br>19:14
												</div>
												<div class="col-8 col-sm-4 pr-0">
													<span class="fw-600 color_black">MediaMarkt</span><br>
													№000140 на сумму 200 990 ₽  
													<div class="d-block d-sm-none">
														29.08.2018 19:14
													</div>
												</div>
												<div class="col-4 col-sm-5 text-right text-sm-left">
													<div class="row">
														<div class="col-12 col-sm-5 pr-sm-0">
															<div class="orders__price">
																3 490 ₽
															</div>
														</div>
														<div class="col-12 col-sm-7 pt-2 pr-sm-0">

															<div class="tags__item tags__item_no-active tags__item_gray mt-2">
														<span class="tags__label">Ожидает</span>
													</div>
														</div>	
													</div>
													
												</div>
											</div>
										</div>
										<div class="orders__item">
											<div class="row">
												<div class="col-3 d-none d-sm-block  pr-sm-0">
													29.08.2018 <br>19:14
												</div>
												<div class="col-8 col-sm-4 pr-0">
													<span class="fw-600 color_black">Связной</span><br>
													№234995 на сумму 19 990 ₽ 
													<div class="d-block d-sm-none">
														29.08.2018 19:14
													</div>
												</div>
												<div class="col-4 col-sm-5 text-right text-sm-left">
													<div class="row">
														<div class="col-12 col-sm-5 pr-sm-0">
															<div class="orders__price">
																490 ₽
															</div>
														</div>
														<div class="col-12 col-sm-7 pt-2 pr-sm-0">

															<div class="tags__item tags__item_red tags__item_no-active mt-2">
																<span class="tags__label">Отменен</span>
															</div>
														</div>	
													</div>
													
												</div>
												
											</div>
										</div>
										<div class="orders__item">
											<div class="row">
												<div class="col-3 d-none d-sm-block  pr-sm-0">
													29.08.2018 <br>19:14
												</div>
												<div class="col-8 col-sm-4 pr-0">
													<span class="fw-600 color_black">MediaMarkt</span><br>
													№000140 на сумму 200 990 ₽  
													<div class="d-block d-sm-none">
														29.08.2018 19:14
													</div>
												</div>
												<div class="col-4 col-sm-5 text-right text-sm-left">
													<div class="row">
														<div class="col-12 col-sm-5 pr-sm-0">
															<div class="orders__price">
																3 490 ₽
															</div>
														</div>
														<div class="col-12 col-sm-7 pt-2 pr-sm-0">

															<div class="tags__item tags__item_no-active tags__item_gray mt-2">
														<span class="tags__label">Ожидает</span>
													</div>
														</div>	
													</div>
													
												</div>
											</div>
										</div>
										<div class="orders__item">
											<div class="row">
												<div class="col-3 d-none d-sm-block  pr-sm-0">
													29.08.2018 <br>19:14
												</div>
												<div class="col-8 col-sm-4 pr-0">
													<span class="fw-600 color_black">Связной</span><br>
													№234995 на сумму 19 990 ₽ 
													<div class="d-block d-sm-none">
														29.08.2018 19:14
													</div>
												</div>
												<div class="col-4 col-sm-5 text-right text-sm-left">
													<div class="row">
														<div class="col-12 col-sm-5 pr-sm-0">
															<div class="orders__price">
																490 ₽
															</div>
														</div>
														<div class="col-12 col-sm-7 pt-2 pr-sm-0">

															<div class="tags__item tags__item_red tags__item_no-active mt-2">
																<span class="tags__label">Отменен</span>
															</div>
														</div>	
													</div>
													
												</div>
												
											</div>
										</div>
										<div class="orders__item">
											<div class="row">
												<div class="col-3 d-none d-sm-block  pr-sm-0">
													29.08.2018 <br>19:14
												</div>
												<div class="col-8 col-sm-4 pr-0">
													<span class="fw-600 color_black">MediaMarkt</span><br>
													№000140 на сумму 200 990 ₽  
													<div class="d-block d-sm-none">
														29.08.2018 19:14
													</div>
												</div>
												<div class="col-4 col-sm-5 text-right text-sm-left">
													<div class="row">
														<div class="col-12 col-sm-5 pr-sm-0">
															<div class="orders__price">
																3 490 ₽
															</div>
														</div>
														<div class="col-12 col-sm-7 pt-2 pr-sm-0">

															<div class="tags__item tags__item_no-active tags__item_gray mt-2">
														<span class="tags__label">Ожидает</span>
													</div>
														</div>	
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12">
								<button class="btn-outline btn-block">
									Показать еще 24 товарa
								</button>
							</div>
							<div class="col-12">
								<div class="pagination">
									<ul class="pagination__list">
										<li class="pagination__item"><a href="" class="pagination__link">1</a></li>
										<li class="pagination__item pagination__item_around"><a href="" class="pagination__link pagination__item_arrow pagination__item_arrow-back"><span class="icon-arrow"></span></a></li>
										<li class="pagination__item"><a href="" class="pagination__link">4</a></li>
										<li class="pagination__item pagination__item_active"><span class="pagination__link">5</span></li>
										<li class="pagination__item"><a href="" class="pagination__link">6</a></li>
										<li class="pagination__item pagination__item_around pagination__item_arrow"><a href="" class="pagination__link"><span class="icon-arrow"></span></a></li>
										<li class="pagination__item"><a href="" class="pagination__link">99</a></li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			

		</div>
		
			

		<?php include_once $partialsPath . '_footer.php'; ?>
		
	</div>


</body>
</html>