<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
		include_once 'variables.php';
		include_once $partialsPath . '_head.php';
		$productsGrid = 'col-sm-3';
	?>

	<link rel="stylesheet" href="<?= $cssPath ?>pages/main.css?v=<?=Date('t' )?>">

</head>
<body>
	<div class="page page_main">
		<?php include_once $partialsPath . '_header-lk.php'; ?>
		<div class="body">
			<div class="container">
				<div class="slider mb-5">
					<div class="slider__container owl-carousel">
						<div class="slider__item">
							<div class="slider__img-container">
								<a href="#"
                                   class="slider__img d-none d-md-block">
                                    <img src="<?= $imagePath ?>banners/desk_banner_main_1120.png" alt="" class="slider__img">
								</a>
								<a href="#"
                                   style="background-image:url(<?= $imagePath ?>banners/main-banner-374@2x.jpg);"
                                   class="slider__img slider__img--2x">
								</a>
								<a href="#"
                                   style="background-image:url(<?= $imagePath ?>banners/main-banner-374.jpg);"
                                   class="slider__img d-block d-md-none">
								</a>
							</div>
						</div>
						<div class="slider__item">
							<div class="slider__img-container">
                                <a href="#"
                                   class="slider__img d-none d-md-block">
                                    <img src="<?= $imagePath ?>banners/desk_banner_main_1120.png" alt="" class="slider__img">
                                </a>
								<a href="#"
                                   style="background-image:url(<?= $imagePath ?>banners/main-banner-374@2x.jpg);"
                                   class="slider__img slider__img--2x">
								</a>
                                <a href="#"
                                   style="background-image:url(<?= $imagePath ?>banners/main-banner-374.jpg);"
                                   class="slider__img d-block d-md-none">
                                </a>
							</div>
						</div>
					</div>
					<div class="slider__controls">
						<button class="slider__arrow slider__arrow_prev">
							<span class="icon-arrow-right"></span>
						</button>
						<button class="slider__arrow slider__arrow_next">
							<span class="icon-arrow-right"></span>
						</button>
					</div>
				</div>
				<?php include_once $partialsPath . '_how-works.php'; ?>


				<div class="row">
					<div class="col-12 mb-4">
						<a class="h1 h-link" href="">Категории товаров <div class="around-link-icon"><span class="icon-arrow"></span></div></a>

					</div>
				</div>
				<div class="category">
					<div class="row">
						<div class="col-12 col-sm-6 col-lg-4">
							<a href="" class="category__item" style="background-image:url(<?= $imagePath ?>category-mobile.png)">
								<div class="category__item-container">
									<div class="category__name">
										Электроника
									</div>

								</div>
							</a>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<a href="" class="category__item" style="background-image:url(<?= $imagePath ?>category-helth.png)">
								<div class="category__item-container">
									<div class="category__name">
										Красота и здоровье
									</div>

								</div>
							</a>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<a href="" class="category__item" style="background-image:url(<?= $imagePath ?>category-home.png)">
								<div class="category__item-container">
									<div class="category__name">
										Дом и отдых
									</div>

								</div>
							</a>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<a href="" class="category__item" style="background-image:url(<?= $imagePath ?>category-tech.png)">
								<div class="category__item-container">
									<div class="category__name">
										Бытовая техника
									</div>

								</div>
							</a>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<a href="" class="category__item" style="background-image:url(<?= $imagePath ?>category-kids.png)">
								<div class="category__item-container">
									<div class="category__name">
										Детские товары
									</div>

								</div>
							</a>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<a href="" class="category__item" style="background-image:url(<?= $imagePath ?>category-all.png)">
								<div class="category__item-container">
									<div class="category__name">
										Все категории
									</div>

								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4 mt-4">
						<a class="h1 h-link" href="">Магазины  <div class="around-link-icon"><span class="icon-arrow"></span></div></a>

					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<?php include_once $partialsPath . '_shops.php'; ?>

					</div>
				</div>
			</div>
			<div class="dark-block mt-4">
				<div class="container">
					<div class="switch mb-4 pb-2">
						<div class="switch__label switch__label_nocheck active mr-2" data-target="switch1" onclick="$(this).parents('.switch').find('input').prop('checked', false ); ToggleSwitch.switchLabel($(this).parents('.switch'))">
							Минимальная цена
						</div>
						<label class="toggle-switch mr-2">
						  <input type="checkbox">
						  <span class="toggle-switch__slider"></span>
						</label>
						<div class="switch__label switch__label_checked" data-target="switch2" onclick="$(this).parents('.switch').find('input').prop('checked', true ); ToggleSwitch.switchLabel($(this).parents('.switch'))">
							Максимальная выгода
						</div>
					</div>
					<div class="dark-block__body">
						<div class="row">
							<div class="col-12">
								<div class="switch-content">
									<div class="switch-content__item collapse" id="switch1">
										<?php include $partialsPath . '_products4.php'; ?>
									</div>
									<div class="switch-content__item collapse" id="switch2">
										<?php include $partialsPath . '_products4.php'; ?>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php include_once $partialsPath . 'banners/banner-samsung.php'; ?>
					</div>
				</div>
				<div class="row mt-4">

					<div class="col-12 mb-4">
						<a class="h1 h-link" href="">Бренды <div class="around-link-icon"><span class="icon-arrow"></span></div></a>
					</div>
					<div class="col-12 pt-2">
						<?php include_once $partialsPath . '_brands.php'; ?>
					</div>

				</div>
				<div class="row mt-4 border-bottom">
					<div class="col-12 col-lg-5 mb-3 mb-lg-0">
						<a class="h1 h-link" href="">Подборки <div class="around-link-icon"><span class="icon-arrow"></span></div></a>
					</div>
					<div class="col-12 col-lg-7 p-lg-0 d-lg-flex align-items-lg-end justify-content-lg-end">
                        <ul class="nav nav-tabs nav-tabs--righted" id="t-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="t-tab-1" data-toggle="tab" href="#t-content-1" role="tab" aria-controls="t-content-1" aria-selected="true">Детские товары</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="t-tab-2" data-toggle="tab" href="#t-content-2" role="tab" aria-controls="t-content-2" aria-selected="false">Красота и здоровье</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="t-tab-3" data-toggle="tab" href="#t-content-3" role="tab" aria-controls="t-content-3" aria-selected="false">Бытовая техника</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="t-tab-4" data-toggle="tab" href="#t-content-4" role="tab" aria-controls="t-content-4" aria-selected="false">Электроника</a>
                            </li>
                        </ul>
					</div>
				</div>
				<div class="row">
					<div class="col-12 list-buttons mt-4" >
                        <div class="tab-content" id="t-tab-content">
                            <div class="tab-pane fade show active" id="t-content-1" role="tabpanel" aria-labelledby="t-tab-1">
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бюджетные смартфоны</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игровые консоли</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игры для PS4</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бензопилы для слабовидящих</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Лампы дневного света</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Лампы дневного света</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Шлемы виртуальной реальности</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Несессеры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Бюджетные смартфоны</a>
                                <a href="#" class="btn-outline mb-3 mr-3">SMART LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Бензопилы для слабовидящих</a>
                                <a href="#" class="btn-outline mb-3 mr-3">LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Детские надувные бассейны</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Игровые консоли</a>
                                <button class="btn-outline mb-3 mr-3 show-more-collections">Показать еще 144</button>
                            </div>
                            <div class="tab-pane fade" id="t-content-2" role="tabpanel" aria-labelledby="t-tab-2">
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Несессеры</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бюджетные смартфоны</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">SMART LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бензопилы для слабовидящих</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Детские надувные бассейны</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игровые консоли</a>
                            </div>
                            <div class="tab-pane fade" id="t-content-3" role="tabpanel" aria-labelledby="t-tab-3">
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бюджетные смартфоны</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игровые консоли</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игры для PS4</a>
                                <a href="#" class="btn-outline mb-3 mr-3">SMART LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Бензопилы для слабовидящих</a>
                                <a href="#" class="btn-outline mb-3 mr-3">LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Детские надувные бассейны</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Игровые консоли</a>
                                <button class="btn-outline mb-3 mr-3 show-more-collections">Показать еще 144</button>
                            </div>
                            <div class="tab-pane fade" id="t-content-4" role="tabpanel" aria-labelledby="t-tab-4">
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бюджетные смартфоны</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игровые консоли</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Игры для PS4</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Бензопилы для слабовидящих</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Лампы дневного света</a>
                                <a href="#" class="btn-outline mb-3 mr-3 shown">Лампы дневного света</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Шлемы виртуальной реальности</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Несессеры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Бюджетные смартфоны</a>
                                <a href="#" class="btn-outline mb-3 mr-3">SMART LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Бензопилы для слабовидящих</a>
                                <a href="#" class="btn-outline mb-3 mr-3">LED телевизоры</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Детские надувные бассейны</a>
                                <a href="#" class="btn-outline mb-3 mr-3">Игровые консоли</a>
                                <button class="btn-outline mb-3 mr-3 show-more-collections">Показать еще 144</button>
                            </div>
                        </div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4 mt-4">
						<a class="h1 h-link" href="">Промокоды <div class="around-link-icon"><span class="icon-arrow"></span></div></a>

					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="shops">
							<div class="row">
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
										</div>
										<span  class="shops__name">М.Видео</span>

										<div class=" shops__promo">
											Промокод при регистрации
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
										</div>
										<span href="" class="shops__name">AliExpress</span>
										<div class="shops__promo">
											Скидки 30% на все!
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Эльдорадо</span>

										<div class="shops__promo">
											2500 ₽ купон на покупки
										</div>
									</a>
								</div>
								<div class="col-6 col-sm-3 px-2 px-sm-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>

										<div class="shops__promo">
											12% кешбек
										</div>
									</a>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4 mt-4">
						<a class="h1 h-link" href="">Товары для лета <div class="around-link-icon"><span class="icon-arrow"></span></div></a>

					</div>
				</div>
				<div class="row mb-4">
					<div class="col-12">
						<?php include $partialsPath . '_products4.php'; ?>
					</div>
				</div>

			</div>
			<?php include $partialsPath . '_advantages.php'; ?>


		</div>



		<?php include_once $partialsPath . '_footer.php'; ?>
		<div class="welcome-block">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-9">
						<div class="welcome-block__title">Вас приветствует первый социально-ответственный кэшбэк-сервис «ДелиКэш». </div>
						<div class="text-small">Мы возвращаем деньги за покупки в 50 000 интернет-магазинов и сервисов  и вместе с вами помогаем нуждающимся</div>
					</div>
					<div class="col-12 col-md-5 col-lg-3">
						<button class="btn btn-danger btn-block">Подробнее</button>
					</div>
				</div>

			</div>
			<button class="btn-icon welcome-block__close">
				<span class="icon-x"></span>
			</button>
		</div>
	</div>

</body>
</html>
