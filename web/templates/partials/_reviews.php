<div class="reviews">
    <div class="reviews__header">
        <div class="row">
            <div class="col-12 col-sm-8">
                <ul class="nav nav-tabs nav-tabs--lefted" id="r-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="r-tab-1" data-toggle="tab" href="#r-content-1" role="tab" aria-controls="r-content-1" aria-selected="true">Все отзывы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="r-tab-2" data-toggle="tab" href="#r-content-2" role="tab" aria-controls="r-content-2" aria-selected="false">Только проверенные</a>
                    </li>
                </ul>
            </div>
            <div class="col-4 text-right lh_1_5 d-none d-sm-block">
                <a href="#addItemReview" class="link link_gray text-small" data-toggle="modal">Хочу написать отзыв <span class="around-link-icon around-link-icon_small">?</span></a>
            </div>
        </div>

    </div>
    <div class="reviews__body">
        <div class="tab-content" id="r-tab-content">
            <div class="tab-pane fade show active review" id="r-content-1" role="tabpanel" aria-labelledby="r-tab-1">
                <div class="review__item">
                    <div class="row">
                        <div class="col-12 col-sm-3">
                            <div class="review__autor">
                                <div class="review__autor-name mb-2">
                                    Константин Добронравов
                                </div>
                                <div class="review__rating mb-3">
                                    <div class="stars stars_view">
                                        <form novalidate="novalidate">
                                            <div class="stars__items">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                            <div class="stars__items stars__items_active" style="width:80%">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                <div class="review__autor-description text-small color_gray_dark lh_1_25">
                                    <span class="mr-3">Санкт-Петербург</span><br class="d-none d-sm-block">
                                    <span>24 июля 2018</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-9">
                            <div class="review__container">
                                <div class="review__text">
                                    <h4>Достоинства</h4>
                                    <p>Быстрый. Отличный дизайн. Высокое качество дисплея. Высокое качество фотокамеры. Скоростная видеосъемка (почему-то называется «медленная съемка»). Хорошо работает сканер отпечатков пальцев.</p>
                                    <h4>Недостатки</h4>
                                    <p>В основном это встроенный софт. Установка обновлений БЕЗ СОГЛАСИЯ владельца телефона! Стр. 207 Инструкции S8: "При выходе срочных обновлений ... они будут устанавливаться на устройстве без согласия пользователя" А с какого перепугу!?... Согласно ст.35 Конституции РФ, Право частной собственности охраняется законом! Телефон это моя ЧАСТНАЯ СОБСТВЕННОСТЬ, Интернет-канал на время действия договора с провайдером также принадлежит мне. На основании чего фирма Samsung без моего разрешения пользуется и тем и другим?!... Некоторые обновления в телефоне имеют в настройках только два варианта: "WiFi или мобильные сети" и "Только через WiFi". Например "Политики безопасности", или "Темы". Совсем отключить НЕЛЬЗЯ! Если WiFi НЕ безлимитный, например, WiFi-роутер с USB-модемом и отдельной SIM-картой, например, где-нибудь на даче, значит при любом включении WiFi телефон выкачает за мой счет столько, сколько посчитает нужным!</p>
                                    <h4>Комментарии</h4>
                                    <p>На Galaxy S4 при приходе SMS оно всплывало на экране блокировки и если на него нажвть, то телефон предлагал разблокировать экран, после чего сразу можно было прочитать сообщение. На S8 всплывает узкая полоска на несколько секунд, но на нее нажимай, не нажимай... Сначала нужно вывести телефон из спячки, набрать пин-код или прислонить палец к сканеру. Сканер отпечатков пальцев рядом с фотокамерой. Но про это уже весь Интернет исписан. Интерфейс ввода значения времени в приложениях, расположенными в двух кругах. <br>
                                        Лично мне всегда было удобно время вводить с ОБЫЧНОЙ ЦИФРОВОЙ клавиатуры! Видимо, постарался тот же "дезигнер", который изваял часы на экране блокировки. Кнопка Bixby. Лично я привык разговаривать с живыми людьми, а не с механизмами и мне эта кнопка - как собаке пятая нога, или рыбе зонтик. Говорят, что без танцев с бубном ее даже не переназначишь на другую более нужную функцию. Я бы фонарик на нее повесил, или фотокамеру, по-моему толку было бы больше.</p>
                                </div>
                                <div class="review__likes text-right">
                                    <span class="text-small color_gray_dark">Отзыв был полезен?</span>
                                    <div class="likes d-inline-block">
                                        <div class="likes__item">
                                            <input type="radio" name="likes[]" id="likes1" class="likes__input">
                                            <label for="likes1" class="likes__button"></label>
                                            <div class="likes__count">
                                                19
                                            </div>
                                        </div>
                                        <div class="likes__item ">
                                            <input type="radio" name="likes[]" id="likes2" class="likes__input">
                                            <label for="likes2" class="likes__button likes__button_dislike"></label>
                                            <div class="likes__count">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="review__item">
                    <div class="row">
                        <div class="col-12 col-sm-3">
                            <div class="review__autor">
                                <div class="review__autor-name mb-2">
                                    Криштиану Роналду
                                </div>
                                <div class="review__rating mb-3">
                                    <div class="stars stars_view">
                                        <form novalidate="novalidate">
                                            <div class="stars__items">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                            <div class="stars__items stars__items_active" style="width:80%">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                <div class="review__autor-description text-small color_gray_dark lh_1_25">
                                    <span class="mr-3">Санкт-Петербург</span><br class="d-none d-sm-block">
                                    <span>24 июля 2018</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-9">
                            <div class="review__container">
                                <div class="review__text">
                                    <h4>Комментарии</h4>
                                    <p>Вообще отстой, берите айфон!</p>

                                </div>
                                <div class="review__likes text-right">
                                    <span class="text-small color_gray_dark">Отзыв был полезен?</span>
                                    <div class="likes d-inline-block">
                                        <div class="likes__item">
                                            <input type="radio" disabled name="likes2[]" id="likes3" class="likes__input">
                                            <label for="likes3" class="likes__button"></label>
                                            <div class="likes__count">
                                                20
                                            </div>
                                        </div>
                                        <div class="likes__item ">
                                            <input type="radio" disabled checked name="likes2[]" id="likes4" class="likes__input">
                                            <label for="likes4" class="likes__button likes__button_dislike"></label>
                                            <div class="likes__count">
                                                137
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="review__item review__item_answer">
                                <div class="row">
                                    <div class="col-12 col-sm-3">
                                        <div class="review__autor">
                                            <div class="review__autor-img-container ">
                                                <img src="<?= $imagePath ?>ava.png" alt="" class="review__autor-img">
                                            </div>
                                            <div class="review__autor-name mb-2">
                                                ДелиКэш
                                            </div>
                                            <div class="review__autor-description text-small color_gray_dark lh_1_25">
                                                <span>Сегодня 18:00</span><br>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-9">
                                        <div class="review__container">
                                            <div class="review__text">

                                                <p>Добрый день, Александр!</p>

                                                <p>Спасибо за комментарий.</p>

                                                <p>Понимаем ваше огорчение. К сожалению, на данный момент, действительно, кэшбэк за покупки в кредит в магазине М.видео не начисляется. В случае изменения каких-либо условий со стороны партнеров, мы обязательно опубликуем информацию на странице магазина.</p>

                                                <p>Возникнут вопросы - пишите. Спасибо, что пользуетесь нашим сервисом.</p>

                                                <p>С уважением,<br>
                                                    команда ДелиКэш</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="review__item">
                    <div class="row">
                        <div class="col-12 col-sm-3">
                            <div class="review__autor">
                                <div class="review__autor-name mb-2">
                                    Всеволод Константинопольский
                                </div>
                                <div class="review__rating mb-3">
                                    <div class="stars stars_view">
                                        <form novalidate="novalidate">
                                            <div class="stars__items">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                            <div class="stars__items stars__items_active" style="width:80%">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                <div class="review__autor-description text-small color_gray_dark lh_1_25">
                                    <span class="mr-3">Санкт-Петербург</span><br class="d-none d-sm-block">
                                    <span>24 июля 2018</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-9">
                            <div class="review__container">
                                <div class="review__text">
                                    <h4>Достоинства</h4>
                                    <p>Внешний вид, экран, быстродействие. Телефон вообще выбивается из текущей действительности, как когда-то iphone 2g! Кстати, сравнение с iphone7 (к покупке рассматривал оба варианта) окончательно расстроило меня как фаната apple в прошлом. Увы, эпоха apple на этом рынке прошла. <br>
                                        NFC с android pay работает молниеносно. <br>
                                        Отпечаток пальца распознается быстро и безошибочно в 95 процентах случаев.</p>
                                    <h4>Недостатки</h4>
                                    <p>1. Телефон выскальзывает из карманов любых более-менее свободных брюк в сидячем положении (особенно в машине). <br>
                                        2. Хваленые наушники AKG совершенно расстроили. Наушники от iphone 5s играют на порядок качественнее. Впрочем, мне они вообще ни к чему. <br>
                                        3. Задняя панель ляпается и царапается больше чем дисплей. То есть, для лицевой и тыльной стороны явно применены разные материалы стекла и олеофобного покрытия. Тем, кто как я привык носить телефон без чехла, придется быть аккуратнее. Стекло есть стекло. <br>
                                        4. Иногда в сумерках не сразу можно сообразить где дисплей, а где тыльная сторона - последствия дизайна юнибоди и симметрично расположенных кнопок блокировки и ассистента. Телефон с обеих строн, практически, одинаков на ощупь за исключением рельефа камеры и датчика отпечатков).</p>
                                    <h4>Комментарии</h4>
                                    <p>РЕЗЮМЕ - телефон супер!!! Жаба, которая душила перед покупкой, моментально успокоилась в первые же часы использования.</p>
                                </div>
                                <div class="review__likes text-right">
                                    <span class="text-small color_gray_dark">Отзыв был полезен?</span>
                                    <div class="likes d-inline-block">
                                        <div class="likes__item">
                                            <input type="radio" checked disabled name="likes3[]" id="likes5" class="likes__input">
                                            <label for="likes5" class="likes__button"></label>
                                            <div class="likes__count">
                                                300
                                            </div>
                                        </div>
                                        <div class="likes__item ">
                                            <input type="radio" disabled name="likes3[]" id="likes6" class="likes__input">
                                            <label for="likes6" class="likes__button likes__button_dislike"></label>
                                            <div class="likes__count">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade review" id="r-content-2" role="tabpanel" aria-labelledby="r-tab-2">
                <div class="review__item">
                    <div class="row">
                        <div class="col-12 col-sm-3">
                            <div class="review__autor">
                                <div class="review__autor-name mb-2">
                                    Константин Добронравов
                                </div>
                                <div class="review__rating mb-3">
                                    <div class="stars stars_view">
                                        <form novalidate="novalidate">
                                            <div class="stars__items">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                            <div class="stars__items stars__items_active" style="width:80%">
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                <span class="stars__item"><span class="icon-star"></span></span>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                <div class="review__autor-description text-small color_gray_dark lh_1_25">
                                    <span class="mr-3">Санкт-Петербург</span><br class="d-none d-sm-block">
                                    <span>24 июля 2018</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-9">
                            <div class="review__container">
                                <div class="review__text">
                                    <h4>Достоинства</h4>
                                    <p>Быстрый. Отличный дизайн. Высокое качество дисплея. Высокое качество фотокамеры. Скоростная видеосъемка (почему-то называется «медленная съемка»). Хорошо работает сканер отпечатков пальцев.</p>
                                    <h4>Недостатки</h4>
                                    <p>В основном это встроенный софт. Установка обновлений БЕЗ СОГЛАСИЯ владельца телефона! Стр. 207 Инструкции S8: "При выходе срочных обновлений ... они будут устанавливаться на устройстве без согласия пользователя" А с какого перепугу!?... Согласно ст.35 Конституции РФ, Право частной собственности охраняется законом! Телефон это моя ЧАСТНАЯ СОБСТВЕННОСТЬ, Интернет-канал на время действия договора с провайдером также принадлежит мне. На основании чего фирма Samsung без моего разрешения пользуется и тем и другим?!... Некоторые обновления в телефоне имеют в настройках только два варианта: "WiFi или мобильные сети" и "Только через WiFi". Например "Политики безопасности", или "Темы". Совсем отключить НЕЛЬЗЯ! Если WiFi НЕ безлимитный, например, WiFi-роутер с USB-модемом и отдельной SIM-картой, например, где-нибудь на даче, значит при любом включении WiFi телефон выкачает за мой счет столько, сколько посчитает нужным!</p>
                                    <h4>Комментарии</h4>
                                    <p>На Galaxy S4 при приходе SMS оно всплывало на экране блокировки и если на него нажвть, то телефон предлагал разблокировать экран, после чего сразу можно было прочитать сообщение. На S8 всплывает узкая полоска на несколько секунд, но на нее нажимай, не нажимай... Сначала нужно вывести телефон из спячки, набрать пин-код или прислонить палец к сканеру. Сканер отпечатков пальцев рядом с фотокамерой. Но про это уже весь Интернет исписан. Интерфейс ввода значения времени в приложениях, расположенными в двух кругах. <br>
                                        Лично мне всегда было удобно время вводить с ОБЫЧНОЙ ЦИФРОВОЙ клавиатуры! Видимо, постарался тот же "дезигнер", который изваял часы на экране блокировки. Кнопка Bixby. Лично я привык разговаривать с живыми людьми, а не с механизмами и мне эта кнопка - как собаке пятая нога, или рыбе зонтик. Говорят, что без танцев с бубном ее даже не переназначишь на другую более нужную функцию. Я бы фонарик на нее повесил, или фотокамеру, по-моему толку было бы больше.</p>
                                </div>
                                <div class="review__likes text-right">
                                    <span class="text-small color_gray_dark">Отзыв был полезен?</span>
                                    <div class="likes d-inline-block">
                                        <div class="likes__item">
                                            <input type="radio" name="likes[]" id="likes1" class="likes__input">
                                            <label for="likes1" class="likes__button"></label>
                                            <div class="likes__count">
                                                19
                                            </div>
                                        </div>
                                        <div class="likes__item ">
                                            <input type="radio" name="likes[]" id="likes2" class="likes__input">
                                            <label for="likes2" class="likes__button likes__button_dislike"></label>
                                            <div class="likes__count">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="reviews__footer text-small text-center">
        <a href="" id="showAllReviews" class="link link_dark">Показать все отзывы</a>
    </div>
</div>