<div class="row how-works">
	<div class="col-12 mb-1 mb-sm-3">
		<a class="h1 h-link" href="">Как работает сервис? <div class="around-link-icon"><span class="icon-arrow"></span></div></a>
	</div>
	<div class="col-12 col-sm-6 col-lg-3 mb-sm-4 mb-lg-0">
		<a href="" class="how-works__link">
			<div class="row justify-content-start">
				<div class="col col-sm-3 how-works__icon how-works__icon_notebook pr-0 d-flex align-items-center"><span class="icon-notebook"></span></div>
				<div class="col col-sm-9 pr-0 d-flex align-items-center">Регистрируетесь на сайте</div>
			</div>
		</a>
		
	</div>
	<div class="col-12 col-sm-6 col-lg-3 mb-sm-4 mb-lg-0">
		<a href="" class="how-works__link">
			<div class="row justify-content-start">
				<div class="col col-sm-3 how-works__icon how-works__icon_mobile pr-0 d-flex align-items-center"><span class="icon-mobile"></span></div>
				<div class="col col-sm-9 pl-0 pr-0 d-flex align-items-center">Выбираете товар или магазин</div>
			</div>
		</a>
	</div>
    <div class="col-12 col-sm-6 col-lg-3">
        <a href="" class="how-works__link">
            <div class="row justify-content-start">
                <div class="col col-sm-3 how-works__icon pl-0 pr-0 d-flex align-items-center"><span class="icon-hands"></span></div>
                <div class="col col-sm-9 pr-0 d-flex align-items-center">Сравнивайте цены с учетом кешбэка</div>
            </div>
        </a>
    </div>
	<div class="col-12 col-sm-6 col-lg-3">
		<a href="" class="how-works__link">
		<div class="row justify-content-start">
			<div class="col col-sm-3 how-works__icon pr-0 pr-0 d-flex align-items-center"><span class="icon-rub"></span></div>
			<div class="col col-sm-9 pr-0 d-flex align-items-center">Возвращайте до 30% потраченых денег</div>
		</div>
		</a>
	</div>
</div>