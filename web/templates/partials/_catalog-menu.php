<nav id="menu-mobile" class="menu-mobile">
    <ul>
<!--        <li class="geolocation">-->
<!--            <button class="btn-icon btn-icon_geolocation" data-toggle="modal" data-target="#geoModal">-->
<!--                <span class="icon-city align-middle" style="font-size: 20px;"></span>-->
<!--                <span class="btn-icon__label align-middle">Санкт-Петербург</span>-->
<!--            </button>-->
<!--        </li>-->
        <li><a href="/">Электроника</a></li>
        <li><a href="/">Бытовая техника</a></li>
        <li>
            <a href="/">Детские товары</a>
            <ul>
                <li><a href="/pages/card.php">4567</a></li>
                <li><a href="/pages/card.php">4567</a></li>
                <li><a href="/pages/card.php">4567</a></li>
                <li>
                    <a href="/pages/card.php">4567</a>
                    <ul>
                        <li><a href="/pages/catalog.php">00000</a></li>
                        <li><a href="/pages/catalog.php">00000</a></li>
                        <li><a href="/pages/catalog.php">00000</a></li>
                        <li><a href="/pages/catalog.php">00000</a></li>
                        <li><a href="/pages/catalog.php">00000</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="/">Зоотовары</a></li>
        <li><a href="/">Красота и уход</a></li>
        <li><a href="/">Автотовары</a></li>
        <li><a href="/">Сад и дача</a></li>
        <li><a href="/">Строительство и ремонт</a></li>
        <li><a href="/">Спорт и активный отдых</a></li>
        <li><a href="/">Летний отдых</a></li>
        <li><a href="/">Другое</a></li>
    </ul>
</nav>
<nav class="mmenu">
    <div class="mmenu__header">
        <div class="mmenu-actions text-right">
            <div class="mmenu-close">
                <button class="btn-icon mmenu-closer">
                    <span class="icon-x"></span>
                </button>
            </div>
        </div>
    </div>
    <div class="mmenu__body">
        <div class="mmenu-geo">
            <button class="btn-icon btn-icon_geolocation" data-toggle="modal" data-target="#geoModal">
                <span class="icon-city align-middle" style="font-size: 20px;"></span>
                <span class="btn-icon__label align-middle">Санкт-Петербург</span>
            </button>
        </div>
        <div class="mmenu__body-wrapper">
            <ul class="mmenu-list mmenu-list--active">
                <li class="mmenu-list__item">
                    <a class="mmenu-list__link text-truncate" href="/pages/card.php">Электроника</a>
                </li>
                <li class="mmenu-list__item has-sub-menu">
                    <a class="mmenu-list__link text-truncate" href="#">Бытовая техника</a>
                    <div class="mmenu-list__card">
                        <div class="mmenu-list__card-header">
                            <a class="mmenu-list__card-title text-truncate" href="">Бытовая техника</a>
                        </div>
                        <div class="mmenu-list__card-body row">
                            <div class="col-6">
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые посудомоечные машины</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые холодильники</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые посудомоечные машины</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые холодильники</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые посудомоечные машины</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые холодильники</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="mmenu-list__item">
                    <a class="mmenu-list__link text-truncate" href="#">Детские товары</a>
                </li>
                <li class="mmenu-list__item">
                    <a class="mmenu-list__link text-truncate" href="#">Зоотовары</a>
                </li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Товары для дома</a></li>
                <li class="mmenu-list__item has-sub-menu">
                    <a class="mmenu-list__link text-truncate" href="#">Красота и уход</a>
                    <div class="mmenu-list__card">
                        <div class="mmenu-list__card-header">
                            <a class="mmenu-list__card-title text-truncate" href="">Бытовая техника</a>
                        </div>
                        <div class="mmenu-list__card-body row">
                            <div class="col-6">
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые посудомоечные машины</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые холодильники</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые посудомоечные машины</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые холодильники</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые посудомоечные машины</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые холодильники</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Вытяжки</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые духовые шкафы</a></div>
                                    <div class="mmenu-list__inner-item"><a href="#">Встраиваемые варочные панели</a></div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                </div>
                                <div class="mmenu-list__inner">
                                    <div class="mmenu-list__inner-header">
                                        <a href="#">Крупная бытовая техника</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Автотовары</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Сад и дача</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Строительство и ремонт</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Спорт и активный отдых</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Летний отдых</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Другое</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Автотовары</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Сад и дача</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Строительство и ремонт</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Спорт и активный отдых</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Летний отдых</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Другое</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Автотовары</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Сад и дача</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Строительство и ремонт</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Спорт и активный отдых</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Летний отдых</a></li>
                <li class="mmenu-list__item"><a class="mmenu-list__link text-truncate" href="#">Другое</a></li>
            </ul>
        </div>
    </div>
</nav>