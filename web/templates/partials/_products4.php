<div class="products">
	<div class="row">
		<div class="col-md-3 px-lg-3 col-6">
			<div class="products__item">
				<div class="products__header">
					<div class="tags__item tags__item_red">
						<span class="tags__label" data-toggle="popover" data-content="Какой то текст классный распрекрасный" data-trigger="click">Товар месяца</span>
					</div>
					<a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
				</div>
				<div class="products__body">
					<a href="#" class="products__images">
						<img class="products__img" src="<?= $imagePath ?>products/photocam.png">
					</a>
					<a href="#" class="products__name">Зеркальный фотоаппарат Canon Mark II</a>
				</div>
				<div class="products__footer">
					<div class="row">
						<div class="col-9 pr-0  align-self-end">
							<div class="products__price">
								от 19 990 ₽
							</div>
							<div class="products__max-price">
								до 24 490 ₽
							</div>
							<div class="products__cashback text-truncate">
								<div class="products__cashback-icon"><span class="icon-ic-plus"></span>
								</div><span class="products__cashback-text">кешбек до 999 ₽</span>
							</div>
						</div>
						<div class="col-3 pl-0 align-self-end text-right">
							<a href="#" class="btn-buy">
                                <span class="icon-cart"></span>
                            </a>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 px-lg-3 col-6">
			<div class="products__item">
				<div class="products__header">
					<div class="products__tags"></div>
					<a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
				</div>
				<div class="products__body">
					<a href="#" class="products__images">
						<img class="products__img" src="<?= $imagePath ?>products/mobile.png">
					</a>
					<a href="#" class="products__name">Смартфон Apple iPhone X 256Gb (серый космос)</a>
				</div>
				<div class="products__footer">
					<div class="row">
						<div class="col-9 pr-0  align-self-end">
							<div class="products__price products__price_once">
								89 490 ₽
							</div>
							<div class="products__cashback text-truncate">
								<div class="products__cashback-icon"><span class="icon-ic-plus"></span>
								</div><span class="products__cashback-text">кешбек до 999 ₽</span>
							</div>
						</div>
						<div class="col-3 pl-0 align-self-end text-right">
							<a href="#" class="btn-buy">
								<span class="icon-cart"></span>
							</a>
						</div>		
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 px-lg-3 col-6">
			<div class="products__item">
				<div class="products__header">
					<div class="tags__item tags__item_red">
						<span class="tags__label" data-toggle="popover" data-content="Какой то текст классный распрекрасный" data-trigger="click">Товар месяца</span>
					</div>
					<a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
				</div>
				<div class="products__body">
					<a href="#" class="products__images">
						<img class="products__img" src="<?= $imagePath ?>products/nest.png">
					</a>
					<a href="#" class="products__name">Зеркальный фотоаппарат Canon Mark II</a>
				</div>
				<div class="products__footer">
					<div class="row">
						<div class="col-9 pr-0  align-self-end">
							<div class="products__old-price">
								<span class="products__old-price-text">24 490 ₽</span>
								
							</div>
							<div class="products__price">
								19 990 ₽
							</div>
							<div class="products__cashback text-truncate">
								<div class="products__cashback-icon"><span class="icon-ic-plus"></span>
								</div><span class="products__cashback-text">кешбек до 999 ₽</span>
							</div>
						</div>
						<div class="col-3 pl-0 align-self-end text-right">
							<a href="#" class="btn-buy">
								<span class="icon-cart"></span>
							</a>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 px-lg-3 col-6">
			<div class="products__item">
				<div class="products__header">
					<div class="tags__item tags__item_red">
						<span class="tags__label" data-toggle="popover" data-content="Какой то текст классный распрекрасный" data-trigger="click">Товар месяца</span>
					</div>
					<a href="#" class="products__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
				</div>
				<div class="products__body">
					<a href="#" class="products__images">
						<img class="products__img" src="<?= $imagePath ?>products/blender.png">
					</a>
					<a href="#" class="products__name">Зеркальный фотоаппарат Canon Mark II</a>
				</div>
				<div class="products__footer">
					<div class="row">
						<div class="col-9 pr-0  align-self-end">
							<div class="products__price">
								от 19 990 ₽
							</div>
							<div class="products__max-price">
								до 24 490 ₽
							</div>
							<div class="products__cashback text-truncate">
								<div class="products__cashback-icon"><span class="icon-ic-plus"></span>
								</div><span class="products__cashback-text">кешбек до 999 ₽</span>
							</div>
						</div>
						<div class="col-3 pl-0 align-self-end text-right">
							<a href="#" class="btn-buy">
								<span class="icon-cart"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>