<div class="col-12 col-lg-3 right-mobile">
    <div class="right-mobile__header">
        Категории
        <button class="btn-icon btn-close toggle-right-mobile"><span class="icon-x"></span></button>
    </div>
    <form id="filters" class="right-mobile__body">
        <div class="multi-filter">
            <div class="accordion" aria-multiselectable="true" id="accordionMultifilter">

                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-8" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Интернет-магазины
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-8" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-1" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Путешествия
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-1" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-2" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Доставка
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-2" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-3" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Доставка
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-3" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-4" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Что то там еще
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-4" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-5" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Еще что то
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-5" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-6" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Почти последний пункт
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-6" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header collapsed" data-toggle="collapse" data-target="#group-7" aria-expanded="false" aria-controls="group-1s">
                        <div class="card-title">
                            Наконец-то последний пункт
                        </div>
                    </div>

                    <div class="card-container collapse" id="group-7" aria-labelledby="headingOne">
                        <div class="card-body">
                            <ul class="shop-filter-list">
                                <li><a href="#">Пункт 1</a></li>
                                <li><a href="#">Пункт 2</a></li>
                                <li><a href="#">Пункт 3</a></li>
                                <li><a href="#">Пункт 4</a></li>
                                <li><a href="#">Пункт 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </form>
</div>