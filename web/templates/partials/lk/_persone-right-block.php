<div class="persone">
	<div class="persone-info">
		<div class="persone-info__img-container">
			<div class="persone-info__img float-left float-lg-none">
				<img src="<?= $imagePath ?>ava.png" alt="">
			</div>
			<div class="persone-info__mb-info">
				<div class="persone-info__name">roman.pavlichuk</div>
				<div class="persone-info__id mb-1">ID: 69023045</div>
				<div class="persone-info__desc">С нами вы сэкономили <span class="d-inline d-lg-none">59 900 ₽</span> <span class="around-link-icon around-link-icon_mini" data-toggle="popover" data-content="какой то текст который никому не интересен" data-trigger="hover">?</span>
					<br>
					<span class="d-none d-lg-block">59 900 ₽</span>
				</div>
			</div>
			

		</div>
	</div>
	<ul class="persone-menu d-none d-lg-block">
		<li class="persone-menu__item active">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">Кабинет</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon" style="font-size: 20px;">
							<span class="icon-profile"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
		<li class="persone-menu__item ">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">Мои заказы</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon" style="font-size: 21px;">
							<span class="icon-stars2"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
		<li class="persone-menu__item ">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">Вывод средств</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon">
							<span class="icon-rub2" style="font-size: 23px"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
		<li class="persone-menu__item ">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">Настройки профиля</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon">
							<span class="icon-setting"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
		<li class="persone-menu__item ">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">FAQ</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon">
							<span class="icon-faq"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
		<li class="persone-menu__item ">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">Мои отзывы</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon">
							<span class="icon-review"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
		<li class="persone-menu__item ">
			<a href="" class="persone-menu__link">
				<div class="row">
					<div class="col-10">
						<div class="persone-menu__name">Мои реферралы</div>
					</div>
					<div class="col-2 pl-0 pr-0">
						<div class="persone-menu__icon" style="font-size: 18px;">
							<span class="icon-referals"></span>
						</div>
					</div>
				</div>
			</a>
		</li>
	</ul>
    <div class="persone-menu__select d-none d-sm-block d-lg-none">
        <select class="select" name="" id="">
            <option value="">Кабинет</option>
            <option value="">Мои заказы</option>
            <option value="">Вывод средств</option>
            <option value="">Настройки профиля</option>
            <option value="">FAQ</option>
            <option value="">Мои отзывы</option>
            <option value="">Мои рефералы</option>
        </select>
    </div>
</div>