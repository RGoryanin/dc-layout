<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-2">
				<div class="footer__colum-menu">
					<div class="footer__title">Каталог товаров</div>
					<ul class="menu menu_footer ">
						<li class="menu__item"><a href="" class="menu__link">Электроника</a></li>
						<li class="menu__item"><a href="" class="menu__link">Бытовая техника</a></li>
						<li class="menu__item"><a href="" class="menu__link">Детские товары</a></li>
						<li class="menu__item"><a href="" class="menu__link">Зоотовары</a></li>
						<li class="menu__item"><a href="" class="menu__link">Товары для дома</a></li>
						<li class="menu__item"><a href="" class="menu__link">Красота и уход</a></li>
						<li class="menu__item"><a href="" class="menu__link">Автотовары</a></li>
						<li class="menu__item"><a href="" class="menu__link">Сад и дача</a></li>	
					</ul>
				</div>
				
			</div>
			
			<div class="col-12 col-sm-2">
				<div class="footer__colum-menu">
					<div class="footer__title">Клиентам</div>
					<ul class="menu menu_footer">
						<li class="menu__item"><a href="" class="menu__link">Оплата и доставка</a></li>
						<li class="menu__item"><a href="" class="menu__link">Возврат</a></li>
						<li class="menu__item"><a href="" class="menu__link">Помощь</a></li>
						<li class="menu__item"><a href="" class="menu__link">Обратная связь</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-sm-2">
				<div class="footer__colum-menu">
					<div class="footer__title">О компании</div>
					<ul class="menu menu_footer">
						<li class="menu__item"><a href="" class="menu__link">Контакты</a></li>
						<li class="menu__item"><a href="" class="menu__link">Реквизиты</a></li>
						<li class="menu__item"><a href="" class="menu__link">Условия использования сайта</a></li>
						<li class="menu__item"><a href="" class="menu__link">Политика обработки персональных данных</a></li>
						<li class="menu__item"><a href="" class="menu__link">Условия заказа и доставки</a></li>
					</ul>
				</div>
			</div>
			<div class="d-none d-sm-block col-sm-2">
				<div class="footer__title"></div>
				<ul class="menu menu_footer">
					<li class="menu__item"><a href="" class="menu__link">Строительство и ремонт</a></li>
					<li class="menu__item"><a href="" class="menu__link">Спорт и активный отдых</a></li>
					<li class="menu__item"><a href="" class="menu__link">Книги</a></li>
					<li class="menu__item"><a href="" class="menu__link">Сумки и аксессуары</a></li>
					<li class="menu__item"><a href="" class="menu__link">Супермаркет</a></li>
					<li class="menu__item"><a href="" class="menu__link">Летний сезон</a></li>

				</ul>
			</div>
			<div class="col-12 col-sm-4 text-right py-3 py-sm-0">
				<div class="footer__logo-container">
					<div class="footer__logo float-left float-sm-none">
						<a href="">ДелиКэш</a>
					</div>
					<button class="btn btn-link btn-link_gray float-right float-sm-none">Связаться с нами</button>
				</div>
				
			</div>	
		</div>
		<div class="row">
			<div class="col-12 col-sm-8">
				<div class="footer__copy">
					© ООО «ИМЯ КОМПАНИИ» 2018. Все права защищены. <br>
					Указанная стоимость товаров и условия их приобретения действительны на текущую дату.
				</div>
			</div>
			<div class="col-12 col-sm-4 text-left text-sm-right">
				<div class="footer__pay-system">
					<div class="footer-pay-system">
						<span class="footer-pay-system__item"><img src="<?= $imagePath ?>svg/visa-inc-logo.svg" alt="" class="footer-pay-system__img"></span>
						<span class="footer-pay-system__item"><img src="<?= $imagePath ?>svg/mastercard-logo.svg" alt="" class="footer-pay-system__img"></span>
						<span class="footer-pay-system__item"><img src="<?= $imagePath ?>svg/mir-logo.svg" alt="" class="footer-pay-system__img"></span>

					</div>
					<p>Мы принимаем к оплате карты</p>
				</div>
			</div>
		</div>
	</div>
	
	 
</footer>


<div class="modal fade" id="geoModal" tabindex="-1" role="dialog" aria-labelledby="geoModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header d-block mb-4">
				<div class="modal-title">Выбор города</div>
				<button class="btn-icon close" data-dismiss="modal" aria-label="Close">
					<span class="icon-x"></span>
				</button>
				<p class="color_gray_dark m-0">Всего 512</p>
			</div>
			<div class="modal-body">
				<form class="form">
					<div class="form-group form-group_search">
						<input type="search" class="form-control form-control_search" id="geo-search" placeholder="Введите название города" autocomplete="off">
						<button type="submit" class="btn btn-icon btn-search"><span class="icon-search"></span></button>
					</div>
                    <div class="search-block__quick-search">
                        <ul class="quick-search-list"></ul>
                    </div>
				</form>
				<div class="name-list">
					<div class="row">
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark fw-600">Москва</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Екатеринбург</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Санкт-Петербург</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Ижевск</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Архангельск</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Иркутск</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Владивосток</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Казань</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Волгоград</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Кемерово</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Воронеж</a></div>
						<div class="col-12 col-sm-6 mb-2"><a href="" class="link link_dark">Краснодар</a></div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal reg-->
<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="regModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">Регистрация</div>
        <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
			<span class="icon-x"></span>
		</button>
      </div>
      <div class="modal-body">
        <form class="form">
		  <div class="form-group">
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ваш email">
		  </div>
		  <div class="form-group">
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Придумайте пароль">
		  </div>
		  <div class="form-group">
		  		<button class="btn btn-primary btn-block" type="submit">Зарегистрироваться</button>
		  </div>
		  <div class="form-group  lh_1_25">
		  	<span class="color_gray_dark text-small">Согласен с условиями <a href="" class="link">клиентского соглашения</a> 
	  		и <a href="" class="link">передачи персональных данных</a> </span>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
        <div class="row">
        	<div class="col-8">
        		<ul class="social-reg-list">
					<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-vk"></span></a></li>
					<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-facebook"></span></a></li>
					<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-ok"></span></a></li>
					<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-gplus"></span></a></li>
				</ul>
        	</div>
        	<div class="col-4 text-right text-small pt-4">
        		<span class="color_gray_dark">есть аккаунт?</span>
        		<br>
        		<a href="#loginModal" class="link to-another-modal" data-dismiss="modal">Войти</a>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal login-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">Войти</div>
        <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
			<span class="icon-x"></span>
		</button>
      </div>
      <div class="modal-body">
        <form class="form">
		  <div class="form-group">
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ваш email">
		  </div>
		  <div class="form-group">
		    <input type="password" class="form-control error" id="exampleInputPassword1" placeholder="Придумайте пароль">
		    <span class="text-small color_red mt-2 d-block">Неверная связка логина и пароля</span>
		  </div>
		  <div class="form-group">
		  		<button class="btn btn-primary btn-block" type="submit">Войти</button>
		  </div>
		</form>
		<div class="row mt-2 text-small">
			<div class="col-5">
				<a href="" class="link">Забыли пароль?</a>
			</div>
			<div class="col-7 text-right">
				Нет аккаунта? <a href="#regModal" class="link to-another-modal" data-dismiss="modal">Зарегистрируйтесь</a>
			</div>
		</div>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="addPhone" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Добавление телефона</div>
                <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-x"></span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-group">
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="addPhone" placeholder="+7 (999) 999-99-99">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Добавить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="addPass" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Смена пароля</div>
                <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-x"></span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-group">
                        <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="addPass" placeholder="Старый пароль">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="addPass" placeholder="Новый пароль">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Сменить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="addItemReview">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Написать отзыв</div>
                <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-x"></span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form form--review">
                    <div class="form-group d-flex align-items-baseline">
                        <label class="d-inline-block mr-3">Оценка</label>
                        <div class="stars stars--big">
                            <div class="stars__items">
                                <input class="stars__button" name="star" value="5" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" name="star" value="4" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" checked name="star" value="3" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" name="star" value="2" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" name="star" value="1" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Достоинства</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Что понравилось в товаре?">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Недостатки</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Что не понравилось?">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Комментарий</label>
                        <textarea class="form-control" name="" id="" cols="30" rows="5" placeholder="Чем руководствовались при выборе, общие ощущения от использования"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Написать</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addShopReview">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Написать отзыв</div>
                <button class="btn-icon close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-x"></span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form form--review">
                    <div class="form-group d-flex align-items-baseline">
                        <label class="d-inline-block mr-3">Оценка</label>
                        <div class="stars stars_red stars--big">
                            <div class="stars__items">
                                <input class="stars__button" name="star" value="5" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" name="star" value="4" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" checked name="star" value="3" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" name="star" value="2" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <input class="stars__button" name="star" value="1" type="radio">
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Комментарий</label>
                        <textarea class="form-control" name="" id="" cols="30" rows="5" placeholder="Опишите опыт от покупки в магазине, общие ощущения, что понравилось или нет"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Написать</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once $partialsPath . '_catalog-menu.php'; ?>

<div class="mobile-overlay"></div>
<div class="lk-overlay"></div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>-->

<script src="<?= $jsPath ?>vendors/owl/owl.carousel.min.js"></script>
<script src="<?= $jsPath ?>jquery.mousewheel.min.js"></script>
<script src="<?= $jsPath ?>jquery.events.swipe.js"></script>
<script src="<?= $jsPath ?>jquery.selectric.js"></script>
<script src="<?= $jsPath ?>vendors/mmenu/jquery.mmenu.all.js"></script>
<script src="<?= $jsPath ?>vendors/autocomplete/jquery.auto-complete.min.js"></script>
<script src="<?= $jsPath ?>main.js"></script>
<script>
	
</script> 


