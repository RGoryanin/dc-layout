<div class="slider__container owl-carousel">
    <div class="slider__item">
        <div class="slider__img-container">
            <a href="#"
               class="slider__img slider__img--small d-none d-md-block">
                <img src="<?= $imagePath ?>banners/banner-samsung.png" alt="" class="slider__img">
            </a>
            <a href="#"
               style="background-image:url(<?= $imagePath ?>banners/banner-samsung374.png);"
               class="slider__img d-block d-md-none">
            </a>
        </div>
    </div>
    <div class="slider__item">
        <div class="slider__img-container">
            <a href="#"
               class="slider__img slider__img--small d-none d-md-block">
                <img src="<?= $imagePath ?>banners/banner-samsung.png" alt="" class="slider__img">
            </a>
            <a href="#"
               style="background-image:url(<?= $imagePath ?>banners/banner-samsung374.png);"
               class="slider__img d-block d-md-none">
            </a>
        </div>
    </div>
</div>