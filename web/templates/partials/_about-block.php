<div class="row">
	<div class="col-12 mb-3 mb-sm-4 " >
		<h2 class="h1">
			О проекте
		</h2>
	</div>
</div>
<div class="row">
	<div class="col-12 col-sm-4 ">
		<p class="text-big">E-commerce is the activity of buying or selling of products on online services or over the internet. <br>
		Electronic commerce draws on technologies such as mobile commerce, electronic funds transfer, supply chain management, Internet marketing, and automated data collection systems.</p>
	</div>
	<div class="col-12 col-sm-8 text-small">
		<p>Modern electronic commerce typically uses the World Wide Web for at least one part of the transaction's life cycle although it may also use other technologies such as e-mail. Typical e-commerce transactions include the purchase of online books (such as Amazon) and music purchases (music download in the form of digital distribution such as iTunes Store), and to a less extent, customized/personalized online liquor store inventory services.</p>

		<p>There are three areas of e-commerce: online retailing, electric markets, and online auctions. E-commerce is supported by electronic business. E-commerce businesses may also employ some or all of the followings: Online shopping for retail sales direct to consumers via Web sites and mobile apps, and conversational commerce via live chat, chatbots, and voice assistants. Providing or participating in online marketplaces, which process third-party business-to-consumer or consumer-to-consumer sales <br>	
		Business-to-business buying and selling.</p>
	</div>
</div>