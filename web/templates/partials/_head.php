<meta charset="UTF-8">
<title>Document</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="<?= $cssPath ?>fonts.css">
<link rel="stylesheet" href="<?= $cssPath ?>library/owl.carousel.min.css">
<link rel="stylesheet" href="<?= $cssPath ?>library/selectric.css">
<link rel="stylesheet" href="<?= $cssPath ?>jquery.mmenu.all.css">
<link rel="stylesheet" href="<?= $cssPath ?>common.css?v=<?=Date('t')?>">
<link rel="stylesheet" href="<?= $cssPath ?>style.css">