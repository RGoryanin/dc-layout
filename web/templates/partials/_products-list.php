<div class="products-list">
	<div class="row">
		<div class="col-6 col-sm-12">
			<div class="products-list__item">
				<div class="products-list__left">
					<a href="#" class="products-list__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
					<a href="" class="products-list__images">
						<img class="products-list__img" src="<?= $imagePath ?>products/photocam.png">
					</a>
				</div>
				<div class="products-list__body">
					<a class="products-list__name" href="">Смартфон Apple iPhone X 256GB Space Gray (MQAF2RU/A)</a>
					<div class="stars d-none d-sm-inline-block">
                        <div class="stars stars_view d-inline-block align-middle">
                            <div class="stars__items">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <div class="stars__items stars__items_active" style="width:40%">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <a class="stars__count" href="">
                                16
                            </a>
                        </div>
	              	</div>
	              	<div class="products-list__description d-none d-sm-block">
	              		смартфон с Android 7.0
	              	</div>
				</div>
				<div class="products-list__right">
					<div class="products-list__prices">
						<div class="products-list__price">
							<span>от</span> 19 990 ₽
						</div>
						<div class="products-list__max-price">
							до 24 490 ₽
						</div>
					</div>
					<div class="">
						<div class="products-list__cashback text-truncate">
							<div class="products-list__cashback-icon">
                                <span class="icon-ic-plus"></span>
							</div>
                            <span class="products-list__cashback-text">кешбек <span>до</span> 999 ₽</span>
                            <a href="#" class="btn-buy d-inline-block d-sm-none">
                                <span class="icon-cart"></span>
                            </a>
						</div>
						<a href="#" class="btn btn-outline btn-outline-primary btn-block d-none d-sm-block">
							Купить
						</a>
					</div>

					
				</div>
			</div>
		</div>
		<div class="col-6 col-sm-12">
			<div class="products-list__item">
				<div class="products-list__left">
					<a href="#" class="products-list__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
					<a href="" class="products-list__images">
						<img class="products__img" src="<?= $imagePath ?>products/mobile.png">
					</a>
				</div>
				<div class="products-list__body">
					<div class="tags__item tags__item_red">
						<span class="tags__label">Товар месяца</span>
					</div>
					<a class="products-list__name" href="">Смартфон Apple iPhone X 256GB Space Gray (MQAF2RU/A)</a>
					<div class="stars d-none d-sm-inline-block">
                        <div class="stars stars_view d-inline-block align-middle">
                            <div class="stars__items">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <div class="stars__items stars__items_active" style="width:40%">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <a class="stars__count" href="">
                                16
                            </a>
                        </div>
	              	</div>
	              	<div class="products-list__description d-none d-sm-block">
	              		смартфон с Android 7.0 <br>
						3G, 4G LTE, LTE-A, Wi-Fi, Bluetooth, NFC, GPS, ГЛОНАСС <br>
						экран 5.8", разрешение 2960x1440
	              	</div>
				</div>
				<div class="products-list__right">
					<div class="products-list__prices">
						<div class="products-list__price">
							<span>от</span> 19 990 ₽
						</div>
					</div>
					<div class="">
						<div class="products-list__cashback text-truncate">
							<div class="products-list__cashback-icon">
                                <span class="icon-ic-plus"></span>
							</div>
                            <span class="products-list__cashback-text">кешбек <span>до</span> 999 ₽</span>
                            <a href="#" class="btn-buy d-inline-block d-sm-none">
                                <span class="icon-cart"></span>
                            </a>
						</div>
						<a href="#" class="btn btn-outline btn-outline-primary btn-block d-none d-sm-block">
							Купить
						</a>
					</div>

					
				</div>
			</div>
		</div>
		<div class="col-6 col-sm-12">
			<div class="products-list__item">
				<div class="products-list__left">
					<a href="#" class="products-list__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
					<a href="" class="products-list__images">
						<img class="products__img" src="<?= $imagePath ?>products/nest.png">
					</a>
				</div>
				<div class="products-list__body">
					<div class="tags__item tags__item_red">
						<span class="tags__label">Товар месяца</span>
					</div>
					<a class="products-list__name" href="">Смартфон Apple iPhone X 256GB Space Gray (MQAF2RU/A)</a>
					<div class="stars d-none d-sm-inline-block">
                        <div class="stars stars_view d-inline-block align-middle">
                            <div class="stars__items">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <div class="stars__items stars__items_active" style="width:40%">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <a class="stars__count" href="">
                                16
                            </a>
                        </div>
	              	</div>
	              	<div class="products-list__description d-none d-sm-block">
	              		смартфон с Android 7.0 <br>
						3G, 4G LTE, LTE-A, Wi-Fi, Bluetooth, NFC, GPS, ГЛОНАСС <br>
						экран 5.8", разрешение 2960x1440
	              	</div>
				</div>
				<div class="products-list__right">
					<div class="products-list__prices">
						<div class="products-list__price">
							<span>от</span> 19 990 ₽
						</div>
						<div class="products-list__max-price">
							до 24 490 ₽
						</div>
					</div>
					<div class="">
						<div class="products-list__cashback text-truncate">
							<div class="products-list__cashback-icon">
                                <span class="icon-ic-plus"></span>
							</div>
                            <span class="products-list__cashback-text">кешбек <span>до</span> 999 ₽</span>
                            <a href="#" class="btn-buy d-inline-block d-sm-none">
                                <span class="icon-cart"></span>
                            </a>
						</div>
						<a href="#" class="btn btn-outline btn-outline-primary btn-block d-none d-sm-block">
							Купить
						</a>
					</div>

					
				</div>
			</div>
		</div>
		<div class="col-6 col-sm-12">
			<div class="products-list__item">
				<div class="products-list__left">
					<a href="#" class="products-list__like btn-icon align-middle btn-icon_heart" data-product-id="12331"><span class="icon-stats-bars"></span></a>
					<a href="" class="products-list__images">
						<img class="products__img" src="<?= $imagePath ?>products/mixer.png">
					</a>
				</div>
				<div class="products-list__body">
					<a class="products-list__name" href="">Ноутбук Acer Predator Helios 300 PH315-51-7441 (NH.Q3FER.001)</a>
					<div class="stars d-none d-sm-inline-block">
                        <div class="stars stars_view d-inline-block align-middle">
                            <div class="stars__items">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <div class="stars__items stars__items_active" style="width:40%">
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                                <span class="stars__item"><span class="icon-star"></span></span>
                            </div>
                            <a class="stars__count" href="">
                                16
                            </a>
                        </div>
	              	</div>
	              	<div class="products-list__description d-none d-sm-block">
                        Acer; 15.6"; 1920x1080; Intel Core i7; 8750H; 2.2 ГГц; 16Gb; 128 Gb; 1 Tb; GeForce GTX 10ххM; nVidia GeForce GTX 1060; Windows 10 Home; черный
	              	</div>
				</div>
				<div class="products-list__right">
					<div class="products-list__prices">
						<div class="products-list__price">
							<span>от</span> 999 999 ₽
						</div>
					</div>
					<div class="">
						<div class="products-list__cashback text-truncate">
							<div class="products-list__cashback-icon">
                                <span class="icon-ic-plus"></span>
							</div>
                            <span class="products-list__cashback-text">кешбек <span>до</span> 999 ₽</span>
                            <a href="#" class="btn-buy d-inline-block d-sm-none">
                                <span class="icon-cart"></span>
                            </a>
						</div>
						<a href="#" class="btn btn-outline btn-outline-primary btn-block d-none d-sm-block">
							Купить
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
