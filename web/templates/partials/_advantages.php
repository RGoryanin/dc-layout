<div class="advantages">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="advantages__title">
					Наши преимущества
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="advantages__items owl-carousel">
					<div class="advantages__item">
						<div class="advantages__image"><span class="icon-proc"></span></div>
						<div class="advantages__name">Максимальный процент кешбека</div>
						<div class="advantages__text">По сравнению с другими сервисами</div>
					</div>
					<div class="advantages__item">
						<div class="advantages__image"><span class="icon-clock"></span></div>
						<div class="advantages__name">Быстрое начисление</div>
						<div class="advantages__text">По некоторым категориям товаров начисления приходят мгновенно</div>
					</div>
					<div class="advantages__item">
						<div class="advantages__image"><span class="icon-multi-select"></span></div>
						<div class="advantages__name">Огромный выбор</div>
						<div class="advantages__text">Представлено более 100 000 товаров разных категорий</div>
					</div>
					<div class="advantages__item">
						<div class="advantages__image"><span class="icon-interface"></span></div>
						<div class="advantages__name">Удобный интерфейс</div>
						<div class="advantages__text">Такое себе преимущество конечно, для массы написал</div>
					</div>
					<div class="advantages__item">
						<div class="advantages__image"><span class="icon-hands"></span></div>
						<div class="advantages__name">Помощь детям</div>
						<div class="advantages__text">Часть денежных средств мы переводим в благотворительные фонды</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
    <div class="advantages__controls">
        <button class="advantages__arrow advantages__arrow_prev">
            <span class="icon-arrow-right"></span>
        </button>
        <button class="advantages__arrow advantages__arrow_next">
            <span class="icon-arrow-right"></span>
        </button>
    </div>
</div>