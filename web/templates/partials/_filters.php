<div class="col-12 col-lg-3 right-mobile">
    <div class="right-mobile__header">
        Фильтры
        <button class="btn-icon btn-close toggle-right-mobile"><span class="icon-x"></span></button>
    </div>
    <form id="filters" class="right-mobile__body">
        <div class="multi-filter">
            <div class="accordion" aria-multiselectable="true" id="accordionMultifilter">

                <div class="card active">
                    <div class="card-header" data-toggle="collapse" data-target="#collapseSort" aria-expanded="true" aria-controls="collapseSorts">
                        <div class="card-title">
                            Сортировка товаров
                        </div>
                    </div>

                    <div class="card-container collapse show" id="collapseSort" aria-labelledby="headingOne">
                        <div class="card-body">
                            <select name="sort" id="" class="select">
                                <option value="popular">По популярности</option>
                                <option value="price">По цене</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card active">
                    <div class="card-header"  data-toggle="collapse" data-target="#collapsePrice" aria-expanded="true" aria-controls="collapseCategory">
                        <div class="card-title">
                            Цена, ₽
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapsePrice" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <div class="btn-group">
                                <label for=" btn-input1" class="btn btn-type-input">
                                    <span class="btn-type-input__label">от</span>
                                    <input type="text" class="btn-type-input__input js-check-min-max" name="price[min]" id="btn-input1" data-min="100" data-max="800" value="699"></span>

                                </label>
                                <label for="btn-input2" class="btn btn-type-input">
                                    <span class="btn-type-input__label">до</span>
                                    <input type="text" class="btn-type-input__input js-check-min-max" name="price[max]" id="btn-input2" data-min="8000" data-max="80000" value="999 999">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card active">
                    <div class="card-header"  data-toggle="collapse" data-target="#collapseCashBack" aria-expanded="true" aria-controls="collapseCategory">
                        <div class="card-title">
                            Кэшбэк, ₽
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapseCashBack" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <div class="btn-group">
                                <label for=" btn-input1" class="btn btn-type-input">
                                    <span class="btn-type-input__label">от</span>
                                    <input type="text" class="btn-type-input__input js-check-min-max" name="cashback[min]" id="btn-input1" data-min="19" data-max="50" value="19"></span>

                                </label>
                                <label for="btn-input2" class="btn btn-type-input">
                                    <span class="btn-type-input__label">до</span>
                                    <input type="text" class="btn-type-input__input js-check-min-max" name="cashback[max]" id="btn-input2" data-min="10000" data-max="99999" value="99 999">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card active">
                    <div class="card-header "  data-toggle="collapse" data-target="#collapseManufacturer" aria-expanded="true" aria-controls="collapseManufacturer">
                        <div class="card-title">
                            Способ доставки
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapseManufacturer" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <div class="checkbox-list">
                                <ul class="checkbox-list__list">
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-radio">
                                                <input type="radio" checked name="delivery" id="radio1" class="form-radio__input" value="any">
                                                <label for="radio1" class="form-radio__label">Любой </label>
                                            </div>
                                        </div>

                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-radio">
                                                <input type="radio" name="delivery" id="radio2" class="form-radio__input" value="tohome">
                                                <label for="radio2" class="form-radio__label">С доставкой </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-radio">
                                                <input type="radio" name="delivery" id="radio3" class="form-radio__input" value="byself">
                                                <label for="radio3" class="form-radio__label">Самовывоз	 </label>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card active">
                    <div class="card-header "  data-toggle="collapse" data-target="#collapseRating" aria-expanded="true" aria-controls="collapseManufacturer">
                        <div class="card-title">
                            Рейтинг магазина
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapseRating" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <div class="checkbox-list">
                                <ul class="checkbox-list__list">
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-radio">
                                                <input type="radio" checked name="rating" id="radio4" class="form-radio__input" value="off">
                                                <label for="radio4" class="form-radio__label">Любой </label>
                                            </div>
                                        </div>

                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-radio">

                                                <input type="radio" name="rating" id="radio5" class="form-radio__input" value="on">

                                                <label for="radio5" class="form-radio__label">
                                                    От
                                                    <div class="stars stars_red mt-0 align-middle">
                                                        <form novalidate="novalidate">
                                                            <div class="stars__items">
                                                                <input class="stars__button" checked="" name="star" value="5" type="radio">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <input class="stars__button" name="star" value="4" type="radio">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <input class="stars__button" name="star" value="3" type="radio">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <input class="stars__button" name="star" value="2" type="radio">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                                <input class="stars__button" name="star" value="1" type="radio">
                                                                <span class="stars__item"><span class="icon-star"></span></span>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card active">
                    <div class="card-header "  data-toggle="collapse" data-target="#collapseshops" aria-expanded="true" aria-controls="collapseManufacturer">
                        <div class="card-title">
                            Магазины
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapseshops" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <div class="checkbox-list">
                                <ul class="checkbox-list__list">
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="label" checked id="checkbox-list1" class="form-checkbox__input" value="acer">
                                                <label for="checkbox-list1" class="form-checkbox__label">
                                                    <a href="">Acer</a>
                                                    <span>2</span>
                                                </label>
                                            </div>
                                        </div>

                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="label" checked id="checkbox-list2" class="form-checkbox__input" value="alienware">
                                                <label for="checkbox-list2" class="form-checkbox__label">Alienware <span>5</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="label" checked disabled id="checkbox-list3" class="form-checkbox__input" value="apple">
                                                <label for="checkbox-list3" class="form-checkbox__label">Apple <span>16</span>  </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" disabled id="checkbox-list4" class="form-checkbox__input">
                                                <label for="checkbox-list4" class="form-checkbox__label">ASUS <span>120</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" id="checkbox-list5" class="form-checkbox__input">
                                                <label for="checkbox-list5" class="form-checkbox__label">Dell <span>3</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" id="checkbox-list6" class="form-checkbox__input">
                                                <label for="checkbox-list6" class="form-checkbox__label">HP <span>6</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" id="checkbox-list7" class="form-checkbox__input">
                                                <label for="checkbox-list7" class="form-checkbox__label">Lenovo <span>9</span></label>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="checkbox-list__list show-more-block">
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="label" checked id="checkbox-list1" class="form-checkbox__input" value="acer">
                                                <label for="checkbox-list1" class="form-checkbox__label">Acer <span>2</span> </label>
                                            </div>
                                        </div>

                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="label" checked id="checkbox-list2" class="form-checkbox__input" value="alienware">
                                                <label for="checkbox-list2" class="form-checkbox__label">Alienware <span>5</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="label" checked disabled id="checkbox-list3" class="form-checkbox__input" value="apple">
                                                <label for="checkbox-list3" class="form-checkbox__label">Apple <span>16</span>  </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" disabled id="checkbox-list4" class="form-checkbox__input">
                                                <label for="checkbox-list4" class="form-checkbox__label">ASUS <span>120</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" id="checkbox-list5" class="form-checkbox__input">
                                                <label for="checkbox-list5" class="form-checkbox__label">Dell <span>3</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" id="checkbox-list6" class="form-checkbox__input">
                                                <label for="checkbox-list6" class="form-checkbox__label">HP <span>6</span> </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="checkbox-list__item">
                                        <div class="form-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" name="" id="checkbox-list7" class="form-checkbox__input">
                                                <label for="checkbox-list7" class="form-checkbox__label">Lenovo <span>9</span></label>
                                            </div>
                                        </div>
                                    </li>
                                    </li>
                                </ul>
                                <button type="button" class="show-more-btn btn btn-link p-0">
                                    Показать все
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card active">
                    <div class="card-header"  data-toggle="collapse" data-target="#collapseType" aria-expanded="true" aria-controls="collapseCategory">
                        <div class="card-title">
                            Тип
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapseType" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <div class="btn-group">
                                <label for=" btn-input1" class="btn btn-type-input">
                                    <span class="btn-type-input__label">от</span>
                                    <input type="text" class="btn-type-input__input" name="" id="btn-input1" value="19"></span>

                                </label>
                                <label for="btn-input2" class="btn btn-type-input">
                                    <span class="btn-type-input__label">до</span>
                                    <input type="text" class="btn-type-input__input" name="" id="btn-input2" value="99 999">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header"  data-toggle="collapse" data-target="#collapseColor" aria-expanded="true" aria-controls="collapseCategory">
                        <div class="card-title">
                            Цвет
                        </div>
                    </div>

                    <div class="card-container collapse show"  id="collapseColor" aria-labelledby="headingOne" >
                        <div class="card-body ">
                            <ul class="list-color">
                                <li class="list-color__item" >
                                    <input type="checkbox" name="color" id="color1" class="list-color__checkbox" value="f1cca8">
                                    <label for="color1" class="list-color__label" style="background-color: #f1cca8"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="color" id="color2" class="list-color__checkbox" value="851a1e">
                                    <label for="color2" class="list-color__label" style="background-color: #851a1e"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color3" class="list-color__checkbox">
                                    <label for="color3" class="list-color__label" style="background-color: #b7b5ca51;"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color4" class="list-color__checkbox">
                                    <label for="color4" class="list-color__label" style="background-color: #111111"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color5" class="list-color__checkbox">
                                    <label for="color5" class="list-color__label" style="background-color: #808080;"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color6" class="list-color__checkbox">
                                    <label for="color6" class="list-color__label" style="background-color: #ff0202;"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color7" class="list-color__checkbox">
                                    <label for="color7" class="list-color__label" style=" background-color: #ffa503;"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color8" class="list-color__checkbox">
                                    <label for="color8" class="list-color__label" style="background-color: #ffd800;"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color9" class="list-color__checkbox">
                                    <label for="color9" class="list-color__label" style="background-color: #f6f5f5"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color10" class="list-color__checkbox">
                                    <label for="color10" class="list-color__label" style="background-color: #fb5550;"></label>
                                </li>
                                <li class="list-color__item" >
                                    <input type="checkbox" name="" id="color11" class="list-color__checkbox">
                                    <label for="color11" class="list-color__label" style="background-color: #a9a3e1;"></label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="multi-filter__footer">
                <button class="multi-filter__btn">
                    Очистить фильтры
                </button>
            </div>
        </div>
    </form>
    <div class="right-mobile__footer">
        <div class="row">
            <div class="col-6">
                <button class="btn btn-primary btn-block">Применить</button>
            </div>
            <div class="col-6 pt-2">
                <button class="multi-filter__btn">
                    Очистить фильтры
                </button>
            </div>
        </div>
    </div>
</div>