<div class="shops">
							<div class="row">
								<div class="col-6 px-2 px-sm-3 col-sm-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/mvideo-copy.svg" alt="" class="shops__img">
										</div>
										<span  class="shops__name">М.Видео</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 px-2 px-sm-3 col-sm-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/aliexpress-logo.svg" alt="" class="shops__img">
										</div>
										<span href="" class="shops__name">AliExpress</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:40%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												390 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Возможна доставка товаров Есть самовывоз
										</div>
									</a>
								</div>
								<div class="col-6 px-2 px-sm-3 col-sm-3">
									<a class="shops__item" href="">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/eldorado-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Эльдорадо</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:60%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" >
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>
								<div class="col-6 px-2 px-sm-3 col-sm-3">
									<a href="" class="shops__item">
										<div class="shops__img-container">
											<img src="<?= $imagePath ?>brands/asos-logo.svg" alt="" class="shops__img">
										</div>
										<span class="shops__name">Asos</span>
										<div class="shops__rating">
											<div class="stars stars_red stars_view d-inline-block">
												<form novalidate="novalidate">
													<div class="stars__items">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
													<div class="stars__items stars__items_active" style="width:10%">
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
														<span class="stars__item"><span class="icon-star"></span></span>
													</div>
												</form>
												
							              	</div>
							              	<span class="link link_gray text-small" href="">
												72 отзывов
											</span>
										</div>
										<div class="shops__description text-small lh_1_25">
											Бесплатная доставка товаров почтой России
										</div>
									</a>
								</div>

							</div>
						</div>