<div class="reg-form row">
	<div class="col-12 col-sm-6">

		<div class="reg-form__form-container">
			<div class="reg-form__top">
				<div class="row">
					<div class="col-7 pr-0">
						<div class="h1">
							Регистрация
						</div>
					</div>
					<div class="col-5 pl-0 text-right reg-form__top-login">	
						<a href="" class="link">Войдите</a><br>
						<span class="color_gray_dark">если есть аккаунт</span>
					</div>
				</div>
			</div>
			
			<form class="reg-form__form">
			  <div class="form-group">
			    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ваш email">
			  </div>
			  <div class="form-group">
			    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Придумайте пароль">
			  </div>
			  <div class="form-group">
			  		<button class="btn btn-primary btn-block" type="submit">Зарегистрироваться</button>
			  </div>
			  <div class="form-group  lh_1_25">
			  	<span class="color_gray_dark text-small">Согласен с условиями клиентского соглашения<br> 
		  		и передачи персональных данных </span>
			  </div>
			</form>
			<ul class="social-reg-list">
				<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-vk"></span></a></li>
				<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-facebook"></span></a></li>
				<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-ok"></span></a></li>
				<li class="social-reg-list__item"><a href="" class="social-reg-list__link"><span class="icon-social-gplus"></span></a></li>
			</ul>
		</div>
		
	</div>
	<div class="col-6 p-0 d-none d-sm-block">
		<div class="reg-form__image">
			
		</div>
	</div>
</div>