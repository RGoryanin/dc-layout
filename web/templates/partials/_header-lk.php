<header class="header">
    <div class="container">
        <div class="row header__top-wrapper">
            <div class="col-6 header__top">
                <div class="header__mobile d-block d-md-none">
                    <a id="to-menu-mobile" href="#menu-mobile" class="btn-icon mr-3">
                        <span class="icon-menu"></span>
                    </a>
                </div>
                <div class="header__big">
                    <div class="header__logo">
                        <a href="/">ДелиКэш</a>
                    </div>
                    <button class="btn-icon btn-icon_geolocation d-none d-md-block" data-toggle="modal" data-target="#geoModal">
                        <span class="icon-city"></span>
                        <span class="btn-icon__label">Санкт-Петербург</span>
                    </button>
                </div>
            </div>
            <div class="col-6 header__top header__top--right">
                <div class="header__mobile profile-group-icons">
                    <a href="#" class="btn-icon btn-icon_view">
                        <span class="badge badge-pill badge-danger">5</span>
                        <span class="icon-viewed"></span>
                    </a>
                    <a href="#" class="btn-icon btn-icon_stats">
                        <span class="badge badge-pill badge-danger">15</span>
                        <span class="icon-stats-bars"></span>
                    </a>
                    <a href="#" class="btn-icon btn-lk isLogged">
                        <span class="btn-lk-wrapper">
                            <span class="badge badge-pill badge-danger">5</span>
                            <span class="btn-lk-text">ДК</span>
                        </span>
                    </a>
                </div>
                <div class="header__big">
                    <ul class="header__menu menu text-right">
                        <li class="menu__item"><a href="" class="menu__link">Магазины</a></li>
                        <li class="menu__item"><a href="" class="menu__link">Вопрос-ответ</a></li>
                        <li class="menu__item"><a href="" class="menu__link">Стать партнером</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row header__bottom-wrapper">
            <div class="col-12">
                <div class="header__float">
                    <div class="container">
                        <div class="header__catalog-wrapper">
                            <a href="#" class="btn btn-catalog">
                                <span class="btn-catalog__label">Каталог товаров</span>
                                <span class="icon-menu"></span>
                            </a>
                        </div>
                        <div class="header__search-wrapper">
                            <div class="search-block">
                                <div class="search-block__form">
                                    <div class="form-group form-group_search">
                                        <input type="search" class="form-control form-control_search" id="search" placeholder="Поиск товаров" autocomplete="off">

                                        <button type="submit" class="btn btn-icon btn-search"><span class="icon-search"></span></button>
                                    </div>

                                </div>
                                <div class="search-block__quick-search">
                                    <ul class="quick-search-list">
                                        <li class="quick-search-list__item"><a href="" class="quick-search-list__link"><span class="quick-search-list__bolder">Знач</span>ение 1</a></li>
                                        <li class="quick-search-list__item"><a href="" class="quick-search-list__link"><span class="quick-search-list__bolder">Знач</span>ение 2</a></li>
                                        <li class="quick-search-list__item"><a href="" class="quick-search-list__link"><span class="quick-search-list__bolder">Знач</span>ение 3</a></li>
                                        <li class="quick-search-list__item"><a href="" class="quick-search-list__link"><span class="quick-search-list__bolder">Знач</span>ение 4</a></li>
                                        <li class="quick-search-list__item"><a href="" class="quick-search-list__link"><span class="quick-search-list__bolder">Значе</span>ние 5</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="header__actions-wrapper profile-group-icons">
                            <a href="#" class="btn-icon btn-icon_view">
                                <span class="badge badge-pill badge-danger">5</span>
                                <span class="icon-viewed"></span>
                            </a>
                            <a href="#" class="btn-icon btn-icon_stats">
                                <span class="badge badge-pill badge-danger">15</span>
                                <span class="icon-stats-bars"></span>
                            </a>
                            <a href="#" class="btn-icon btn-lk isLogged">
                                <span class="btn-lk-wrapper">
                                    <span class="badge badge-pill badge-danger">5</span>
                                    <span class="btn-lk-text">ДК</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lk-block collapse lk-block--authed">
            <div class="persone-info">
                <div class="persone-info__img-container">
                    <div class="persone-info__img"></div>
                    <div class="persone-info__mb-info">
                        <div class="persone-info__name">roman.pavlichuk</div>
                        <div class="persone-info__desc">59 900 ₽</div>
                    </div>
                </div>
            </div>
            <ul class="persone-menu">
                <li class="persone-menu__item active">
                    <a href="" class="persone-menu__link">
                        <div class="row">
                            <div class="col-10">
                                <div class="persone-menu__name">Кабинет</div>
                            </div>
                            <div class="col-2 pl-0 pr-0">
                                <div class="persone-menu__icon" style="font-size: 20px;">
                                    <span class="icon-profile"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="persone-menu__item ">
                    <a href="" class="persone-menu__link">
                        <div class="row">
                            <div class="col-10">
                                <div class="persone-menu__name">Мои заказы</div>
                            </div>
                            <div class="col-2 pl-0 pr-0">
                                <div class="persone-menu__icon" style="font-size: 21px;">
                                    <span class="icon-stars2"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="persone-menu__item ">
                    <a href="" class="persone-menu__link">
                        <div class="row">
                            <div class="col-10">
                                <div class="persone-menu__name">Вывод средств</div>
                            </div>
                            <div class="col-2 pl-0 pr-0">
                                <div class="persone-menu__icon">
                                    <span class="icon-rub2" style="font-size: 23px"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="persone-menu__item ">
                    <a href="" class="persone-menu__link">
                        <div class="row">
                            <div class="col-10">
                                <div class="persone-menu__name">Настройки профиля</div>
                            </div>
                            <div class="col-2 pl-0 pr-0">
                                <div class="persone-menu__icon">
                                    <span class="icon-setting"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="persone-menu__item ">
                    <a href="" class="persone-menu__link">
                        <div class="row">
                            <div class="col-10">
                                <div class="persone-menu__name">Выход</div>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>