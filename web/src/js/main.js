$(function () {
	$('#search').on('focus focusout', function () {
		$('.search-block__quick-search').slideToggle();
	});
	$('#geo-search').on('focusout', function () {
		let results = $(this).closest('form').find('.search-block__quick-search');
		if (!results.is(':hidden')) results.slideUp();
	});
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

  	$('.select').selectric({
		inheritOriginalWidth: true
	});

  	$('#menu-mobile').mmenu({
		extensions: [
			"theme-white",
			"pagedim-black",
			"fx-menu-slide"
		],
		navbar: {
			title: ' ',
			titleLink: 'anchor'
		},
		navbars: [
			{
				position: "top",
				content: [
					"prev",
					"title",
					"close"
				]
			}
		],
		onClick: {
			preventDefault: true
		},
		hooks: {
			"setSelected:after": function( $panel ) {
				let anchors = $panel.find('a');
				if(anchors.length === 2) {
					anchors[1].click();
				} else {
					location.href(anchors[0].attr('href'));
				}
			}
		}
	});
	let API = $('#menu-mobile').data('mmenu');
	$(window).resize(function () {
		API.close();
	});

  	$('#geo-search').autoComplete({
		minChars: 2,
		source: function (term) {
			$.get('http://dev.cashback.market/geo/suggest/', {term: term}, function (data) {
				drawSearchResults($('#geo-search').parent().next(), data);
			});
		}
	});

  	let drawSearchResults = (target, data) => {

  		let i = 0,
			list = $(target).find('.quick-search-list'),
			item;

  		list.empty();

  		if(data.length === 0) {
			item = $('<li/>', {
				"class": 'quick-search-list__item',
				text: 'Ничего не найдено...'
			});

			$(list).append(item);
		} else {
			while(i<data.length) {
				item = $('<li/>', {
					"class": 'quick-search-list__item',
					html: $('<a/>', {
						href: data[i].returnUrl,
						'class': 'quick-search-list__link',
						text: data[i].value
					})
				});

				$(list).append(item);
				i++;
			}
		}

		if($(target).is(':hidden')) $(target).slideToggle();
	};

  	var AdaptiveMobile = {
		initAction: function () {
			this.bindEvent();
			this.widthWindow();
			this.footerMenu();
			return this;
		},
		bindEvent: function () {
			self = AdaptiveMobile;
			$(window).resize(function () {
				self.widthWindow();

			});
		},
		widthWindow: function () {
			self = AdaptiveMobile;
			var widthDoc = $(document).width();
			if (widthDoc < 561) {
				$('.footer__colum-menu').addClass('footer__colum-menu_mobile');
			}else{
				$('.footer__colum-menu').removeClass('footer__colum-menu_mobile');
			}
		},
		footerMenu: function (argument) {
			$('.footer__title').on("click", function () {
				if ($(this).parent().hasClass('footer__colum-menu_mobile')) {
					$(this).parent().toggleClass('active');
					$(this).next().slideToggle();
				}

			});
		}

	};
	window.AdaptiveMobile = AdaptiveMobile.initAction();
	$(window).scroll(function () {

	    var iCurScrollPos = $(this).scrollTop();

	    if(!$('.header__bottom-wrapper').hasClass('no-float') && location.pathname !== '/pages/compare.php') {
			if (iCurScrollPos > 59) {

				$('.header').addClass('fixed');
				$('.header__float').addClass('active');

			} else {

				$('.header').removeClass('fixed');
				$('.header__float').removeClass('active');

			}
		}
	});
	var owl = $('.slider__container');
		owl.owlCarousel({
		    loop:true,
			autoplay: true,
			autoplayTimeout: 7000,
		    autoplaySpeed: 500,
			autoplayHoverPause: true,
		    smartSpeed: 1000,
		    margin:10,
		    nav:false,
		    items: 1,
		    autoHeight:true,
		    dots: false
		});
	  $(".slider__arrow_next").click(function(){
	    owl.trigger('next.owl.carousel');
	  })
	  $(".slider__arrow_prev").click(function(){
	    owl.trigger('prev.owl.carousel');
	  })

	var owlmobile = $('.advantages__items');
		owlmobile.owlCarousel({
		    loop:false,
		    autoplaySpeed: 1000,
		    smartSpeed: 1000,
		    margin:10,
		    nav:false,
		    items: 5,
	       	responsive:{
		        0:{
		            items:2,
		            nav:false
		        },
		        768:{
		            items:4,
		            nav:false
		        }
		    },
		    autoHeight:true,
		    dots: false
		});

	$(".advantages__arrow_next").click(function(){
		owlmobile.trigger('next.owl.carousel');
	});
	$(".advantages__arrow_prev").click(function(){
		owlmobile.trigger('prev.owl.carousel');
	});

	var MenuMobile = {
		initAction: function () {
			this.bindEvent();
			return this;
		},
		bindEvent: function () {
			let timer;
			$('.btn-catalog, .mmenu-closer, .mobile-overlay').on('touchend, click', (e) => {
				e.preventDefault();
				if($(e.currentTarget).hasClass('btn-catalog')) {
					let ww = $(window).width();
					let wh = $(window).height();

					if(ww < 992) {
						$('#to-menu-mobile').click();
						return false;
					}

					let activeHeight = wh - 65;
					$('.mmenu__body-wrapper').css('height', activeHeight);
					$('.mmenu-list__card').css('max-height', activeHeight);
				}

				$('body').toggleClass('mobile-open');
			});
			$('.mmenu-list__item.has-sub-menu').on('mouseenter', function () {
				clearTimeout(timer);
				$(this).siblings('.has-sub-menu').removeClass('hover');
				$(this).addClass('hover');
				$(this).children('.mmenu-list__card').css('top', 122);
			}).on('mouseleave', function () {
				let $self = $(this);
				timer = setTimeout(() => {
					$self.removeClass('hover');
				}, 300);
			});
			$('.mmenu a').on('click', function (e) {
				e.stopPropagation();
			});
			$('.mmenu__body-wrapper').on('click', function (e) {
				if (e.target === this) {
					$('body').toggleClass('mobile-open');
				} else {
					return;
				}
			});
			$('.mmenu').on('mouseenter', '.mmenu-list__card', function () {
				$('.mmenu__body-wrapper').css('overflow-y', 'hidden');
			}).on('mouseleave', '.mmenu-list__card', function () {
				$('.mmenu__body-wrapper').css('overflow-y', 'scroll');
			});
			$(window).resize(function () {
				if($('body').hasClass('mobile-open')) {
					$('body').removeClass('mobile-open');
				}
			});
		}
	};

	window.MenuMobile = MenuMobile.initAction();

	var RightMobile = {
		initAction: function () {
			this.bindEvent();
			return this;
		},
		bindEvent: function () {
			let self = this;
			$('.toggle-right-mobile').on('click', function () {
				self.toggle();
			});
			$('.right-mobile').on('swipeleft', function () {
				let ww = $(window).width();

				if(ww<992) self.toggle();
			});
			$(window).on('resize', function () {
				$('body').removeClass('open-right-mobile');
                $('html').removeClass('overflow');
				$('.right-mobile').removeAttr('style');
			});
		},
		toggle: function () {
			$('body').toggleClass('open-right-mobile');
			$('html').toggleClass('overflow');
			$('.right-mobile').slideToggle();
		}
	};

	window.RightMobile = RightMobile.initAction();

	let LkBlock = {
		initAction: function () {
			this.bindEvent();
			if(!this.checkClick()) $('.btn-lk').addClass('pulse');
			return this;
		},
		checkClick: function () {
			return localStorage.getItem('clickedLkButton');
		},
		isMobile: function() {
			return $(window).width() < 768;
		},
		checkBlockStyles: function() {
			let isMobile = this.isMobile();
			let $block = $('.lk-block');

			if (isMobile) {
				$block.css('top', -32);
			} else {
				$block.removeAttr('style');
			}
		},
		bindEvent: function () {
			let self = this;
			$('.btn-lk, .lk-overlay').on('click', function (e) {
				e.preventDefault();
				e.stopPropagation();

				let isLogged = $(this).hasClass('isLogged');
				let $lkBlock = $('.lk-block');

				if (!self.checkClick() && !isLogged) {
					localStorage.setItem('clickedLkButton', 'true');
					$('.btn-lk').removeClass('pulse');
				}

				if($lkBlock.is(':hidden')) self.checkBlockStyles();

				$('.lk-overlay').toggle();
				$lkBlock.fadeToggle(500);
			});

			// .on('blur', function (e) {
			// 	e.preventDefault();
			//
			// 	if(!$('.lk-block').is(':hover')) {
			// 		$(this).click();
			// 	} else {
			// 		$(this).focus();
			// 	}
			// });

			$(window).resize(function () {
				if($('.lk-block').is(':visible')) self.checkBlockStyles();
			});
		}
	};

	window.LkBlock = LkBlock.initAction();

	var ToggleSwitch = {
		initAction: function () {
			this.bindEvent();
			this.switchLabel($('.switch'));
			return this;

		},
		bindEvent: function () {

			$('.toggle-switch input').on('change', function () {
				var self = ToggleSwitch;
				self.switchLabel($(this).parents('.switch'));

			});

		},
		switchLabel: function ($t){
			var self = ToggleSwitch;
			$t.find('.switch__label').removeClass('active');
			if ($t.find('input').prop('checked')) {
				$('.switch__label_checked').addClass('active');
				self.switchContent($('.switch__label_checked').attr('data-target'));
			}else{
				$('.switch__label_nocheck').addClass('active');
				self.switchContent($('.switch__label_nocheck').attr('data-target'));
			}

		},
		switchContent: function (activeEl){
			$('#'+activeEl).siblings().removeClass('show');
			$('#'+activeEl).addClass('show');
		}

	};
	window.ToggleSwitch = ToggleSwitch.initAction();

	var PhotoGallery = {
		initAction: function () {
			this.bindEvent();
			return this;

		},
		bindEvent: function () {
			$('.c-carousel__preview-item').on('click', function () {
			   $(this).addClass('active').siblings().removeClass('active');
			   $('.c-carousel__img').attr("src", $(this).attr("data-img"));
			   return false;
			});
		}
	};

	window.PhotoGallery = PhotoGallery.initAction();

	VerticalSlider = {
		initAction: function () {
			this.bindEvent();
			this.cash();
			this.calcEl();
			return this;

		},
		bindEvent: function () {

			$('.c-carousel__nav_prev').on('click', function() {
				var self = VerticalSlider;
				self.prevEl();
			});
			$('.c-carousel__nav_next').on('click', function() {
				var self = VerticalSlider;
				self.nextEl();
			});
		},
		cash:  function () {
			this.maxHeightContainer = 0;
			this.heightElement = 0;
			this.allElsContainersHeight = 0;
			this.currentScroll = 0;
		},
		calcEl: function () {
			this.maxHeightContainer = $('.c-carousel__preview-container').height();
			this.heightElement = $('.c-carousel__preview-container').children().outerHeight(true);
			this.allElsContainersHeight = $('.c-carousel__preview-container').children().length * this.heightElement;
		},
		prevEl: function () {
			var self = VerticalSlider;
			self.currentScroll = $('.c-carousel__preview-container').scrollTop() - self.heightElement;

			$('.scroll-container').stop().animate({
		        scrollTop: self.currentScroll
		    }, 500)
		},
		nextEl: function () {
			var self = VerticalSlider;
			self.currentScroll = $('.c-carousel__preview-container').scrollTop() + self.heightElement;
			$('.scroll-container').stop().animate({
		        scrollTop: self.currentScroll
		    }, 500);
		}
	};

	window.VerticalSlider = VerticalSlider.initAction();

	let Catalog = {
		catalogWrapper: $('.content-view'),
		catalogType: 'list',

		init: function() {
			this.handlers();
			return this;
		},

		switchView: function() {
			let selector = $('#'+ this.catalogType +'-view');
			selector.toggleClass('collapse').siblings().toggleClass('collapse');
		},

		addSkeleton: function(count) {
			let wrapperClass = this.catalogType === 'block' ? '.products' : '.products-list',
				wrapperSelector = $('#'+ this.catalogType +'-view '+ wrapperClass +'>.row'),
				index = 0;

			wrapperSelector.empty();

			while(index < count) {
				wrapperSelector.append($('#'+ this.catalogType +'-skeleton').html());
				index++;
			}
		},

		getFilters: function() {
			return $('#filters').serializeArray();
		},

		getProducts: function(url, filters, showSkeleton, cb) {
			let self = this;
			let data = filters || null;

			if(showSkeleton) this.addSkeleton(6);

			$.ajax({
				type: "POST",
				url: url,
				async: true,
				data: data,
				success: function (html) {
					setTimeout(function () {
						self.addProducts(html);
						if(cb) cb();
					}, 5000);
				}
			})

		},

		addProducts: function(items) {
			let wrapperSelector = $('#'+ this.catalogType +'-view');

			wrapperSelector.empty().append(items);
		},

		handlers: function() {
			let self = this;

			$('.catalog-view__item').on('click', function () {
				if($(this).hasClass('active')) return false;

				$(this).toggleClass('active').siblings().toggleClass('active');

				self.catalogType = $(this).attr('data-catalog-type');
				let url = self.isBlock() ? '/web/templates/partials/_products.php' : '/web/templates/partials/_products-list.php';

				self.switchView();
				self.getProducts(url, self.getFilters(), true);

			});

			$('#filters').on('change', function (e) {
				e.preventDefault();
				let data = self.getFilters();
				let url = self.isBlock() ? '/web/templates/partials/_products.php' : '/web/templates/partials/_products-list.php';

				Loader.action(self.catalogWrapper, true);
				self.getProducts(url, data, false,() => Loader.action());
			});

			$('.js-check-min-max').on('change', function (e) {
                e.preventDefault();

                let value = $(this).val(),
                    minRule = parseFloat($(this).attr('data-min')),
                    maxRule = parseFloat($(this).attr('data-max'));

                if (value < minRule) $(this).val(minRule);
                if (value > maxRule) $(this).val(maxRule);
            });

			$(document).on('click', '.pagination__link, .pagination-more', function (e) {
				e.preventDefault();
				let url = $(this).attr('href');

				Loader.action(self.catalogWrapper, true);
				self.getProducts(url, self.getFilters(), false, () => Loader.action());
			}).on('click', '.products-list__like, .products__like, .cards__compare', function (e) {
                e.preventDefault();

                let $self = $(this);

                let id = $(this).attr('data-product-id');
                let badges = $('.header .btn-icon_stats');

                $.ajax({
                    method: 'post',
                    url: '',
                    data: id,
                    success: function () {
                        $self.toggleClass('added');

                        $.each(badges, function (i, block) {
							let badge = $(this).find('.badge');
							if(!badge) {
								$(block).prepend('<span class="badge badge-pill badge-danger">1</span>');
							} else {
								let number = parseFloat($(badge).text()) + 1; // number заменить числом из ответа бэка
								// if(!number) {
								// 	$(badge).remove();
								// }
								$(badge).text(number);
							}
						});
                    }
                })
            })

		},

		isBlock: function () {
			return this.catalogType === 'block';
		}
	};

	window.Catalog = Catalog.init();

	let smallCatalog = {
		catalogWrapper: $('.prices-list'),

		init: function() {
			this.handlers();
			return this;
		},

		getFilters: function() {
			return $('#filters').serializeArray();
		},

		getProducts: function(url, filters, cb) {
			let self = this;
			let data = filters || null;

			$.ajax({
				type: "POST",
				url: url,
				async: true,
				data: data,
				success: function (html) {
					setTimeout(function () {

						if(cb) cb();
					}, 5000);
				}
			})

		},

		handlers: function () {
			let self = this;

			$('#filters').on('change', function (e) {
				e.preventDefault();
				let data = self.getFilters();
				let url = '';

				Loader.action(self.catalogWrapper, true);
				self.getProducts(url, data, false,() => Loader.action());
			});

			$(document).on('click', '.prices-list__footer', function (e) {
				e.preventDefault();

				let data = self.getFilters();
				let url = '';

				Loader.action(self.catalogWrapper, true);
				self.getProducts(url, data, false,() => Loader.action());
			})
		}
	};

	window.smallCatalog = smallCatalog.init();

	Loader = {
		target: null,
		init: function () {
			return this;
		},
		action: function (target = null, show) {
			if(target) this.target = target;

			if(show) {
				this.target.addClass('preloader');
			} else {
				this.target.removeClass('preloader');
			}
		}
	};

	window.Loader = Loader.init();

	Reviews = {
		wrapper: $('.review'),
		init: function () {
			this.handlers();
			return this;
		},
		getReviews: function() {
			let self = this;

			// emulation
			let answer = this.wrapper.html();
			Loader.action(this.wrapper, true);

			setTimeout(function() {
				self.drawReviews(answer);
				Loader.action();
			}, 5000)
		},
		drawReviews: function (data) {
			this.wrapper.html(data);
		},
		handlers: function () {
			let self = this;

			$('#showAllReviews').on('click', function (e) {
				e.preventDefault();

				self.getReviews();
			})
		}
	};

	window.Review = Reviews.init();

	function getCookie(name) {
		let matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	}

	let showWelcomeBanner = getCookie('welcomeBannerShowed');
	if(!showWelcomeBanner) {
		$('.welcome-block').show();

		$('.welcome-block__close').on('click', function (e) {
			e.preventDefault();
			$('.welcome-block').hide();
			document.cookie = 'welcomeBannerShowed=true';
		});
	}

	$(document).on('click', '.show-more-btn', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).parent().addClass('hidden-block-opened');
	});

	(function () {
		let _overlay = document.querySelector('.header__float');
		let _clientY = null; // remember Y position on touch start

		_overlay.addEventListener('touchstart', function (event) {
			if (event.targetTouches.length === 1) {
				// detect single touch
				_clientY = event.targetTouches[0].clientY;
			}
		}, false);

		_overlay.addEventListener('touchmove', function (event) {
			if (event.targetTouches.length === 1) {
				// detect single touch
				disableRubberBand(event);
			}
		}, false);

		function disableRubberBand(event) {
			let clientY = event.targetTouches[0].clientY - _clientY;

			if (_overlay.scrollTop === 0 && clientY > 0) {
				// element is at the top of its scroll
				event.preventDefault();
			}

			if (isOverlayTotallyScrolled() && clientY < 0) {
				//element is at the top of its scroll
				event.preventDefault();
			}
		}

		function isOverlayTotallyScrolled() {
			return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
		}
	}());

    $('.to-another-modal').on('click', function (e) {
    	e.preventDefault();

        let modal = $(this).attr('href');

        setTimeout(function () {
			$(modal).modal('show')
		}, 350);
    });

	$('.cards__like').on('click', function (e) {
		e.preventDefault();

		let $self = $(this);
		let isLogged = false;

		if(!isLogged) {
			$('#loginModal').modal('show');
			return false;
		} else {
			let id = $(this).attr('data-product-id');
			let block = $('.persone-img-block');

			$.ajax({
				method: 'post',
				url: '',
				data: id,
				success: function () {
					$self.toggleClass('added');
					let badge = $(block).find('.badge');

					if(!badge) {
						$(block).prepend('<span class="badge badge-pill badge-danger">1</span>');
					} else {
						let number = parseFloat($(badge).text()) + 1; // number заменить числом из ответа бэка
						// if(!number) {
						// 	$(badge).remove();
						// }
						$(badge).text(number);
					}
				}
			})
		}
	});

	$('.show-more-collections').on('click', function (e) {
		e.preventDefault();

		$(this).addClass('hided');
		let hidedCollections = $(this).parent().find('a:not(.shown)');

		$.map(hidedCollections, function (collection) {
			$(collection).addClass('shown')
		});
	});

	$('.card__product-images-wrapper').owlCarousel({
        items: 1,
        loop: true
    });

	$('.c-carousel__body, .card__product-images').on('click', 'img', function(e) {
        $('body').addClass('modal-open');
        $('html').addClass('overflow');
	    $('.card__product-zoomer').addClass('active');
        $('.card__product-zoomer-wrapper').owlCarousel({
            items: 1,
            loop: true,
            dots: false,
            nav: true
        });
    });

	$('.card__product-zoomer-closer').on('click', function () {
        $('body').removeClass('modal-open');
        $('html').removeClass('overflow');
        $('.card__product-zoomer').removeClass('active');
        $('.card__product-zoomer-wrapper').owlCarousel('destroy');
    });

	$('body').on('keyup', function (e) {
        if($('body').hasClass('modal-open') && $('.card__product-zoomer').hasClass('active')) {
            if(e.keyCode === 27) {
                $('.card__product-zoomer-closer').click();
            } else if (e.keyCode === 37) {
                $('.card__product-zoomer-wrapper').owlCarousel('prev');
            } else if (e.keyCode === 39) {
                $('.card__product-zoomer-wrapper').owlCarousel('next');
            }
        }
    });

	(function() {
		let carouselcontainer = $('.c-carousel__preview-container');

		if(carouselcontainer) {
			let needArrow = carouselcontainer.children('div').length > 1;
			if(needArrow) carouselcontainer.parents('.c-carousel__preview').addClass('arrow-on');
		}
	})();

	function findVideos() {
		let videos = document.querySelectorAll('.video');

		for (let i = 0; i < videos.length; i++) {
			setupVideo(videos[i]);
		}
	}

	function setupVideo(video) {
		let link = video.querySelector('.video-link');
		let media = video.querySelector('.video-cover');
		let button = video.querySelector('.video-play');
		let id = parseMediaURL(media);

		video.addEventListener('click', () => {
			let iframe = createIframe(id);

			link.remove();
			button.remove();
			video.appendChild(iframe);
		});

		link.removeAttribute('href');
		video.classList.add('video--enabled');
	}

	function parseMediaURL(media) {
		let regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
		let url = media.src;
		let match = url.match(regexp);

		return match[1];
	}

	function createIframe(id) {
		let iframe = document.createElement('iframe');

		iframe.setAttribute('allowfullscreen', '');
		iframe.setAttribute('allow', 'autoplay');
		iframe.setAttribute('src', generateURL(id));
		iframe.classList.add('video-cover');

		return iframe;
	}

	function generateURL(id) {
		let query = '?rel=0&showinfo=0&autoplay=1';

		return 'https://www.youtube.com/embed/' + id + query;
	}

	findVideos();
});
