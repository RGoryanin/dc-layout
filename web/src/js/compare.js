(function ($) {

    let Compare = {

        firstLaunch: true,

        getScreenWidth: () => $(window).width(),
        breakPoints: {
            xs: 0,
            sm: 576,
            md: 768,
            lg: 992,
            xl: 1200
        },

        context: '',
        setContext: function(id) {
            this.context = '#'+ id;
        },

        itemsAmount: '',
        getVisibleItemsAmount: function() {
            let currentScreenWidth = this.getScreenWidth();
            let itemsShowed;

            if(currentScreenWidth < this.breakPoints.sm) {
                itemsShowed = 2;
            } else if(currentScreenWidth < this.breakPoints.md) {
                itemsShowed = 3;
            } else if(currentScreenWidth < this.breakPoints.lg) {
                itemsShowed = 4;
            } else {
                itemsShowed = 5;
            }

            return itemsShowed;
        },
        items: '',

        navigation: {},

        init: function (context) {

            this.setContext(context || $(document).find('.tab-pane.active').attr('id'));
            this.itemsAmount = $(this.context + ' .product__list-item').length;
            this.items = $.merge($(this.context + ' .product__list-item'), $(this.context +' .characteristic__row-item'));


            this.navigation = {
                prev: -1,
                next: Compare.getVisibleItemsAmount(),
                prevButton: $(Compare.context + ' .compare__list-arrow--prev'),
                nextButton: $(Compare.context + ' .compare__list-arrow--next')
            };

            this.drawItems();

            if(this.firstLaunch) {
                this.firstLaunch = false;
                this.handlers();

                return this;
            }
        },

        drawItems: function() {

            let currentVisibleItemsAmount = this.getVisibleItemsAmount();
            let showAmount = this.itemsAmount < currentVisibleItemsAmount ? this.itemsAmount : currentVisibleItemsAmount;

            Array.from(this.items).reduce((status, item) => {

                if(status < showAmount) {
                    $(item).addClass('show');
                    if(status === 0) $(item).addClass('first');
                }

                if(status === this.itemsAmount - 1) {
                    status = 0;
                } else {
                    status += 1;
                }

                return status;
            }, 0);
        },

        pushLeft: function () {

            this.navigation.next = this.navigation.next - 1;
            this.navigation.prev = this.navigation.prev - 1;
        },

        pushRight: function() {

            this.navigation.next += 1;
            this.navigation.prev += 1;
        },

        pushAction: function(hide, show) {

            for(let i = hide; i < this.items.length; i += this.itemsAmount) {
                $(this.items[i]).hide().removeClass('show');
            }
            for(let i = show; i < this.items.length; i += this.itemsAmount) {
                $(this.items[i]).show(1).addClass('show');
            }

            Array.from(this.items).reduce((status, item) => {
                if($(item).hasClass('first')) $(item).removeClass('first');

                if(!status && $(item).hasClass('show')) {
                    $(item).addClass('first');
                }

                status = $(item).hasClass('show');

                return status;
            }, true);
        },

        reset: function(cb) {
            $.map(this.items, item => {
                $(item).removeClass('show first').removeAttr('style');
            });

            if(this.navigation.prevButton) $(this.navigation.prevButton).addClass('disabled');

            if(cb) cb();
        },

        handlers: function () {

            let self = this;

            $('.compare__list').on('click', '.compare__list-arrow', function (e) {
                e.preventDefault();

                if($(this).hasClass('compare__list-arrow--next')) {
                    if(self.navigation.next < self.itemsAmount) {

                        self.pushAction(self.navigation.prev + 1, self.navigation.next);
                        self.pushRight();

                        if(self.navigation.next === self.itemsAmount) $(self.navigation.nextButton).addClass('disabled');
                        if($(self.navigation.prevButton).hasClass('disabled')) $(self.navigation.prevButton).removeClass('disabled');
                    } else {
                        return false;
                    }
                } else {
                    if(self.navigation.prev >= 0) {

                        self.pushAction(self.navigation.next - 1, self.navigation.prev);
                        self.pushLeft();

                        if(self.navigation.prev < 0) $(self.navigation.prevButton).addClass('disabled');
                        if($(self.navigation.nextButton).hasClass('disabled')) $(self.navigation.nextButton).removeClass('disabled');
                    } else {
                        return false;
                    }
                }
            }).on('change', '.form-checkbox__input', function () {
                let rows = $(self.context + ' .characteristic__row');

                $.map(rows, row => {
                   if(!$(row).hasClass('characteristic__row--diffs') && $(this).is(':checked')) {
                       $(row).hide();
                   } else {
                       $(row).show();
                   }
                });
            });

            $(document).on('click', '.nav-link', function () {

                self.reset();
                self.init($(this).attr('aria-controls'));
            });

            $(window).on('resize', function (e) {
                self.reset(() => self.init());
            }).on('scroll', function () {

                let position = $(this).scrollTop();

                if(position > 339) {
                    $('.product__list-wrapper').addClass('floated');
                } else {
                    $('.product__list-wrapper').removeClass('floated');
                }
            })
        }
    };

    window.Compare = Compare.init();
})($);